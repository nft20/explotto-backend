const {sequelize, ApiRoleAccess } = require("../../models")
const jwt = require("jsonwebtoken");


module.exports = {
    verifyRole: async (req, res, next) => {
        const path = req._parsedUrl.path;
        const data = await sequelize.query(
            `select admin_apis.id, admin_apis.api_name, api_role_accesses.roles_id from admin_apis inner join api_role_accesses on admin_apis.id = api_role_accesses.admin_apis_id 
	         where admin_apis.api_name = '${path}' `
          );

        const roleId = req.user.role === "super_admin" ? 1 : (req.user.role === "admin" ? 2 : 3)

        if (data[0].length === 0) {
            res.status(403).send("Access Forbidden");
        } else if(data[0][0].roles_id.length !== 0){
            roles_id = data[0][0].roles_id;
            if(roles_id.includes(roleId)){
                next();
            }
        } else{
            res.status(403).send("Access Forbidden");
        }
    }
}