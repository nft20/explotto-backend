const {UserAccount} = require("../../models")
const jwt = require("jsonwebtoken");
const config = process.env;

module.exports = {
  verifyToken: async(req, res, next) =>{
  const token = req.headers["authorization"];

  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    TokenArray = token.split(" ");
    const decoded = jwt.verify(TokenArray[1], config.TOKEN_KEY);

    let user = await UserAccount.findOne({ where: { email: decoded.email } });
    if (user === null) {
      return res.status(401).send("Invalid Token ");
    } else if (user.dataValues.token === TokenArray[1]) {
      req.user = user;
    }else{
      return res.status(401).send("Invalid Token ");
    }
    
  } catch (err) {
    return res.status(401).send("Invalid Token " + err.message);
  }
  return next();
  }
}
