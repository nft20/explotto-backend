require('dotenv').config();
const Constants = require("./Constants");
const responseHandler = require('./utils/response-handler');
const requestValidator = require('./utils/request-validator');
const {AbortController} = require("node-abort-controller");
global.AbortController = AbortController;
global.currentLotteryId = 1;
global.adminAccountAddress = process.env.ADMIN_ACCOUNT_ADDRESS;
global.nftContractAddress = process.env.NFT_CONTRACT_ADDRESS; // Access Control Contract 4 TEST NET
global.nftContractABI = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"approved","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"bytes32","name":"previousAdminRole","type":"bytes32"},{"indexed":true,"internalType":"bytes32","name":"newAdminRole","type":"bytes32"}],"name":"RoleAdminChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":true,"internalType":"address","name":"sender","type":"address"}],"name":"RoleGranted","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":true,"internalType":"address","name":"sender","type":"address"}],"name":"RoleRevoked","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[],"name":"DEFAULT_ADMIN_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MINTER_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"approve","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"burn","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"getApproved","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"}],"name":"getRoleAdmin","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"uint256","name":"index","type":"uint256"}],"name":"getRoleMember","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"}],"name":"getRoleMemberCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"grantRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"hasRole","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"string","name":"_tokenURI","type":"string"}],"name":"mint","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"renounceRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"revokeRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"tokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"transferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"}];
global.currency = "BNB";
global.salt = process.env.SALT;
global.FireTicketColorArray = [
  "#F63B3B",
  "#FF5E62",
  "#FFCE56",
  "#B20A2C",
  "#F67E3B",
];
global.AirTicketColorArray = [
  "#DBB3C6",
  "#FAACA8",
  "#9796F0",
  "#D3CCE3",
  "#F5C5FA",
];
global.WaterTicketColorArray = [
  "#3CCAC2",
  "#0052D4",
  "#2BC0E4",
  "#3CCAC2",
  "#619AEE",
];
global.EarthTicketColorArray = [
  "#16A085",
  "#988C4D",
  "#155159",
  "#757519",
  "#376748",
];
global.mintLimit = 100;
global.minBalanceFactor=10;
const Web3 = require("web3");
const web3 = new Web3(process.env.BNB_NODE_MODULE);
global.web3=web3;
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const userRouter = require('./routers/web/userRouter')
const lotteryRouter = require('./routers/web/lotteryRouter')
const statisticsRouter = require('./routers/web/statisticsRouter')
const adminRouter = require('./routers/admin/adminRouter.js')
var cron = require('node-cron');
const lotteryController = require('./controllers/web/lotteryController')
const  walletService=require('./services/walletService');
const resultService = require('./services/resultService')
const  blockChainService = require('./services/blockChainService');
const  adminRouteService = require('./services/adminRouteService');
const { A } = require('svg.js');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
// app.use(bodyParser.json())
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));
app.use(requestValidator);
app.use(responseHandler);
app.use('/ticket-images', express.static(__dirname + '/tickets'))
app.use('/avatar-templates', express.static(__dirname + '/avatars'))
app.use('/user', userRouter);
app.use('/lottery', lotteryRouter);
app.use('/stats', statisticsRouter);
app.use('/admin', adminRouter);
app.get('/', (req, res) => {
	res.send('Hello World!')
})
app.use('/generateResult', async (req, res) => {
	console.log("Req parameters ", req.query.key);
	var masterKey = process.env.MASTER_KEY;
	if (masterKey == req.query.key) {
    try {
      await resultService.resultScheduler();
      res.send(
        "Results are declared. Please check your credit history in a while. We are confirming transactions"
      );
    } catch (e) {
      console.log(e);
      res.send(e.message);
    }
		
	}
	else res.send('Incorrect Key');
})

console.log("SERVER PORT ", process.env.PORT)
app.listen(process.env.PORT, () => console.log(`Server running on port ${process.env.PORT}`));
adminRouteService.addNewRoutes(adminRouter);
walletService.loadMasterWallet();
walletService.fetchAndAddAllAdminAccountsToWallet();
global.gasPrice=10000000000 ;
global.gasPriceInEther=web3.utils.fromWei('10000000000', "ether");
global.minNFTFee=(gasPriceInEther*Constants.MINT_APPROX_GAS);
blockChainService.getLatestGasPrice();
 // Cron Jobs
 // ┌────────────── second (optional)
 // │ ┌──────────── minute
 // │ │ ┌────────── hour
 // │ │ │ ┌──────── day of month
 // │ │ │ │ ┌────── month
 // │ │ │ │ │ ┌──── day of week
 // │ │ │ │ │ │
 // │ │ │ │ │ │
 // * * * * * *
// At every minute


cron.schedule('* * * * *', async () => {

	// confirm lottery prize payment
	resultService.lotteryPrizePaymentConfirmationScheduler();
  // topup addmin accounts
	walletService.transferPaymentsToAdminAccounts();

  // confirm admin topup paymnet
  walletService.confirmTransferPaymentsToAdminAccounts();

  // Trade tickets flow
	// STEP 1: Trade ticket schedular
	lotteryController.transferTicketsToAdmin();
	// STEP 2: confirm transfer ticket are recived to admin
	lotteryController.transferNftConfirmation();
	// STEP 3: it will confirm transfer tickets and will create new ticket as trade reward
	lotteryController.ticketTransferOrderConfirmation();
});

cron.schedule('*/15 * * * * *', () => {
	// Buy Ticket STEP 1
	lotteryController.verifyOrderPayment();
	// Buy Ticket STEP 2 in cron cron.schedule('*/30 * * * * *', ()
	

	 // Buy Ticket STEP 5
	lotteryController.orderConfirmation();
});
cron.schedule('*/1 * * * *', () => {

    // Buy Ticket STEP 3
    lotteryController.createNft();
	 // Buy Ticket STEP 4 in cron cron.schedule('*/30 * * * * *', ()

});
cron.schedule('*/30 * * * * *', () => {

	// Buy Ticket STEP 2 create local tickets
	lotteryController.startTicketCreation();
  lotteryController.processFailedTickets();

  // Buy Ticket STEP 4
	lotteryController.nftConfirmation();


});


cron.schedule('*/5 * * * *', async () => {
  // on lottrey result declared give prize to users
   resultService.lotteryPrizePayment();

   // declare result
   resultService.automaticResultDeclare();
 });
 

cron.schedule('*/10 * * * *', async () => {
  // Check if we need more admin accounts for mint
	walletService.checkAccountsRequirement();
  walletService.topupAccount();
});


cron.schedule('*/2 * * * *', async () => {
  // set latest gas price into global variable
	blockChainService.getLatestGasPrice()
});

cron.schedule('*/30 * * * * *', async () => {
	lotteryController.transferLotteryOwner();
});

blockChainService.updateTransferNFT();
walletService.resetAdminAccountToAvailable();
//blockChainService.grantMinterRole('0x88cA48Eaa7552b265431430c4E4AB50b13770Dd9')

