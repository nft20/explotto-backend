const walletService = require('./walletService');
const Web3 = require("web3");
const web3=global.web3;
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket,
} = require("../models");

const lotteryController = require("../controllers/web/lotteryController");
const Constants = require("../Constants");

const nftContractABI = global.nftContractABI;
const nftContractAddress = global.nftContractAddress;
let nftContractInstance = new web3.eth.Contract(
  nftContractABI,
  nftContractAddress
);
async function getLatestGasPrice()
{
  let gasPrice = await web3.eth.getGasPrice();
  let gasPriceInEther = web3.utils.fromWei(gasPrice, "ether");
  global.gasPrice=gasPrice;
  global.gasPriceInEther=gasPriceInEther;
  global.minNFTFee=(gasPriceInEther*Constants.MINT_APPROX_GAS);
}
async function getConfirmations(txHash){
  
  try {
    // Get transaction details
    const trx = await web3.eth.getTransaction(txHash);
    // const gasUsed = await web3.eth.getTransactionReceipt(txHash)

    console.log("TRX IS ", trx);
    if (trx == null) return new Promise(function(resolve, reject) {
      resolve(null);
    });;
    // console.log("Gas Used IS ", gasUsed)

    // Get current block number
    const currentBlock = await web3.eth.getBlockNumber();

    // When transaction is unconfirmed, its block number is null.
    // In this case we return 0 as number of confirmations
    let t=trx.blockNumber === null ? 0 : currentBlock - trx.blockNumber;
    return new Promise(function(resolve, reject) {
      resolve(t);
    });
  } catch (error) {
    console.log(error);
    return new Promise(function(resolve, reject) {
      reject(t);
    });
  }
};
async function getConfirmationsWithDetails(txHash,confirmation){
  
  try {
    // Get transaction details
    const trx = await web3.eth.getTransaction(txHash);
    // const gasUsed = await web3.eth.getTransactionReceipt(txHash)

    console.log("TRX IS ", trx);
    if (trx == null) return new Promise(function(resolve, reject) {
      resolve(null);
    });;
    
    const currentBlock = await web3.eth.getBlockNumber();
    let t=trx.blockNumber === null ? 0 : currentBlock - trx.blockNumber;
    let isConfirmed= t >= confirmation?true:false;
    let trxCharge=0;
    if(isConfirmed)
    {
      const trxReceipt = await web3.eth.getTransactionReceipt(txHash);
      let trxGasFee = await web3.utils.fromWei(trx.gasPrice, "ether");
      trxCharge = trxReceipt.gasUsed * JSON.parse(trxGasFee);
    }

    
    return new Promise(function(resolve, reject) {
      resolve({
        isConfirmed:isConfirmed,
        txFee:trxCharge
      });
    });
  } catch (error) {
    console.log(error);
    return new Promise(function(resolve, reject) {
      reject(error);
    });
  }
};
async function getNFTEstimateGas()
{
  let nftEstimatedGas = await nftContractInstance.methods
    .mint(
      "0x16Ea5a0fada525EF239e13CdA7667934027A3EB1",
      "https://ipfs.io/ipfs/QmQusy6FCkV4b97Y4gVbXeraBvmLwKNhhe7nwMWJGJPDnn"
    )
    .estimateGas({
      from: "0x151f07b004FD0D12e1623b8f9fa6FcDb4Ef3Dea4",
      gasLimit: 200000,
      gasPrice: 10000000000,
    });
    return nftEstimatedGas;
}

async function estimateGasPrices() {
  let gasPrice = await web3.eth.getGasPrice();
  let gasInEther = web3.utils.fromWei(gasPrice, "ether");

  let nftEstimatedGas = await getNFTEstimateGas();
  console.log("NFT EstimatedGas ", nftEstimatedGas);

  let amountToBePaid = "0.001";
  var tokens = web3.utils.toWei(amountToBePaid, "ether");
  var bntokens = web3.utils.toBN(tokens);

  console.log("BN Tokens ", tokens, bntokens);
  let bnbSendGas = await web3.eth.estimateGas({
    from: global.adminAccountAddress,
    to: "0x16Ea5a0fada525EF239e13CdA7667934027A3EB1",
    value: bntokens,
    gas: 50000,
    gasPrice: 10000000000,
  });
  console.log("BNB send gas ", bnbSendGas);

  return {
    nftEstimatedGas: nftEstimatedGas,
    bnbSendGas: bnbSendGas,
    gasPrice: JSON.parse(gasInEther),
  };
}
async function mintNFT(userTicket,availableAccount) {
  let ticketURIHash = userTicket.nft_doc_hash;
  let owner = userTicket.address;

  console.log("Inside Mint NFT ");
  
  let data = null;
  try {
  
    
    await AdminAccounts.update(
      {
        availability_status: "busy",
      },
      {
        where: {
          admin_account_address: availableAccount.address,
        },
      }
    );

    data = await nftContractInstance.methods.mint(owner, ticketURIHash).send({
      from: availableAccount.address,
      //from: global.adminAccountAddress,
      gasLimit: 250000,
      gasPrice: gasPrice,
    });
    console.log("NFT Tx Hash is ", data);

    await LotteryUserTicket.update(
      {
        nft_created_by:availableAccount.address,
        nft_tx_id: data.transactionHash,
        ticket_status: Constants.TICKET_STATUS.NFT_CONFIRMATION_PENDING,
      },
      {
        where: { id:userTicket.id},
      }
    );
  } catch (error) {
    console.log("ERROR IN MINT NFT ", error);
    const item = await LotteryUserTicket.update(
      {
        ticket_status: Constants.TICKET_STATUS.NFT_FAILED,
      },
      {
        where: { id:userTicket.id },
      }
    );
  }
  finally {
    let currentBalance = await getAccountBalance(availableAccount.address);
    await AdminAccounts.update(
      {
        availability_status: "available",
        account_balance: currentBalance,
      },
      {
        where: {
          admin_account_address: availableAccount.address,
        },
      }
    );
  }
  return {
    nftTxHash: data,
  };
}
async function transfer(fromAddress,toAddress, amount) {
  console.log(
    "Inside transfer, Amount to be paid : ",
    amount,
    "  to address  :  ",
    toAddress,
    "  Lottery Id  "
  );


  // var contractAbi = ABI
  let gasPrice = await web3.eth.getGasPrice();



  console.log("TRANSACTING .... ");

  let amountToBePaid = amount;
  var tokens = web3.utils.toWei(amountToBePaid, "ether");
  var bntokens = web3.utils.toBN(tokens);

  console.log("BN Tokens ", tokens, bntokens);
  let txHash = null;

    txHash = await web3.eth.sendTransaction({
      from: fromAddress,
      to: toAddress,
      value: bntokens,
      gas: 50000,
      gasPrice: gasPrice,
      // chain: '0x61',
    });

  return txHash;

  
}
async function getAccountBalance(address) {
  let currentBalanceInWei = await web3.eth.getBalance(address);
  // return currentBalanceInWei;

  const currentBalanceInEther = web3.utils.fromWei(
    currentBalanceInWei,
    "ether"
  );
  console.log(currentBalanceInEther);
  return JSON.parse(currentBalanceInEther);
}

async function grantMinterRole(address) {
  let minterRoleInBytes =
    await nftContractInstance.methods.MINTER_ROLE.call().call();

  let grantRoleEstimatedGas = await nftContractInstance.methods
    .grantRole(minterRoleInBytes, address)
    .estimateGas({
      from: global.adminAccountAddress,
      gasLimit: 200000,
      gasPrice: 10000000000,
    });
  console.log("Grant Minter role", grantRoleEstimatedGas);

  try{
  let grantRoleResponse = await nftContractInstance.methods
    .grantRole(minterRoleInBytes, address)
    .send({
      from: global.adminAccountAddress,
      gasLimit: grantRoleEstimatedGas,
      gasPrice: 10000000000,
    });
    console.log("Grant Minter role", grantRoleResponse);
  }
  catch(err)
  {
    console.error('ERROR=================')
    console.log(err)
  }
  
  
}
async function getAndGrantMinterRole(address, privateKey) {
  let minterRoleInBytes =
    await nftContractInstance.methods.MINTER_ROLE.call().call();

  let grantRoleEstimatedGas = await nftContractInstance.methods
    .grantRole(minterRoleInBytes, address)
    .estimateGas({
      from: global.adminAccountAddress,
      gasLimit: 200000,
      gasPrice: 10000000000,
    });
  console.log("Grant Minter role", grantRoleEstimatedGas);

  let grantRoleResponse = await nftContractInstance.methods
    .grantRole(minterRoleInBytes, address)
    .send({
      from: global.adminAccountAddress,
      gasLimit: grantRoleEstimatedGas,
      gasPrice: 10000000000,
    });
  console.log("Grant Minter role", grantRoleResponse);
  if (grantRoleResponse.status) {
    await addCredentialsToWallet(address, privateKey);
  }
}
async function addCredentialsToWallet(address, privateKey) {
  let response = await web3.eth.accounts.wallet.add({
    privateKey: privateKey,
    address: address,
  });
}
async function updateTransferNFT() {
  // let userAddresses = await sequelize.query(`select address from user_accounts`);
  // userAddresses = userAddresses[0];

  var options = {
    timeout: 30000, // ms

    clientConfig: {
      // Useful if requests are large
      maxReceivedFrameSize: 100000000, // bytes - default: 1MiB
      maxReceivedMessageSize: 100000000, // bytes - default: 8MiB

      // Useful to keep a connection alive
      keepalive: true,
      keepaliveInterval: 60000, // ms
    },

    // Enable auto reconnection
    reconnect: {
      auto: true,
      delay: 5000, // ms
      maxAttempts: 5,
      onTimeout: false,
    },
  };

  const web3 = new Web3(
    new Web3.providers.WebsocketProvider(
      process.env.BNB_WS_NODE_MODULE,
      options
    )
  );

  const currentBlock = await web3.eth.getBlockNumber()-2400; // 2 hours before
  var subscription = web3.eth
    .subscribe(
      "logs",
      {
        fromBlock:currentBlock,
        address: nftContractAddress,
        topics: [
          "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
        ], // Transfer topic0 hash value
      },
      function (error, result) {
        if (error) console.log(error);
      }
    )
    .on("connected", function (subscriptionId) {
      console.log(subscriptionId);
    })
    .on("data", async function (log) {
      console.log("=============subscribe_log===========",log);
      lotteryController.updateLotteryOwner(log);
    })
    .on("changed", function (log) {
      console.log("changed");
    });

  return;
}
async function deployContract() {
  const contractABI = require("../nftContractABI.json");
  const contractByteCode = require("../nftContractByteCode.json");

  const MyContract = new web3.eth.Contract(contractABI);

  const gas = await MyContract.deploy({
    data: contractByteCode.object,
  }).estimateGas();

  MyContract.deploy({
    data: contractByteCode.object,
  })
    .send({
      from: "0x151f07b004FD0D12e1623b8f9fa6FcDb4Ef3Dea4",
      gas: 5000000,
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("transactionHash", (transactionHash) => {
      console.log(transactionHash);
    })
    .on("receipt", (receipt) => {
      // receipt will contain deployed contract address
      console.log(receipt);
    })
    .on("confirmation", (confirmationNumber, receipt) => {
      console.log(receipt);
    });
}
exports.deployContract = deployContract;
exports.updateTransferNFT = updateTransferNFT;
exports.getAndGrantMinterRole = getAndGrantMinterRole;
exports.grantMinterRole = grantMinterRole;
exports.getAccountBalance = getAccountBalance;

exports.mintNFT = mintNFT;
exports.estimateGasPrices = estimateGasPrices;
exports.transfer = transfer;
exports.getConfirmations = getConfirmations;
exports.getConfirmationsWithDetails=getConfirmationsWithDetails;
exports.getNFTEstimateGas=getNFTEstimateGas;
exports.nftContractAddress = nftContractAddress;
exports.nftContractInstance = nftContractInstance;
exports.getLatestGasPrice=getLatestGasPrice;
