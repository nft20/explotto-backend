const db = require("../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
} = require("../models");
const { LotteryUserTicket } = require("../models");
const { LotteryUser } = require("../models");
const { Team } = require("../models");
const { Lottery } = require("../models");
const user = require("../models/userAccount");
const Op = db.Sequelize.Op;

const nftContractABI = global.nftContractABI;
const date = require("date-and-time");
require("dotenv").config();
const crypto = require("crypto");
const properties = require("../models/properties");
const nodemailer = require('nodemailer');
const otpGenerator = require('otp-generator');
const passwordGenerator = require('generate-password');

async function sendEmail(email, subject, message){
    var transporter = nodemailer.createTransport({
        service: 'yandex',
        auth: {
          user: process.env.ADMIN_EMAIL_ADDRESS,
          pass: process.env.ADMIN_EMAIL_PASSW0RD
        }
      });
      
      var mailOptions = {
        from: process.env.ADMIN_EMAIL_ADDRESS,
        to: email,
        subject: subject,
        text: message
      };
      
     transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
  }
  exports.sendEmail = sendEmail;