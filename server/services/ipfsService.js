

const { create,CID,globSource } = require('ipfs-http-client')
const ipfs = create({
  host: "ipfs.infura.io",
  port: 5001,
  protocol: "https",
})







exports.storeToIPFS=async function(buffer) {
    console.log("Inside Store to IPFS");
    
    let data,error;
    try{
      data=await ipfs.add(buffer);
    }
    catch(err)
    {
      error=err;
      console.error("ERROR INSIDE STORE TI IPFS ", error);
    }
    return new Promise((resolve, reject) => {
      if (error) {
        reject(error);
      } else {
        console.log("IPFS Result ", data);
        resolve(data.path);
      }
    });
  }