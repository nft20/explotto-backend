const db = require("../models");
const Op = db.Sequelize.Op;
const fs = require("fs");
const cryptoUtils = require('../utils/cryptoUtils');
const web3=global.web3;
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket,
} = require("../models");
const axios = require('axios');

const blockChainService = require("../services/blockChainService");
const Constants = require("../Constants");

function loadMasterWallet() {
  var walletName = global.currency + "_" + process.env.PROFILE + ".json"; //BNB_sandbox.json

  let data = fs.readFileSync(process.env.WALLET_PATH + walletName, {
    encoding: "utf8",
    flag: "r",
  }); //  '/opt/nftLottery/'+ BNB_sandbox.json

  if (data) {
    console.log("Admin account exists.");
   
    let enPrivateKey = JSON.parse(data).privateKey;
    var decrypted =cryptoUtils.decrypt(enPrivateKey)
    process.privateKey = decrypted
  } else {
    let data = web3.eth.accounts.create();

    let privateKey = JSON.parse(data).privateKey;
    var enPrivateKey =cryptoUtils.encrypt(privateKey)

    let credentials = {
      address: JSON.parse(data).address,
      privateKey: enPrivateKey,
    };
    fs.writeFile(
      process.env.WALLET_PATH + walletName,
      JSON.stringify(credentials),
      (err, data) => {
        if (err) throw err;
      }
    );
  }

  if (process.privateKey) {
    console.log("INSIDE IF OF WALLET ADD");
    web3.eth.accounts.wallet.add({
      privateKey: process.privateKey,
      address: global.adminAccountAddress,
    });
  }
}

async function loadAdminWallets() {
  let adminAccounts = await sequelize.query(`select * from admin_accounts`);
  adminAccounts = adminAccounts[0];

  for (let i = 0; i < adminAccounts.length; i++) {
    const element = adminAccounts[i];
    var dePrivateKey =cryptoUtils.decrypt(element.en_private_key)
    await addCredentialsToWallet(element.admin_account_address, dePrivateKey);
  }
}
async function addCredentialsToWallet(address, privateKey) {
  let response = await web3.eth.accounts.wallet.add({
    privateKey: privateKey,
    address: address,
  });
}

// ________________________________________________________________________________
//  Parallel Processing Code

async function checkAccountsRequirement() {
  let pendingTicketsCount = await sequelize.query(
    `select count(id) from lottery_user_tickets where ticket_status in ('${Constants.TICKET_STATUS.IPFS_FAILED}','${Constants.TICKET_STATUS.IPFS_COMPLETED}','${Constants.TICKET_STATUS.NFT_FAILED}','${Constants.TICKET_STATUS.CREATED}')`
  );
  pendingTicketsCount = JSON.parse(pendingTicketsCount[0][0].count);

  let totalAdminAccountsCount = await sequelize.query(
    `select count(admin_account_address) from admin_accounts`
  );
  totalAdminAccountsCount = JSON.parse(
    totalAdminAccountsCount[0][0].count
  );
  let currentMintingCapacity =
    totalAdminAccountsCount * global.mintLimit;

  if (pendingTicketsCount > currentMintingCapacity) {

    let numberOfAccountsRequired = Math.ceil(
      (pendingTicketsCount - currentMintingCapacity) / global.mintLimit
    );
    if (numberOfAccountsRequired >= 0) {
      await createNewAdminAccounts(numberOfAccountsRequired,pendingTicketsCount);
      await topupAccount();
    } 

  }

}


async function topupAccount() {
  let fundDistributionDetails = await Properties.findOne({
    where: { id: "FUND_DISTRIBUTION_DETAILS" },
  });
  fundDistributionDetails = JSON.parse(fundDistributionDetails.dataValues.val);
  let minBalance=global.minNFTFee*global.minBalanceFactor;
  let superAdminBalance =await blockChainService.getAccountBalance(global.adminAccountAddress);
  let distributionAmount=(superAdminBalance*fundDistributionDetails.maintenance)/2;
  if(distributionAmount<minBalance)
  {
    console.log(`Admin account topup failed because Super admin ${global.adminAccountAddress} has insufficent balance`)
   //TODO send email to support team
   return;
  }

  let pendingTicketsCount = await sequelize.query(
    `select count(id) from lottery_user_tickets where ticket_status 
    in ('${Constants.TICKET_STATUS.IPFS_FAILED}',
    '${Constants.TICKET_STATUS.IPFS_COMPLETED}',
    '${Constants.TICKET_STATUS.NFT_FAILED}',
    '${Constants.TICKET_STATUS.CREATED}')`
  );
  pendingTicketsCount = JSON.parse(pendingTicketsCount[0][0].count);
  if (pendingTicketsCount == 0) {
    return;
  }
  let requiredAdminAccount = Math.ceil(
    pendingTicketsCount / global.mintLimit
  );

  
  


  let availableAccount = await sequelize.query(
    `select account_balance,admin_account_address
    from admin_accounts
    where admin_accounts.admin_account_address not in ( select to_account from admin_account_payments
         where pay_status in ('${Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PROCESSING}',
         '${Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PENDING}',
         '${Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PAYMENT_CONF_PENDING}')   )
         and account_balance <= ${global.minNFTFee} 
         order by account_balance asc
    limit ${requiredAdminAccount}`
  );
  
  availableAccount = availableAccount[0];
  
  if(!availableAccount||availableAccount.length==0)
  {
    return;
  }


  let sendAccountCount=(Math.round(distributionAmount/minBalance))
  let finalAccountCount= sendAccountCount>requiredAdminAccount?requiredAdminAccount:sendAccountCount
  let accountDistributionAmount=distributionAmount/finalAccountCount;

  for (let k = 0; k < finalAccountCount; k++) {
      let account=availableAccount[k];
      await AdminAccountPayments.create({
        amount: accountDistributionAmount,
        from_account: global.adminAccountAddress,
        to_account: account.admin_account_address,
        pay_status: Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PENDING,
        pay_currency: global.currency,
      });
    }  
}

async function createNewAdminAccounts(numberOfAccountsRequired) {
 



  for (let i = 0; i < numberOfAccountsRequired; i++) {
    let data = web3.eth.accounts.create();
    console.log("New Admin Account ", data);
    var enPrivateKey =cryptoUtils.encrypt(data.privateKey)

    let createNewAdminAccountResponse = await AdminAccounts.create({
      // admin_account_name: newAccountName,
      admin_account_address: data.address,
      en_private_key: enPrivateKey,
      availability_status: "available",
      account_balance: 0,
    });

    await blockChainService.getAndGrantMinterRole(data.address, data.privateKey);
  }
}



async function transferPaymentsToAdminAccounts() {
  try{
	let adminAccount =
    await sequelize.query(`select to_account, sum(amount) as amount 
    from admin_account_payments
    where pay_status = '${Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PENDING}'
    group by to_account limit 1`);
		adminAccount = adminAccount[0][0];
		if (adminAccount) {
      await AdminAccountPayments.update({
        pay_status: Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PROCESSING,
      }, {
        where: {
          to_account: adminAccount.to_account,
          pay_status:Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PENDING
        }
      })
			let paymentDetails = await blockChainService.transfer(global.adminAccountAddress,adminAccount.to_account, adminAccount.amount);
			console.log("Payment To Admin Account TRX Hash ", paymentDetails);
			if (paymentDetails && paymentDetails.status) {
				await AdminAccountPayments.update({
					pay_status: Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PAYMENT_CONF_PENDING,
					pay_tx_id: paymentDetails.transactionHash
				}, {
					where: {
						to_account: adminAccount.to_account,
            pay_status:Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PROCESSING
					}
				})
			}
      else{
        await AdminAccountPayments.update({
					pay_status: Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PENDING,
				}, {
					where: {
						to_account: adminAccount.to_account,
            pay_status:Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PROCESSING
					}
				})
      }
		}
  }
  catch(e)
  {
    console.error(e);
    const url = process.env.group_url;
    axios.post(url, { text: "Error in transferPaymentsToAdminAccounts  :" +  e.message})
    .then(res => {
      console.log(`statusCode: ${res.status}`);
      console.log(res);
    })
    .catch(error => {
      console.error(error);
    });
  }
}



async function confirmTransferPaymentsToAdminAccounts() {
  try{
	
    let txList;
     txList = await AdminAccountPayments.findAll({
      where: {
        pay_status: Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.PAYMENT_CONF_PENDING,
      }
    });

    for (let i = 0; i < txList.length; i++) {
      let pendingPayment = txList[i].dataValues;
      
      const txHash = pendingPayment.pay_tx_id;
      let confirmation=await blockChainService.getConfirmationsWithDetails(txHash,Constants.SEND_CONFIRM_BLOCK_COUNT);
      if(confirmation&&confirmation.isConfirmed)
      {
        await AdminAccountPayments.update({
          pay_fee:confirmation.txFee,
					pay_status: Constants.TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS.COMPLETED,
				}, {
					where: {
						id: pendingPayment.id
					}
				})
        let currentBalance = await blockChainService.getAccountBalance(pendingPayment.to_account);
        await AdminAccounts.update({
					account_balance: currentBalance,
				}, {
					where: {
						admin_account_address: pendingPayment.to_account
					}
				})
      }
    }

  }
  catch(e)
  {
    console.error(e);
  }
}

async function fetchAndAddAllAdminAccountsToWallet() {
  let adminAccounts = await sequelize.query(`select * from admin_accounts`);
  adminAccounts = adminAccounts[0];

  for (let i = 0; i < adminAccounts.length; i++) {
    const element = adminAccounts[i];
    let enPrivateKey = element.en_private_key;
    var dePrivateKey =cryptoUtils.decrypt(enPrivateKey);
    await addCredentialsToWallet(element.admin_account_address, dePrivateKey);
  }
}

async function resetAdminAccountToAvailable()
{
  console.log("==========resetAdminAccountToAvailable=============")
  await AdminAccounts.update({
    availability_status:Constants.ACCOUNT_STATUS.AVAILABLE
  }, {
    where: {
      availability_status: Constants.ACCOUNT_STATUS.BUSY
    }
  })
}



exports.fetchAndAddAllAdminAccountsToWallet=fetchAndAddAllAdminAccountsToWallet;
exports.transferPaymentsToAdminAccounts=transferPaymentsToAdminAccounts;

exports.checkAccountsRequirement = checkAccountsRequirement;

exports.loadMasterWallet = loadMasterWallet;
exports.loadAdminWallets = loadAdminWallets;
exports.addCredentialsToWallet = addCredentialsToWallet;
exports.confirmTransferPaymentsToAdminAccounts=confirmTransferPaymentsToAdminAccounts;
exports.topupAccount=topupAccount;
exports.resetAdminAccountToAvailable=resetAdminAccountToAvailable;
