const {AdminApi, ApiRoleAccess} = require("../models");

// add new register routes on admin
async function addNewRoutes(adminRouter) {
	const apiEndPoints = adminRouter.stack.filter(r => r.route).map(r => r.route.path);
	let lastApiAdded = await AdminApi.findAll({
		order: [["createdAt", "DESC"]],
		limit: 1,
	});
	if (lastApiAdded.length !== 0) {
		lastApiAdded = lastApiAdded[0].dataValues;
		let index = apiEndPoints.indexOf(lastApiAdded.api_name);
		try {
			for (i = index + 1; i < apiEndPoints.length; i++) {
				const item = await AdminApi.create({
					api_name: apiEndPoints[i]
				});

				let addRoleAccess = await ApiRoleAccess.create({
					admin_apis_id: item.id,
					roles_id: [1]
				});
			}
		} catch (error) {
			console.log("Error");
		}
	} else{
		try {
			for (i = 0; i < apiEndPoints.length; i++) {
				const item = await AdminApi.create({
					api_name: apiEndPoints[i]
				});

				let addRoleAccess = await ApiRoleAccess.create({
					admin_apis_id: item.id,
					roles_id: [1]
				});
			}
		} catch (error) {
			console.log("Error");
		}
	}
}
exports.addNewRoutes = addNewRoutes;