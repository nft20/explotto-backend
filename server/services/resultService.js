const db = require("../models");
const Op = db.Sequelize.Op;
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket,
  LotteryResults,
} = require("../models");
const lotteryController = require("../controllers/web/lotteryController");
const commonUtil = require("../utils/commonUtil");
const blockChainService = require("../services/blockChainService");
const date = require("date-and-time");
const Constants = require("../Constants");
const web3=global.web3;
const axios = require('axios');
/* 
 @author:
 @date:19th April 2022
 @description: get all lottries whose end time is passed and result not delcare that is winning_no is null
*/
exports.automaticResultDeclare = async function () {
  let pendingLotteryResult = await Lottery.findOne({
    where: {
      end_time: { [Op.lte]: new Date() },
      winning_no: null,
    },
  });
  console.log("Pending Lottery  ", pendingLotteryResult);
  if (pendingLotteryResult != null) {
    pendingLotteryResult = pendingLotteryResult.dataValues;
    try{
      await resultScheduler();
    }
    catch(e)
    {
        console.log(e);
    }
    
  }
};
async function resultScheduler() {

  let activelotteryId = await lotteryController.getLatestLotteryId();
  let countOfEmptyTickets = await sequelize.query(
    `select count(id) from lottery_orders where status not in('${Constants.ORDER_STATUS.PENDING}','${Constants.ORDER_STATUS.COMPLETED}') and lottery_id=${activelotteryId}`
  );
  countOfEmptyTickets = countOfEmptyTickets[0][0];
  if(countOfEmptyTickets.count>0)
  {
    const url = process.env.group_url;
    axios.post(url, { text: "Error declaring result some order not generated" })
    .then(res => {
      console.log(`statusCode: ${res.status}`);
      console.log(res);
    })
    .catch(error => {
      console.error(error);
    });
    
    // throw new Error('error_some_orders_tickets_not_generated');
    return;
  }

  let winningNumber = await commonUtil.generateTicketNumber();

 
  console.log(`winning_number_${winningNumber}_for_${activelotteryId}}`);
  await lotteryController.updateWinningNumberToLotteryTable(
    activelotteryId,
    winningNumber
  );

  lotteryWinningNumber = winningNumber.split(" ");
  await getResult(activelotteryId, lotteryWinningNumber);
}

async function lotteryPrizePaymentConfirmationScheduler() {
  let txList;
  try{
   txList = await LotteryResults.findAll({
    where: {
      prize_payment_status: Constants.PRIZE_PAYMENT_STATUS.PAYMENT_CONF_PENDING,
    },
    limit: 30,
  });
}
catch(err)
{
  console.log(err);
  return;
}
  console.log(
    "Transaction List With Pending Payment Transactions Confirmations ",
    txList
  );
  for (let i = 0; i < txList.length; i++) {
    let pendingPayment = txList[i].dataValues;
    console.log("Pending Payment For Confimation ", i, " is  ", pendingPayment);
    const txHash = pendingPayment.prize_payment_id;
    await confirmTransferToUser(txHash);
  }
};

async function lotteryPrizePayment() {
  let listOfWinners = await sequelize.query(
    `select id,address, total_prize as net_amount, lottery_id  from lottery_results where prize_payment_status = '${Constants.PRIZE_PAYMENT_STATUS.PENDING}' limit 1`
  );

  if (listOfWinners.length > 0 && listOfWinners[0][0]) {
    let listOfResult = listOfWinners[0][0];

    let res = await LotteryResults.update(
      {
        prize_payment_status:
          Constants.PRIZE_PAYMENT_STATUS.PAYMENT_IN_PROGRESS,
      },
      {
        where: {
          id: listOfResult.id,
        },
      }
    );
    let txHash;
    try{
      txHash = await blockChainService.transfer(
      global.adminAccountAddress,
      listOfResult.address,
      JSON.parse(listOfResult.net_amount).toFixed(Constants.CURRENCY_DECIMAL)
    );
    }
    catch(err)
    {
      console.error(err);
      console.log(listOfResult.id,listOfResult.address)
      console.log(`======error_transfer_prize_for_${txHash}==============================`,err)
    
      let res = await LotteryResults.update(
        {
          prize_payment_status: Constants.PRIZE_PAYMENT_STATUS.FAILED,
          error: JSON.stringify(err.message),
        },
        {
          where: {
            id: listOfResult.id,
          },
        }
      );

      const url = process.env.group_url;
      axios.post(url, { text: "Error sending prize amount :" +  err.message + "\n for lotery: " + listOfResult.lottery_id + " \n Address: " + listOfResult.address})
      .then(res => {
        console.log(`statusCode: ${res.status}`);
        console.log(res);
      })
      .catch(error => {
        console.error(error);
      });
      return;
    }
    if (txHash != null) {
      console.log("====================================",txHash)
      let res = await LotteryResults.update(
        {
          prize_payment_id: txHash.transactionHash,
          prize_payment_status:
            Constants.PRIZE_PAYMENT_STATUS.PAYMENT_CONF_PENDING,
          prize_payment_time: date.format(new Date(), "YYYY-MM-DD HH:mm:ss"),
        },
        {
          where: {
            id: listOfResult.id,
          },
        }
      );
    } else {
      let res = await LotteryResults.update(
        {
          prize_payment_status: Constants.PRIZE_PAYMENT_STATUS.PENDING,
        },
        {
          where: {
            id: listOfResult.id,
          },
        }
      );
    }
  }
}

async function getResult(lotteryId, lotteryWinningNumber) {
  console.log("Inside getResult");

  const foundLottery = await Lottery.findOne({
    where: { id: lotteryId },
  });
  console.log("Found Lottery ", foundLottery.dataValues);

  if (parseFloat(foundLottery.dataValues.total_fund) != 0) {
    try{
    let pointsObject = await calculateTotalPoints(
      lotteryId,
      lotteryWinningNumber,
      foundLottery
    );
    console.log(" Response from Calculate Points ", pointsObject.winningTeams);
    }
    catch(err)
    {
      console.log(" ================error_calculateTotalPoints====== ", err);
    }
  } else {
    await Lottery.update(
      {
        winning_teams: [],
        winning_users: [],
      },
      {
        where: {
          id: lotteryId,
        },
      }
    );
    await createEmptyTeamStatsStructure(lotteryId, true);
    await lotteryController.createNewLotteryEvent(lotteryId);
  }
}

async function calculateTotalPoints(
  lotteryId,
  lotteryWinningNumber,
  foundLottery
) {
  console.log("Inside calculateTotalPoints");


  let FUND_DISTRIBUTION_DETAILS=foundLottery.dataValues.fund_distribution;
  
  let totalFund=foundLottery.dataValues.total_fund;
  let fundToBeDistributedToWinningTeam =(totalFund*FUND_DISTRIBUTION_DETAILS.team_event);
  let fundToBeDistributedToWinners =(totalFund*FUND_DISTRIBUTION_DETAILS.prize_pool);
  
  // update ticket poins
  let userPointsList = await sequelize.query(
    `update lottery_user_tickets as ut set total_point = subquery.points
        from (select id,ticket_no,address,team_id,lottery_id,
          (  
            case when (split_part(ticket_no,' ',1) = '${lotteryWinningNumber[0]}' ) then 1 else 0 end 
          +  case when (split_part(ticket_no,' ',2) = '${lotteryWinningNumber[1]}' ) then 1 else 0 end 
          +  case when (split_part(ticket_no,' ',3) = '${lotteryWinningNumber[2]}' ) then 1 else 0 end 
          +  case when (split_part(ticket_no,' ',4) = '${lotteryWinningNumber[3]}' ) then 1 else 0 end 
          +  case when (split_part(ticket_no,' ',5) = '${lotteryWinningNumber[4]}' ) then 1 else 0 end 
          ) 
          as points FROM lottery_user_tickets where lottery_id = ${lotteryId} ) 
          as subquery where ut.lottery_id = subquery.lottery_id AND subquery.ticket_no = ut.ticket_no`,
    { model: LotteryUserTicket }
  );
  //===================================================================================
  //winning team calculations
  let winningTeamsMaxTotalPoints =
    await sequelize.query(`select team_id,sum(total_point) as total_points
       from lottery_user_tickets
       where lottery_id = ${lotteryId}
       group by team_id 
       order by total_points desc`);
  let winningTeams = [];
  let winningTeamNames = [];
  let max = Number(winningTeamsMaxTotalPoints[0][0].total_points);

    if (max < 1) {
      await Lottery.update(
        {
          winning_teams: [],
          winning_users: [],
        },
        {
          where: {
            id: lotteryId,
          },
        }
      );
      await createEmptyTeamStatsStructure(lotteryId, true);
      await lotteryController.createNewLotteryEvent(lotteryId);
      return;
    }
  winningTeamsMaxTotalPoints[0].forEach((element) => {
    if (Number(element.total_points) >= max) {
      
      max = element.total_points;
      if (max > 0) {
        winningTeams.push(element.team_id);
        if (element.team_id == 1) winningTeamNames.push(" Fire ");
        if (element.team_id == 2) winningTeamNames.push(" Water ");
        if (element.team_id == 3) winningTeamNames.push(" Air ");
        if (element.team_id == 4) winningTeamNames.push(" Earth ");
      }
    }
  });

  // formatting above array

  console.log("Winning Teams List ", winningTeams); // can be useful

  console.log("Winning Teams List With Names ", winningTeamNames);

  // picking up addresses with more than 2 tickets.

  let winningTeamIds = "('" + winningTeams.join("','") + "')";
  let numberOfPlayersInWinningTeam =
    await sequelize.query(`select count(distinct(ticket_no)),
     address from lottery_user_tickets where lottery_id = ${lotteryId} 
     and team_id in ${winningTeamIds}
     group by address having count(distinct(ticket_no)) > 1 or sum(total_point) > 0`);
  numberOfPlayersInWinningTeam = numberOfPlayersInWinningTeam[0];


  //Each user from each winning team will get equal prize
  let perUserFund = 0;
  if (numberOfPlayersInWinningTeam.length !== 0) {
    perUserFund =
      fundToBeDistributedToWinningTeam / numberOfPlayersInWinningTeam.length;
  }
  //===================================================================================

  console.log("Points Calculated Of Each User ", userPointsList);

  let top3Points = await sequelize.query(
    `select total_point from lottery_user_tickets where 
    lottery_id = ${lotteryId} and total_point <> 0
    group by total_point order by total_point desc limit 3 `
  );
  top3Points = top3Points[0];
  let lotteryResult = {};
  let winningUsers=[];
  for (let i = 0; i < top3Points.length; i++) {
    let element = top3Points[i];
    let winnerTickets = await sequelize.query(
      `select ticket_no,address,team_id,lottery_order_id from lottery_user_tickets 
      where total_point=${element.total_point} 
      AND lottery_id = ${lotteryId}`
    );
    let ticketInfo = {};
    winnerTickets[0].forEach((ele) => {
      if (ticketInfo[ele.address]) {
        ticketInfo[ele.address].tickets.push({
          ticket_no: ele.ticket_no,
          order_id: ele.lottery_order_id,
          total_point:element.total_point
        });
        ticketInfo[ele.address].total_ticket_point += element.total_point;
      } else {
        if (!winningUsers.includes(ele.address)) {
          winningUsers.push(ele.address);
        }
       
        ticketInfo[ele.address] = {
          tickets: [
            { ticket_no: ele.ticket_no, 
              order_id: ele.lottery_order_id, 
              total_point:element.total_point },
          ],
          team_id: ele.team_id,
          total_ticket_point:element.total_point
        };
      }
    });
    let noOfuserCount = Object.keys(ticketInfo).length;
    let prizePoolValue = 0;
    let description='';
    if (i==0) {
      description="1st winner";
      if( noOfuserCount == 1 && top3Points.length==3)
      {
        prizePoolValue = FUND_DISTRIBUTION_DETAILS.first_winner ;
      }
     else if( noOfuserCount == 1 && top3Points.length==2)
      {
        prizePoolValue = FUND_DISTRIBUTION_DETAILS.first_winner+FUND_DISTRIBUTION_DETAILS.third_winner ;
      }
      else{
        prizePoolValue = FUND_DISTRIBUTION_DETAILS.first_winner+FUND_DISTRIBUTION_DETAILS.second_winner+FUND_DISTRIBUTION_DETAILS.third_winner;
      } 
    }
    else if (i==1) {
      description="2nd winner";
      prizePoolValue = FUND_DISTRIBUTION_DETAILS.second_winner;
    }
    else{
      description="3rd winner";
      prizePoolValue = FUND_DISTRIBUTION_DETAILS.third_winner;
    }
    let winnerFundPool =(totalFund * prizePoolValue);
    let perTicketPrize = (winnerFundPool / winnerTickets[0].length).toFixed(Constants.CURRENCY_DECIMAL)*1;
    for (const [key, value] of Object.entries(ticketInfo)) {
      let orderDetails =
        await sequelize.query(`select sum(total_ticket) as total_ticket, sum(total_price) as total_price
        from lottery_orders 
        where address='${key}' and lottery_id=${lotteryId} group by address`);
        orderDetails=orderDetails[0]
        let teamPrize =0;
        if (
          winningTeams.indexOf(value.team_id) != -1 &&
          (value.tickets.length >= 2 || value.total_ticket_point > 0)
        ) {
          teamPrize = perUserFund;
        }
         // check if address is  a part of winning team
      let ticketsPrize =perTicketPrize * value.tickets.length;
      
      let ticket_prize_info = [];
      value.tickets.map((ticket) => {
        ticket_prize_info.push({
          order_id: ticket.order_id,
          ticket_no: ticket.ticket_no,
          total_point: ticket.total_point,
          prize: Number(perTicketPrize),
          description: description,
        });
      });
     let existingLotteryResult=lotteryResult[key];
      if(existingLotteryResult)
      {
        existingLotteryResult.tickets_prize+=Number(ticketsPrize);
        existingLotteryResult.total_prize+=Number(ticketsPrize);
        existingLotteryResult.ticket_prize_info.push(...ticket_prize_info);
        //existingLotteryResult.team_prize+=Number(teamPrize);
        //existingLotteryResult.total_ticket_price+=Number(orderDetails[0].total_price);
        //existingLotteryResult.total_ticket+=Number(orderDetails[0].total_ticket);
       
       

      }
      else{
       
        lotteryResult[key]={
          lottery_id: lotteryId,
          address: key,
          team_id:value.team_id,
          team_prize: Number(teamPrize),
          tickets_prize: Number(ticketsPrize),
          result_view_status: false,
          total_ticket_price: Number(orderDetails[0].total_price),
          total_ticket: Number(orderDetails[0].total_ticket),
          total_prize: Number(ticketsPrize + teamPrize),
          ticket_prize_info: ticket_prize_info,
          prize_payment_currency:global.currency
        };
      }

      
    }
    if (noOfuserCount > 1 && i == 0) {
      break;
    }
  }

  let updateLotteryTable = await Lottery.update(
    {
      winning_teams: winningTeamNames,
      winning_users: winningUsers,
    },
    {
      where: {
        id: lotteryId,
      },
    }
  );



  let winningUserIds = "('" + winningUsers.join("','") + "')";
  let listOfGhostUser = await sequelize.query(`
    select address from user_accounts
    where user_accounts.pic_url = '' and address in ${winningUserIds}
    `);

  listOfGhostUser = listOfGhostUser[0];

  let listOfGhostUserArray = [];
  listOfGhostUser.forEach((element) => {
    listOfGhostUserArray.push(element.address);
  });

  console.log(`final_lottery_result_array[${lotteryId}]`, lotteryResult);
  for (const [key, value] of Object.entries(lotteryResult)) {
    let result = value;
    result.prize_payment_status = Constants.PRIZE_PAYMENT_STATUS.PENDING;
    if (listOfGhostUserArray.indexOf(result.address) != -1) {
      result.prize_payment_status =
        Constants.PRIZE_PAYMENT_STATUS.PAYMENT_CLAIM_PENDING;
    }
    try {
      let lotteryCreated=await LotteryResults.create(result);
      console.log(`lotteryResultCreated[${lotteryId}]`, lotteryCreated);
    } catch (err) {
      console.log(`error_creating_lottery_result[${lotteryId}]`, err);
    }
  }
  

  let userDetails = await sequelize.query(
    `select user_account_id, address, cast(count(ticket_no) as integer) as tradeVolume, 
    cast(sum(total_point) as integer) as totalPoint from lottery_user_tickets 
    where lottery_id = ${lotteryId} 
    group by user_account_id,address `
  );
 
  userDetails = userDetails[0];

  console.log(`user_details[${lotteryId}]`, userDetails);
  for (let i = 0; i < userDetails.length; i++) {
    const element = userDetails[i];
    let winCount=winningUsers.includes(element.address)?1:0;
    await UserAccount.update(
      {
        total_trade_volume: sequelize.literal(
          `total_trade_volume + ${element.tradevolume}`
        ),
        total_point: sequelize.literal(`total_point + ${element.totalpoint}`),
        total_win_cnt: sequelize.literal(`total_win_cnt + ${winCount}`),
      },
      { where: { id: element.user_account_id } }
    );
  }

  await Team.update(
    {
      win_cnt: sequelize.literal(`win_cnt + 1`),
    },
    {
      where: { team_id: winningTeams },
    }
  );
 
  await createEmptyTeamStatsStructure(lotteryId, false);

  return {
    winningTeams: winningTeams,
  };
  }



async function createEmptyTeamStatsStructure(
  lotteryId,
  emptyLotteryResultFlow
) {
  let numberOfTeams = await Team.findAll({});
  console.log("numberOfTeams ", numberOfTeams.length);

  for(let teamIndex=0;teamIndex<numberOfTeams.length;teamIndex++)
  {
    let element=numberOfTeams[teamIndex];
    console.log("Element", element);
    await TeamStat.create({
      team_id: element.dataValues.team_id,
      lottery_id: lotteryId,
      user_cnt: 0,
      ticket_cnt: 0,
      total_point: 0,
      rank: 0,
      total_trade_currency: global.currency,
      total_prize_amount: 0,
    });
    console.log("elementEnd");
  }
  if (!emptyLotteryResultFlow) {
    await updateTeamStatTable(lotteryId);
  }
}

async function updateTeamStatTable(lotteryId) {
  console.log("Inside Update Team Stat Table ");
  let foundTeamDetails = await sequelize.query(`select teamStat.*,COALESCE(teamPrize.total_prize_amount,0) as total_prize_amount
  from
  (
  select team_id,
  cast(count(distinct(address)) as integer) as user_cnt ,
  cast(count(ticket_no)as integer) as ticket_cnt,
  cast(sum(total_point)as integer) as totalPoint
  from lottery_user_tickets where lottery_id = ${lotteryId} 
  group by team_id
  )
  as teamStat left join
  (
  select team_id,
  sum(total_prize) as total_prize_amount
  from lottery_results where lottery_id = ${lotteryId} 
  group by team_id
  )
  as teamPrize
  on teamStat.team_id = teamPrize.team_id
  order by totalpoint desc`);

  console.log("teamDetails Team Stat Table ", foundTeamDetails);
  foundTeamDetails = foundTeamDetails[0];

  console.log(" Found Team Details ", foundTeamDetails);
  for (let i = 0; i < foundTeamDetails.length; i++) {
    let teamStat = await TeamStat.update(
      {
        team_id: foundTeamDetails[i].team_id,
        lottery_id: lotteryId,
        user_cnt: Number(foundTeamDetails[i].user_cnt),
        total_point: Number(foundTeamDetails[i].totalpoint),
        ticket_cnt: Number(foundTeamDetails[i].ticket_cnt),
        total_prize_amount: Number(foundTeamDetails[i].total_prize_amount),
      },
      { where: { team_id: foundTeamDetails[i].team_id, lottery_id: lotteryId } }
    );
    await Team.update(
      {
        total_point: sequelize.literal(
          `total_point + ${Number(foundTeamDetails[i].totalpoint)}`
        ),
      },
      {
        where: { team_id: foundTeamDetails[i].team_id },
      }
    );
  }
  await lotteryController.createNewLotteryEvent(lotteryId);
}


async function confirmTransferToUser(txHash) {
  // Get current number of confirmations and compare it with sought-for value
 
  let trxDetails = await web3.eth.getTransaction(txHash);
  if (trxDetails == null) 
  {
    console.log(`Transaction ${txHash} not found`)
    return;
  }
  const currentBlock = await web3.eth.getBlockNumber();
  let trxConfirmations=trxDetails.blockNumber === null ? 0 : currentBlock - trxDetails.blockNumber;


  if (trxConfirmations >= Constants.SEND_CONFIRM_BLOCK_COUNT) {
    // Handle confirmation event according to your business logic
    console.log(
      "Transaction with hash " + txHash + " has been successfully confirmed"
    );

  const trxReceipt = await web3.eth.getTransactionReceipt(txHash);

  let trxGasFee = await web3.utils.fromWei(trxDetails.gasPrice, "ether");
  let trxCharge = trxReceipt.gasUsed * JSON.parse(trxGasFee);
  console.log("Transaction Charges ", trxCharge);


    const item = await LotteryResults.update(
      {
        prize_payment_fee: trxCharge,
        prize_payment_status: Constants.PRIZE_PAYMENT_STATUS.COMPLETED,
      },
      {
        where: { prize_payment_id: txHash },
      }
    );

    return;
  }
  // Recursive call
  return await confirmTransferToUser(txHash);
}

exports.confirmTransferToUser = confirmTransferToUser;
exports.updateTeamStatTable = updateTeamStatTable;
exports.calculateTotalPoints = calculateTotalPoints;
exports.getResult = getResult;
exports.lotteryPrizePayment = lotteryPrizePayment;
exports.resultScheduler = resultScheduler;
exports.lotteryPrizePaymentConfirmationScheduler=lotteryPrizePaymentConfirmationScheduler;
