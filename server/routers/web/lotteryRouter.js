const express = require('express')
const router = express.Router();
const lotteryController = require('../../controllers/web/lotteryController');
const exportExcel = require('../../controllers/web/exportExcelContoller');
const notificationController = require('../../controllers/web/notificationController');

router.post('/recordUserOrderRequest', function (req, res) {
    console.log("Recording User Order Transaction ", req.body);   
    lotteryController.recordUserOrderRequest(req, res);
})

router.post('/confirmAmountReceivedFromUser', function (req, res) {
    console.log("Recording User Order Transaction ", req.body);   
    lotteryController.confirmAmountReceivedFromUser(req, res);
})

router.post('/checkOrderStatus', function (req, res) {
    console.log("Recording User Order Transaction ", req.body);   
    lotteryController.checkOrderStatus(req, res);
})




router.post('/getUserLotteryTicketDetails', function (req, res) {
    console.log("Fetching User Lottery Ticket Details ", req.body);  
    lotteryController.getUserLotteryTicketDetails(req, res)

})



router.post('/fetchPresentPricePerTicket', function (req, res) {
    console.log(" fetching Present Price Per Ticket ", req.body);
    
    lotteryController.fetchPresentPricePerTicket(req, res)

})

router.post('/fetchLotteryWinningNumber', function (req, res) {
    console.log(" fetching Lottery Winning Number ", req.body);
    lotteryController.fetchLotteryWinningNumber(req, res)

})



router.post('/getLotteryEventPrizePoolDetails', function (req, res) {
    console.log(" Fetching Lottery Pool Prize ", req.body);
    lotteryController.getLotteryEventPrizePoolDetails(req, res)

})

router.post('/getAllLotteryEventHistoryDetails', function (req, res) {
    console.log(" Fetching All Past Lotteries Event History Details ", req.body);
    lotteryController.getAllLotteryEventHistoryDetails(req, res)
})


router.post('/getLatestLotteryId', function (req, res) {
    console.log(" Fetching Current Lottery Id ");
    lotteryController.getLatestLotteryId(req, res);
})

router.post('/getAllLotteryTicketOrders', function (req, res) {
    console.log(" Fetching All  Orders  From  User  ");
    lotteryController.getAllLotteryTicketOrders(req, res);
})


router.post('/getAllTransactions', function (req, res) {
    console.log(" Fetching All  Transactions  For  User  ");
    lotteryController.getAllTransactions(req, res);
})


router.post('/getSelectedOrderDetails', async function (req, res) {
    console.log("Getting Order Details",req.body)
    lotteryController.getSelectedOrderDetails(req, res)
})

router.post('/getAllLotteryTicketPoints', async function (req, res) {
    console.log("Getting Points Calculation Details",req.body)
    lotteryController.getAllLotteryTicketPoints(req, res)
})

router.post('/getCreditDetails', async function (req, res) {
    console.log("Getting Credit Amount Details",req.body)
    lotteryController.getCreditDetails(req, res)
})

router.post('/getReceivedOrderDetails', async function (req, res) {
    console.log("Getting Received Order Details",req.body)
    lotteryController.getReceivedOrderDetails(req, res)
})

router.post('/getAllNft', async function (req, res) {
    console.log("Getting All NFTs",req.body)
    lotteryController.getAllNft(req, res)
})

router.post('/checkNFTTransferStatus', async function (req, res) {
    console.log("Checking nft status",req.body)
    lotteryController.checkNFTTransferStatus(req, res)
})

router.post('/placeTicketTransferOrder', async function (req, res) {
    console.log("Placing Ticket Transfer Order",req.body)
    lotteryController.tradeTicket(req, res)
})

router.post('/verifyApprovalTransaction', async function (req, res) {
    console.log("verifyApprovalTransaction ",req.body)
    lotteryController.verifyApprovalTransaction(req, res)
})

router.post('/fetchPendingUserOrders', function (req, res) {
    console.log("Fetching Pending User Orders", req.body);   
    lotteryController.fetchPendingUserOrders(req, res);
})

router.post('/updateShowCredit', function (req, res) {
    console.log("Update Credit Alert", req.body);   
    lotteryController.updateShowCredit(req, res);
})

router.post('/updateNftTransferProcess', function (req, res) {
    console.log("Update Lottery Owner", req.body);   
    lotteryController.updateNftTransferProcess(req, res);
})

router.get('/exportExcel', function (req, res) {
    console.log("exportExcel", req.body);   
    exportExcel.exportExcel(req, res);
})

router.post('/getExtentedOrderList', function (req, res) {
    console.log("getExtentedOrderList", req.body);   
    lotteryController.getExtentedOrderList(req, res);
})

router.post('/getAllNotificationEventDetails', function (req, res) {
    console.log("getAllNotificationEventDetails", req.body);   
    notificationController.getAllNotificationEventDetails(req, res);
})

module.exports = router

