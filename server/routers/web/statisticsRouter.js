const express = require('express')
const router = express.Router();
const statisticsController = require('../../controllers/web/statisticsController');

router.post('/getTeamStats', function (req, res) {
    console.log(" fetching Team stats ", req.body);
    statisticsController.getTeamStats(req, res)

})
router.post('/getTopUserStats', function (req, res) {
    console.log(" Fetching All Top Users ", req.body);
    statisticsController.getTopUserStats(req, res);
})

module.exports = router

