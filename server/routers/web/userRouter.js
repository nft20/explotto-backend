const express = require('express')
const router = express.Router();
const userController = require('../../controllers/web/userController');

router.post('/getUserProfileDetails', function (req, res) {
    console.log("Fetching User Profile Details", req.body);
    userController.getUserProfileDetails(req, res);
})

router.post('/saveUserProfileDetails', function (req, res) {
    console.log("Saving User Profile Details", req.body);   
    userController.saveUserProfileDetails(req, res)
})

router.post('/saveUserProfileAvatar', function (req, res) {
    console.log("Saving User Profile Avatar", req.body);   
    userController.saveUserProfileAvatar(req, res)
})
router.post('/confirmAndUpdateAvatar', function (req, res) {
    console.log("Confirming and Updating User Avatar", req.body);   
    userController.confirmAndUpdateAvatar(req, res)

})
router.post('/confirmTransactionForProfileUpdate', function (req, res) {
    console.log(" Confirming Transaction Hash ", req.body); 
    userController.confirmTransactionForProfileUpdate(req, res)

})


router.post('/getUserLotteryTicketDetails', function (req, res) {
    console.log("Fetching User Lottery Ticket Details ", req.body);  
    lotteryController.getUserLotteryTicketDetails(req, res)

})


    


module.exports = router

