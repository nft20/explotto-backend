const express = require('express')
const router = express.Router();
const adminController = require('../../controllers/admin/adminLoginController');
const tableController = require('../../controllers/admin/tableController');
const configurationController = require('../../controllers/admin/configurationController');
const userController = require('../../controllers/admin/userController');
const walletController = require('../../controllers/admin/walletController');
const dashboardController = require('../../controllers/admin/dashboardController');
const roleAccessController = require('../../controllers/admin/roleAccessController');
const lotteryResultController = require('../../controllers/admin/lotteryResultController');
const checkRole = require('../../middleware/admin/checkRole');
const auth = require('../../middleware/admin/authMiddleware');

router.post('/login',async function (req, res, next) {
    console.log("Login",req.body)
    adminController.login(req, res)
})

router.post('/forgotPassword', async function (req, res, next) {
    console.log("forgotPassword",req.body)
    adminController.forgotPassword(req, res)
})

router.post('/verifyOtp', async function (req, res, next) {
    console.log("verifyOtp",req.body)
    adminController.verifyOtp(req, res)
})

router.post('/resetPassword', async function (req, res, next) {
    console.log("resetPassword",req.body)
    adminController.resetPassword(req, res)
})

router.post('/getAllLotteryEventDetails',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAllLotteryEventDetails",req.body)
    tableController.getAllLotteryEventDetails(req, res)
})

router.post('/getAdminAllLotteryOrders',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAdminAllLotteryOrders",req.body)
    tableController.getAdminAllLotteryOrders(req, res)
})

router.post('/getAdminSelectedOrderDetails',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAdminSelectedOrderDetails",req.body)
    tableController.getAdminSelectedOrderDetails(req, res)
})

router.post('/getAdminTotalFeeAndNFTFee',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAdminTotalFeeAndNFTFee",req.body)
    tableController.getAdminTotalFeeAndNFTFee(req, res)
})

router.post('/getAdminAllProperties',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAdminAllProperties",req.body)
    configurationController.getAdminAllProperties(req, res)
})

router.post('/addAdminProperties',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("addAdminProperties",req.body)
    configurationController.addAdminProperties(req, res)
})

router.post('/updateAdminProperties',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("updateAdminProperties",req.body)
    configurationController.updateAdminProperties(req, res)
})

router.post('/getAllUsers',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAllUsers",req.body)
    userController.getAllUsers(req, res)
})

router.post('/adminCreateUser',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("adminCreateUser",req.body)
    userController.adminCreateUser(req, res)
})

router.post('/disableUser',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("disableUser",req.body)
    userController.disableUser(req, res)
})

router.post('/getAllAdminAccounts',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAllAdminAccounts",req.body)
    walletController.getAllAdminAccounts(req, res)
})

router.post('/verifySecretKey',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("verifySecretKey",req.body)
    walletController.verifySecretKey(req, res)
})

router.post('/loteryStat',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("loteryStat",req.body)
    dashboardController.loteryStat(req, res)
})

router.post('/getAdminTotalExpense',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAdminTotalExpense",req.body)
    dashboardController.getAdminTotalExpense(req, res)
})

router.post('/getAllApis',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAllApis",req.body)
    roleAccessController.getAllApis(req, res)
})

router.post('/addRoleAccess',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("addRoleAccess",req.body)
    roleAccessController.addRoleAccess(req, res)
})

router.post('/getAllLotteryResult',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getAllLotteryResult",req.body)
    lotteryResultController.getAllLotteryResult(req, res)
})

router.post('/changeAvailibilityStatus',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("changeAvailibilityStatus",req.body)
    walletController.changeAvailibilityStatus(req, res)
})

router.post('/changeOrderStatus',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("changeOrderStatus",req.body)
    tableController.changeOrderStatus(req, res)
})

router.post('/getUserAndPic',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("getUserAndPic",req.body)
    tableController.getUserAndPic(req, res)
})

router.post('/changePaymentStatus',[auth.verifyToken, checkRole.verifyRole], async function (req, res, next) {
    console.log("changePaymentStatus",req.body)
    lotteryResultController.changePaymentStatus(req, res)
})

module.exports = router;