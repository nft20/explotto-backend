const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket,
  LotteryResults,
  Notification
} = require("../../models");
const path = require('path');
const fs = require('fs');
const lotteryController = require('./lotteryController');
const Op = db.Sequelize.Op;



exports.getAllNotificationEventDetails = async function (req, res) {
  console.log("getAllNotificationEventDetails");
  
  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit;
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset");
    return;
  }

  const valid = req.validate(
    [
      { field: "address", type: "STRING", isRequired: true },
    ],
    req.body
  );

  if (!valid) {
    return res.sendError();
  }

  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    let totalCount = await sequelize.query(`select count(id) from notifications where event ilike '%${req.body.searchText}%' and address = '${req.body.address}' `);
    totalCount = parseInt(totalCount[0][0].count);
  
    if (totalCount > 0) {
      const notifications = await sequelize.query(`select * from notifications where event ilike '%${req.body.searchText}%' and address = '${req.body.address}' order by id Desc limit ${limit} offset ${offset}`);
      console.log("All Notifications by serach ", notifications[0]);
      return res.send({
        totalCount: totalCount,
        list: notifications[0],
      });
    } else {
      return res.send({
        totalCount: 0,
        list: [],
      });
    }
  }

  let totalCount = await sequelize.query(`select count(id) from notifications where address = '${req.body.address}' `);
  totalCount = parseInt(totalCount[0][0].count);

  if (totalCount > 0) {
    const notifications = await Notification.findAll({
      where: { address: req.body.address },
      order: [["createdAt", "DESC"]],
      limit: limit,
      offset: offset,
    });
    console.log("All Notifications ", notifications);
    return res.send({
      totalCount: totalCount,
      list: notifications,
    });
  } else {
    return res.send({
      totalCount: 0,
      list: [],
    });
  }
};