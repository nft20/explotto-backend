// const User = require("../Models/User"); // This has data to be used
const excelJS = require("exceljs");
const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket,
  LotteryResults,
  Notification
} = require("../../models");
const path = require('path');
const fs = require('fs');
const lotteryController = require('./lotteryController');
const Op = db.Sequelize.Op;

async function exportOrder(req, res){
  const workbook = new excelJS.Workbook(); // Create a new workbook
  const worksheet = workbook.addWorksheet("My Orders"); // New Worksheet
  var fileName = 'orders.xlsx';
  // Column for data in excel. key must match data key
  worksheet.columns = [
    { header: "Order Id", key: "id", width: 10 },
    { header: "Lottery Id", key: "lottery_id", width: 10 },
    { header: "Total Tickets", key: "total_ticket", width: 10 },
    { header: "Total Amount", key: "total_price", width: 10 },
    { header: "Currency", key: "total_price_currency", width: 10 },
    { header: "Status", key: "status", width: 10 },
    { header: "Time", key: "createdAt", width: 10 },
  ];

  let lotteryOrders = await LotteryOrders.findAll({
    where: { address: req.query.address },
    order: [
      ["id", "DESC"],
      ["createdAt", "DESC"],
    ],
  });

  // Looping through User data
  let counter = 1;
  lotteryOrders.forEach((orders) => {
    orders.s_no = counter;
    worksheet.addRow(orders); // Add data in worksheet
    counter++;
  });
  // Making first line in excel bold
  worksheet.getRow(1).eachCell((cell) => {
    cell.font = { bold: true };
  });
  try {
      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + "orders.xlsx"
      );
      return workbook.xlsx.write(res).then(function () {
        res.status(200).end();
      });

  } catch (err) {
    return res.send({
      status: "error",
      message: "Something went wrong",
    });
  }
};

async function exportCreditHistory(req, res){
  const workbook = new excelJS.Workbook(); // Create a new workbook
  const worksheet = workbook.addWorksheet("My Credit History"); // New Worksheet
  var fileName = 'Credit_History.xlsx';
  // Column for data in excel. key must match data key
  worksheet.columns = [
    { header: "Lottery Id", key: "lottery_id", width: 10 },
    { header: "Transaction Id", key: "prize_payment_id", width: 10 },
    { header: "Tickets Purchased", key: "total_ticket", width: 10 },
    { header: "Description", key: "ticket_prize_info", width: 10 },
    { header: "Amount Credited(BNB)", key: "total_prize", width: 10 },
    { header: "Time", key: "createdAt", width: 10 },
  ];

  let userTransactions = await LotteryResults.findAll({
    attributes: [
      "prize_payment_id",
      "prize_payment_currency",
      "lottery_id",
      "ticket_prize_info",
      "prize_payment_time",
      "total_prize",
      "total_ticket",
      "team_prize",
      "createdAt"
    ],
    where: { address: req.query.address, prize_payment_id: { [Op.ne]: "" } },
    order: [["lottery_id", "DESC"]],
  });

  // Looping through User data
  let counter = 1;
  userTransactions.forEach((orders) => {
    orders.s_no = counter;
    worksheet.addRow(orders); // Add data in worksheet
    counter++;
  });
  // Making first line in excel bold
  worksheet.getRow(1).eachCell((cell) => {
    cell.font = { bold: true };
  });
  try {

      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + "Credit_History.xlsx"
      );
      return workbook.xlsx.write(res).then(function () {
        res.status(200).end();
      });
   
  } catch (err) {
    return res.send({
      status: "error",
      message: "Something went wrong",
    });
  }
};

async function exportPointsHistory(req, res){
  const workbook = new excelJS.Workbook(); // Create a new workbook
  const worksheet = workbook.addWorksheet("My Points History"); // New Worksheet
  var fileName = 'Points_History.xlsx';
  // Column for data in excel. key must match data key
  worksheet.columns = [
    { header: "Lottery Id", key: "lottery_id", width: 10 },
    { header: "Winning Number", key: "winning_no", width: 10 },
    { header: "Your Tickets Number", key: "ticket_no", width: 10 },
    { header: "Points Earned", key: "total_point", width: 10 },
  ];

  let activeLotteryId = await lotteryController.getLatestLotteryId();

  let userPoints = await sequelize.query(`select 
  lottery_user_tickets.lottery_id,
  lottery_user_tickets.ticket_no,
  lottery_user_tickets.total_point, 
  lotteries.winning_no
  from lottery_user_tickets
  inner join lotteries 
  on lottery_user_tickets.lottery_id = lotteries.id 
  where lotteries.id <> ${activeLotteryId} 
  AND lottery_user_tickets.address = '${req.query.address}'
  group by lottery_user_tickets.lottery_id, 
  lottery_user_tickets.ticket_no,
  lottery_user_tickets.total_point, 
  lotteries.winning_no
  order by lottery_user_tickets.lottery_id desc,lottery_user_tickets.total_point desc 
  `);

  console.log("All Ticket Points of User ", userPoints);
  userPoints = userPoints[0];

  // Looping through User data
  let counter = 1;
  userPoints.forEach((orders) => {
    orders.s_no = counter;
    worksheet.addRow(orders); // Add data in worksheet
    counter++;
  });
  // Making first line in excel bold
  worksheet.getRow(1).eachCell((cell) => {
    cell.font = { bold: true };
  });
  try {
    // const data = await workbook.xlsx
    //   .writeFile(path.join(__dirname + `/files/Points_History.xlsx`));

    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=" + "Points_History.xlsx"
    );
    return workbook.xlsx.write(res).then(function () {
      res.status(200).end();
    });
  } catch (err) {
    return res.send({
      status: "error",
      message: "Something went wrong",
    });
  }
};

async function exportReceievdTickets(req, res){
  const workbook = new excelJS.Workbook(); // Create a new workbook
  const worksheet = workbook.addWorksheet("Receievd Tickets"); // New Worksheet
  var fileName = 'Receievd_Tickets.xlsx';
  // Column for data in excel. key must match data key
  worksheet.columns = [
    { header: "Lottery Id", key: "lottery_id", width: 10 },
    { header: "Ticket Number", key: "ticket_no", width: 10 },
    { header: "Status", key: "ticket_status", width: 10 },
    { header: "Time", key: "createdAt", width: 10 },
  ];

  let userOrderDetails = await sequelize.query(`select 
  lottery_user_tickets.lottery_id, 
  lottery_user_tickets.lottery_order_id,
  lottery_user_tickets.ticket_no,
  lottery_user_tickets.ticket_status,
  lottery_user_tickets.nft_tx_id,
  lottery_user_tickets."createdAt",
  lottery_user_tickets.ticket_filename,
  lottery_user_tickets.nft_tx_currency
  from lottery_user_tickets 
  where lottery_user_tickets.lottery_order_id = -1 AND
  lottery_user_tickets.address = '${req.query.address}'
  `);

  userOrderDetails = userOrderDetails[0];

  // Looping through User data
  let counter = 1;
  userOrderDetails.forEach((orders) => {
    orders.s_no = counter;
    worksheet.addRow(orders); // Add data in worksheet
    counter++;
  });
  // Making first line in excel bold
  worksheet.getRow(1).eachCell((cell) => {
    cell.font = { bold: true };
  });
  try {
    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=" + "Receievd_Tickets.xlsx"
    );
    return workbook.xlsx.write(res).then(function () {
      res.status(200).end();
    });
  } catch (err) {
    return res.send({
      status: "error",
      message: "Something went wrong",
    });
  }
};

async function exportOrderDetailsList(req, res){
  const workbook = new excelJS.Workbook(); // Create a new workbook
  const worksheet = workbook.addWorksheet("Order Details List"); // New Worksheet
  var fileName = 'Order_Details_list.xlsx';
  // Column for data in excel. key must match data key
  worksheet.columns = [
    { header: "Order Id", key: "lottery_order_id", width: 10 },
    { header: "Lottery Id", key: "lottery_id", width: 10 },
    { header: "Ticket Number", key: "ticket_no", width: 10 },
    { header: "Amount Paid", key: "per_ticket_price", width: 10 },
    { header: "Currency", key: "pay_tx_currency", width: 10 },
    { header: "Status", key: "ticket_status", width: 10 },
    { header: "Time", key: "createdAt", width: 10 },
  ];

  let userOrderDetails = await sequelize.query(`select 
  lottery_user_tickets.lottery_id, 
  lottery_user_tickets.lottery_order_id,
  lottery_user_tickets.ticket_no,
  lottery_user_tickets.ticket_status,
  lottery_user_tickets.nft_tx_id,
  lottery_user_tickets.ticket_filename,
  lottery_orders.pay_tx_currency,
  lottery_orders.pay_tx_id,
  lottery_orders."createdAt",
  ROUND(lottery_orders.total_price/lottery_orders.total_ticket, 6) as per_ticket_price
  from lottery_user_tickets 
  inner join lottery_orders on lottery_user_tickets.lottery_order_id = lottery_orders.id
  where lottery_user_tickets.lottery_order_id = ${req.query.id} AND
  lottery_user_tickets.lottery_id = ${req.query.lotteryId} AND
  lottery_user_tickets.address = '${req.query.address}'
  `);

  userOrderDetails = userOrderDetails[0];

  // Looping through User data
  let counter = 1;
  userOrderDetails.forEach((orders) => {
    orders.s_no = counter;
    worksheet.addRow(orders); // Add data in worksheet
    counter++;
  });
  // Making first line in excel bold
  worksheet.getRow(1).eachCell((cell) => {
    cell.font = { bold: true };
  });
  try {
  res.setHeader(
    "Content-Type",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  );
  res.setHeader(
    "Content-Disposition",
    "attachment; filename=" + "Order_Details_list.xlsx"
  );
  return workbook.xlsx.write(res).then(function () {
    res.status(200).end();
  });

  } catch (err) {
    return res.send({
      status: "error",
      message: "Something went wrong",
    });
  }
};

async function exportNotification(req, res){
  const workbook = new excelJS.Workbook(); // Create a new workbook
  const worksheet = workbook.addWorksheet("My Credit History"); // New Worksheet
  // Column for data in excel. key must match data key
  worksheet.columns = [
    { header: "Event", key: "event", width: 10 },
    { header: "Details", key: "details", width: 10 },
    { header: "Time", key: "createdAt", width: 15 },
  ];

  let notification = await Notification.findAll({
    where: { address: req.query.address.toLowerCase() },
    order: [["createdAt", "DESC"]],
  });

  // Looping through User data
  let counter = 1;
  notification.forEach((item) => {
    item.s_no = counter;
    worksheet.addRow(item); // Add data in worksheet
    counter++;
  });
  // Making first line in excel bold
  worksheet.getRow(1).eachCell((cell) => {
    cell.font = { bold: true };
  });
  try {

      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + "Notification.xlsx"
      );
      return workbook.xlsx.write(res).then(function () {
        res.status(200).end();
      });
   
  } catch (err) {
    return res.send({
      status: "error",
      message: "Something went wrong",
    });
  }
};

exports.exportExcel = async function (req, res){
  console.log(req.query);

  if (Object.keys(req.query).length === 0) {
    return res.send("Empty query");
  }

  const valid = req.validate(
    [
      { field: "type", type: "STRING", isRequired: true },
      { field: "address", type: "STRING", isRequired: true },
    ],
    req.query
  );

  if (!valid) {
    return res.sendError();
  }

  if(req.query.type === "orderList"){
    exportOrder(req,res);
  }
  else if(req.query.type === "orderDetailsList"){
    exportOrderDetailsList(req,res);
  }
  else if(req.query.type === "pointsHistoryList"){
    exportPointsHistory(req,res);
  }
  else if(req.query.type === "receievedTicketsList"){
    exportReceievdTickets(req,res);
  } 
  else if(req.query.type === "creditHistoryList"){
    exportCreditHistory(req,res);
  }
  else if(req.query.type === "notificationList"){
    exportNotification(req,res);
  } else{
    return res.sendError("Invalid List Type");
  }
};