const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket
} = require("../../models");
require("dotenv").config();
const lotteryController=require("./lotteryController")

async function getTeamStats(req, res) {
    console.log("Inside getTeamStats ");
    let teamStats;
    if (req.body.lastLotteryTabActive) {
      let lastLotteryId = await lotteryController.getLatestLotteryId();
  
      teamStats = await sequelize.query(`select 
      team_stats.team_id, teams.name, 
      teams.pic_url, team_stats.total_point, 
      team_stats.user_cnt, 
      dense_rank() over(partition by 1 order by team_stats.total_point desc) as rank 
      from team_stats
      inner join teams on teams.id = team_stats.team_id
      where team_stats.lottery_id = (select MAX(id) from lotteries where id <> ${lastLotteryId})
      order by team_stats.total_point desc`);
      console.log(" teamStats ", teamStats);
      teamStats = teamStats[0];
      res.send(teamStats);
    } else {
      teamStats = await sequelize.query(
        `select team_id,name, pic_url, total_point, win_cnt, user_cnt, dense_rank() over(partition by 1 order by total_point desc) as rank from teams order by total_point desc`
      );
      console.log(" teamStats ", teamStats);
      teamStats = teamStats[0];
      res.send(teamStats);
    }
  }
  
  async function getTopUserStats(req, res) {
    console.log("Inside getTopUserStats");
  
    let offset = req.body.pageNumber * 100;
    let userStats;
    if (req.body.lastLotteryTabActive) {
      let lastLotteryId = await lotteryController.getLatestLotteryId();
  
      userStats = await sequelize.query(
        `select 
        user_accounts.username as user_name,
        user_accounts.address,
        user_accounts.pic_url as user_image_url,
        teams.name as team_name, 
        teams.pic_url as team_image_url,
        sum(lottery_user_tickets.total_point) as total_point,
        dense_rank() over(partition by 1 order by max(lottery_user_tickets.total_point) desc) as rank,
        ( user_accounts.address = ANY (lotteries.winning_users::text[])) as is_winner
        from lottery_user_tickets
        inner join teams on lottery_user_tickets.team_id = teams.team_id 
        inner join user_accounts on user_accounts.id = lottery_user_tickets.user_account_id 
        inner join lotteries on lotteries.id=lottery_user_tickets.lottery_id
        where lottery_user_tickets.lottery_id = (select MAX(id) from lotteries where id <> ${lastLotteryId}) and lottery_user_tickets.total_point > 0
        group by 
          lotteries.winning_users,
          user_accounts.username,
          user_accounts.address,
          user_accounts.pic_url,
          teams.name, 
          teams.pic_url 
        order by rank asc,total_point desc
        limit 100 offset ${offset}`
      );
  
      console.log(" userStats ", userStats);
      userStats = userStats[0];
      res.send(userStats);
    } else {
      userStats =
        await sequelize.query(`select user_accounts.id,user_accounts.address,user_accounts.username as user_name, user_accounts.pic_url as user_image_url, user_accounts.total_point, user_accounts.total_win_cnt, teams.name as team_name, teams.pic_url as team_image_url, dense_rank() over(partition by 1 order by user_accounts.total_win_cnt desc) as rank
      from user_accounts
      inner join teams on user_accounts.team_id = teams.team_id where user_accounts.total_point > 0 order by user_accounts.total_win_cnt desc limit 100 offset ${offset}`);
  
      console.log(" userStats ", userStats);
      userStats = userStats[0];
      res.send(userStats);
    }
  }


exports.getTeamStats = getTeamStats;
exports.getTopUserStats = getTopUserStats;