const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket,
  LotteryResults,
  Notification
} = require("../../models");
require("dotenv").config();
const commonUtil = require('../../utils/commonUtil');
const blockChainService = require("../../services/blockChainService");
const ipfsService=require("../../services/ipfsService");
const userController = require("./userController");
const Constants = require('../../Constants');
var moment = require('moment');
const Op = db.Sequelize.Op;

const fs = require("fs");

const web3=global.web3;

const nftContractABI = global.nftContractABI;
const date = require("date-and-time");
const { constants } = require("buffer");

exports.recordUserOrderRequest = async function (req, res) {
  console.log(" Recording the transaction ", req.body);

  const valid = req.validate(
    [
      { field: "totalPrice", type: "NUMBER", isRequired: true },
      { field: "numberOfTickets", type: "NUMBER", isRequired: true },
    ],
    req.body
  );
  if (!valid) {
    return res.sendError();
  }

  if (JSON.parse(req.body.totalPrice) === 0) {
    res.status(400).send("invalid_amount");
    return
  }
  else if (req.body.numberOfTickets<1||req.body.numberOfTickets>100) {
    res.status(400).send("invalid_noOfTickets");
    return
  }

  const foundReq = await LotteryOrders.findOne({
    where: {
      pay_tx_id: req.body.txId,
    },
  });

  if (foundReq != null) {
    res.send("order_exists");
    return;
  }

  const foundUser = await UserAccount.findOne({
    where: { address: req.body.address },
  });
  if (foundUser == null) {
    res.send("unreg_user");
    return;
  } else {
    console.log("Found User", foundUser.dataValues);

    const item = await LotteryOrders.create({
      lottery_id: req.body.lotteryId,
      user_account_id: foundUser.dataValues.id,
      address: foundUser.dataValues.address,
      team_id: foundUser.dataValues.team_id,
      total_ticket: req.body.numberOfTickets,
      total_price: JSON.parse(req.body.totalPrice),
      total_price_currency: global.currency,
      pay_tx_id: req.body.txId,
      pay_tx_fee: null,
      pay_tx_currency: global.currency,
      status: Constants.ORDER_STATUS.CREATED,
      order_type:Constants.ORDER_TYPE.BUY
    });
    res.send("req_recorded");
  }
};

exports.confirmAmountReceivedFromUser = async function (req, res) {
  console.log(" Confirming the transaction ", req.body);
  let trx;
  if (req.body.numberOfTickets > 100) {
    res.send("invalid_ticket_count");
    return;
  }
  let currentLotteryId = await getLatestLotteryId();
  if (req.body.lotteryId != currentLotteryId) {
    res.send("invalid_lottery_id");
    return;
  }
  try {
    setTimeout(async () => {
      trx = await web3.eth.getTransaction(req.body.txId);
      if (trx == null) {
        res.send("invalid_trx");
        return;
      }
      let amountPaid = req.body.totalPrice;
      var tokens = web3.utils.toWei(amountPaid, "ether");
      if (trx.value != tokens) {
        res.send("invalid_price_paid");
        return;
      }
    }, 15 * 1000)
    
  } catch (error) {
    res.send("invalid_trx");
    return;
  }

  let orderDetails = await sequelize.query(
    `select pay_tx_id from lottery_orders where pay_tx_id = '${req.body.txId}'`
  );
  orderDetails = orderDetails[0];
  if (orderDetails.length != 0) {
    res.send("order_exists");
    return;
  } else {
    if (trx.from.toLowerCase() == req.body.address.toLowerCase()) {
      let response = await confirmOrderPaymentTransaction(req, res);
      console.log(" Response from Confirm Transaction ", response);
    } else {
      res.send("invalid_user");
    }
  }
};

async function checkOrderStatus(req, res) {
  console.log("Checking Order Statuses", req.body.data);
  let trxArray = req.body.data;
  let trxStatusArray = [];

  for (let i = 0; i < trxArray.length; i++) {
    let trx = trxArray[i];
    // trx = JSON.parse(trx);
    let orderStatus = await sequelize.query(
      `select * from lottery_orders where pay_tx_id = '${trx}'`
    );
    orderStatus = orderStatus[0][0];
    if (orderStatus) {
      if (orderStatus.status == Constants.ORDER_STATUS.PENDING||orderStatus.status == Constants.ORDER_STATUS.PAYMENT_COMPLETED) {
        if (JSON.parse(orderStatus.total_price) === 0) {   // Admin traded ticket order
          let trxDetails = {
            txHash: trx,
            status: "completed_admin",
            orderId: orderStatus.id,
          };
          trxStatusArray.push(trxDetails);
        } else {
          let trxDetails = {
            txHash: trx,
            status: "completed",
            orderId: orderStatus.id,
          };
          trxStatusArray.push(trxDetails);
        }
      } else {
        if (JSON.parse(orderStatus.total_price) === 0) {
          let trxDetails = {
            txHash: trx,
            status: "pending_admin",
            orderId: orderStatus.id,
          };
          trxStatusArray.push(trxDetails);
        } else {
          let trxDetails = {
            txHash: trx,
            status: "pending",
            orderId: orderStatus.id,
          };
          trxStatusArray.push(trxDetails);
        }
      }
      // trx.status = "completed"
    }
  }
 
  res.send(trxStatusArray);
}


exports.getUserLotteryTicketDetails = async function (req, res) {
  console.log("Inside getUserLotteryTicketDetails", req.body);
  const accountAddress = req.body.accountAddress;
  console.log("Account Address  ", accountAddress);
  const foundItem = await LotteryUserTicket.findAll({
    order: [["createdAt", "DESC"]],
    where: { address: accountAddress },
  });
  console.log("Found item", foundItem);

  res.send(foundItem);
};





function getTeamNameById(teamId)
{
  let teamName = "";

  if (teamId == 1) teamName = "Fire";
  if (teamId == 2) teamName = "Water";
  if (teamId == 3) teamName = "Air";
  if (teamId == 4) teamName = "Earth";
  return teamName;
}
async function getTicketColorArray(teamName)
{
  let ticketColorArrayName = teamName.toUpperCase() + "_TICKET_COLOR_ARRAY";

  let ticketColorArray = await sequelize.query(
    `select val from properties where id = '${ticketColorArrayName}'`
  );
  return JSON.parse(ticketColorArray[0][0].val);
}
// Saving Order and Ticket Details
async function createUserTickets(orderUser, pendingOrder) {
  let orderId = pendingOrder.id;
  console.log("Inside createUserTickets");


  console.log("Found item", orderUser.dataValues);

  let numberOfTickets = pendingOrder.total_ticket;

  console.log("ORDER ID IS  ", orderId);

  let teamName = getTeamNameById(orderUser.dataValues.team_id);

  let ticketColorArray = await getTicketColorArray(teamName)
  console.log(ticketColorArray);

  let currentTicketPrice = await sequelize.query(
    `select val from properties where id = 'TICKET_PRICE'`
  );
  currentTicketPrice = currentTicketPrice[0][0].val;

  for (let i = 0; i < numberOfTickets; i++) {
    let ticketNumber;
    //Get unique ticket number per lottery
    while (true) {
      ticketNumber = await commonUtil.generateTicketNumber();
      const existingTicket = await LotteryUserTicket.findOne({
        where: { ticket_no: ticketNumber, lottery_id: pendingOrder.lottery_id },
      });
      if (!existingTicket) {
        break;
      }
    }
    let ticket={
      lottery_id: pendingOrder.lottery_id,
      user_account_id: orderUser.dataValues.id,
      address: orderUser.dataValues.address,
      team_id: orderUser.dataValues.team_id,
      ticket_no: ticketNumber,
      nft_tx_currency: global.currency,
      ticket_status: Constants.TICKET_STATUS.CREATED,
      total_point: 0,
      lottery_order_id: orderId,
      ticket_price:currentTicketPrice
    }
    try {
      let ticket_filename = await generateLocalTicket(ticketNumber, teamName, ticketColorArray);
      console.log("FILENAME", ticket_filename);
      ticket.ticket_filename=ticket_filename;
      let userTicket=await LotteryUserTicket.create(ticket);
      console.log(userTicket)
      
    } catch (error) {
      console.error(error);
      ticket. ticket_status=Constants.TICKET_STATUS.FAILED;
      await LotteryUserTicket.create(ticket);
    }
  }

  await LotteryOrders.update(
    {
      status: Constants.ORDER_STATUS.PENDING,
    },
    {
      where: {
        id: orderId,
      },
    }
  );
  await Team.update(
    {
      total_trade_volume: sequelize.literal(
        `total_trade_volume + ${pendingOrder.total_ticket}`
      ),
    },
    { where: { team_id: orderUser.dataValues.team_id } }
  );

  const foundLottery = await Lottery.findOne({ where: { id: pendingOrder.lottery_id } });
  let total_fund = foundLottery.dataValues.total_fund;
  const userDetails = await LotteryOrders.findAll({
    where: { lottery_id: pendingOrder.lottery_id, address: orderUser.dataValues.address }
  });

  let user_cnt = 0;
  if (!userDetails||userDetails.length<2) {
    user_cnt = 1;
  }
  total_fund = JSON.parse(total_fund) + JSON.parse(pendingOrder.total_price);

  const updatedLottery = await Lottery.update(
    {
      total_fund: total_fund,
      total_ticket: sequelize.literal(`total_ticket + ${pendingOrder.total_ticket}`),
      total_user: sequelize.literal(`total_user + ${user_cnt}`),
    },
    { where: { id: pendingOrder.lottery_id } }
  );
 
}




async function generateLocalTicket(ticketNumber, teamName, ticketColorArray) {
  return new Promise((resolve, reject)=>{
    console.log("Inside Generating Local Ticket ");

    let randomColor =
      ticketColorArray[Math.floor(Math.random() * ticketColorArray.length)];
  
    console.log("Picked Random Color ", randomColor);
  
    const data = fs.readFileSync(
      __dirname + "/../../templates/" + teamName + ".svg",
      "utf8"
    );
  
    let fileName ;
  
    if (data) {
      console.log("File Data Found ");
      var change1 = data.replace("ticketNumber", ticketNumber);
      var change3 = change1.replace("ticketColor", randomColor);
      fileName = (ticketNumber.replace(/\s/g, '')) + new Date().getTime();
      // fs.writeFileSync(__dirname + "/tickets/" + ticketNumber + ".svg", change3);
      fs.writeFileSync(__dirname + "/../../tickets/" + fileName + ".svg", change3);
      resolve(fileName);
    } else {
      console.log("Some Error Occured. No Template data Found");
      reject("Some Error Occured. No Template data Found");
    }
  });

}
exports.generateLocalTicket = generateLocalTicket;

async function uploadIpfsTicket(userTicket) {
  let ticketNumber=userTicket.ticket_no, teamId=userTicket.team_id, lotteryEventId=userTicket.lottery_id, ticketFilename=userTicket.ticket_filename;
  console.log("Inside uploadIpfsTicket ");
  let ticketNFTDetails = null;
  try {
    let teamName = "";
    if (teamId == 1) teamName = "Fire";
    if (teamId == 2) teamName = "Water";
    if (teamId == 3) teamName = "Air";
    if (teamId == 4) teamName = "Earth";

    if (
      !(fs.existsSync(__dirname + "/../../tickets/" + ticketFilename + ".svg"))
    ) {
      console.log("The file does not exist",__dirname + "/../../tickets/" + ticketFilename + ".svg");
      await LotteryUserTicket.update(
        {
          ticket_status: Constants.TICKET_STATUS.FILE_NOT_FOUND,
        },
        {
          where: { id:userTicket.id },
        }
      );
      return ticketNFTDetails;
    } 

    let change3 = fs.readFileSync(
      __dirname + "/../../tickets/" + ticketFilename + ".svg",
      "utf8"
    );
    

    let imageBuffer = Buffer.from(change3);

    let ticketImageHash = await ipfsService.storeToIPFS(imageBuffer);

    let tokenData = {
      lotteryEventId: lotteryEventId,
      ticketNumber: ticketNumber,
      teamId: teamId,
      teamName: teamName,
      ticketImageHash: "https://ipfs.io/ipfs/" + ticketImageHash,
    };

    let tokenUri = Buffer.from(JSON.stringify(tokenData));
    let ticketUriHash = await ipfsService.storeToIPFS(tokenUri);
    console.log("ticketUriHash ", ticketUriHash);

    ticketNFTDetails = {
      ticketUriHash: "https://ipfs.io/ipfs/" + ticketUriHash,
      ticketImageHash: "https://ipfs.io/ipfs/" + ticketImageHash,
    };

    console.log("ticketNFTDetails ", ticketNFTDetails);
  } catch (error) {
    console.log("ERROR IN SELECT TICKET TEMPLATE ", error);
    await LotteryUserTicket.update(
      {
        ticket_status: Constants.TICKET_STATUS.IPFS_FAILED,
      },
      {
        where: { id:userTicket.id },
      }
    );
   
  }

  return ticketNFTDetails;

}


async function nftConfirmation() {
	let txList = await LotteryUserTicket.findAll({
    where: {
      ticket_status: Constants.TICKET_STATUS.NFT_CONFIRMATION_PENDING
    },
  })
  console.log("Transaction List With Pending NFT Transaction Confirmations ", txList);
  for (let i = 0; i < txList.length; i++) {
    let userTicket = txList[i].dataValues;
    console.log("USER TICKET For Confimation ", i, " is  ", userTicket)
    await LotteryUserTicket.update(
      {
        ticket_status: Constants.TICKET_STATUS.NFT_CONFIRMATION_PROCESSING,
      },
      {
        where: {
          id:userTicket.id
        },
      }
    );
    let result=await confirmNFTTransaction(userTicket);
    await LotteryUserTicket.update(
      {
        ...result,
        ticket_status: Constants.TICKET_STATUS.COMPLETED,
      },
      {
        where: {
          id:userTicket.id
        },
      }
    );
  }
}

async function confirmNFTTransaction(userTicket) {
  const txHash = userTicket.nft_tx_id;
  // Get current number of confirmations and compare it with sought-for value
 
  let nftTxDetails = await web3.eth.getTransaction(txHash);
  const currentBlock = await web3.eth.getBlockNumber();
  let trxConfirmations=nftTxDetails.blockNumber === null ? 0 : currentBlock - nftTxDetails.blockNumber;

  console.log(
    "NFT Transaction with hash " +
    txHash +
    " has " +
    trxConfirmations +
    " confirmation(s)"
  );

  if (trxConfirmations >= Constants.NFT_CONFIRM_BLOCK_COUNT) {
    // Handle confirmation event according to your business logic

    console.log(
      "NFT Transaction with hash " + txHash + " has been successfully confirmed"
    );

    let nftTxGasFee = await web3.utils.fromWei(nftTxDetails.gasPrice, 'ether');
    let nftTxReceipt = await web3.eth.getTransactionReceipt(txHash);
    let nftTxCharge = nftTxReceipt.gasUsed * JSON.parse(nftTxGasFee);
    let logs = nftTxReceipt.logs;
    console.log("nftTxReceipt logs", logs);
    let nftTokenId = await web3.utils.hexToNumberString(logs[0].topics[3]);
    console.log(nftTokenId);

    

    return {
      nft_id: nftTokenId,
      nft_tx_id: nftTxReceipt.transactionHash,
      nft_tx_fee: nftTxCharge,
    };
  }
  // Recursive call
  
  return await confirmNFTTransaction(userTicket);
}




exports.getLotteryEventPrizePoolDetails = async function (req, res) {
  console.log("Inside getUserLotteryTicketDetails");
  // const lotteryId = await getLatestLotteryId();
  // console.log("Active lottery Id ", lotteryId)
  const foundLottery = await Lottery.findOne({
    order: [["id", "DESC"]],
    limit: 1,
  });
  console.log(
    "Found Lottery ",
    foundLottery.dataValues.fund_distribution.prize_pool
  );
  res.send(foundLottery.dataValues);
};

async function getCreditDetails(req, res) {
  const accountAddress = req.body.accountAddress;

	let creditAmount = await LotteryResults.findAll({
		attributes: [
			"lottery_id",
		  "total_prize"
		],
		where: {
		  address: accountAddress,
		  prize_payment_status: Constants.PRIZE_PAYMENT_STATUS.COMPLETED,
		  result_view_status: false,
		},
		order: [["lottery_id", "DESC"]]
	  });

	  console.log("data " , creditAmount);
    res.send(creditAmount);

  return;
}


exports.getAllLotteryEventHistoryDetails = async function (req, res) {
  console.log("Inside getAllLotteryEventHistoryDetails");
  const presentLotteryId = await getLatestLotteryId();
  let offset = req.body.pageNumber * 100;
  const pastLotteries = await Lottery.findAll({
    where: { id: { [Op.ne]: presentLotteryId } },
    order: [["createdAt", "DESC"]],
    limit: 100,
    offset: offset,
  });
  console.log("All Past Lotteries History Details ", pastLotteries);
  res.send(pastLotteries.dataValues);
};





exports.verifyOrderPayment = async function () {
  let pendingUserOrders = await sequelize.query(
    `select * from lottery_orders where status= '${Constants.ORDER_STATUS.CREATED}' and order_type <> '${Constants.ORDER_TYPE.TRADE}' limit 10`
  );
  pendingUserOrders = pendingUserOrders[0];
  for (let i = 0; i < pendingUserOrders.length; i++) {
    const element = pendingUserOrders[i];
    if (element) {
      await LotteryOrders.update(
        {
          status: Constants.ORDER_STATUS.PROCESSING,
        },
        {
          where: {
            id: element.id,
          },
        }
      );
      confirmOrderPaymentTransaction(element);
      
    }
  }
 
  

}

exports.processFailedTickets = async function () {

  let txList = await LotteryUserTicket.findAll({
    where: {
      ticket_status: Constants.TICKET_STATUS.FAILED,

    },
  })
  for (let i = 0; i < txList.length; i++) {
    let userTicket = txList[i].dataValues;
    let teamName = getTeamNameById(userTicket.team_id);
    let ticketColorArray = await getTicketColorArray(teamName)
    let ticket_filename = await generateLocalTicket(userTicket.ticket_no, teamName, ticketColorArray);
     if(ticket_filename)
     {
      await LotteryUserTicket.update(
        {
          ticket_status: Constants.TICKET_STATUS.CREATED,
          ticket_filename:ticket_filename
        },
        {
          where: {
            id: userTicket.id,
          },
        }
      );
     }
  }
}

exports.startTicketCreation = async function () {
 
  let pendingUserOrders = await sequelize.query(
    `select * from lottery_orders where status = '${Constants.ORDER_STATUS.PAYMENT_COMPLETED}' limit 1`
  );
  pendingUserOrders = pendingUserOrders[0][0];
  console.log(" Verifying the Pending transaction ", pendingUserOrders);
  if (pendingUserOrders == null) return;

  const orderUser = await UserAccount.findOne({
    where: { address: pendingUserOrders.address },
  });
  console.log("Found item", orderUser.dataValues);

  await LotteryOrders.update(
    {
      status: Constants.ORDER_STATUS.TICKET_PROCESSING,
    },
    {
      where: {
        id: pendingUserOrders.id,
      },
    }
  );
  await createUserTickets(orderUser, pendingUserOrders);
};

async function confirmOrderPaymentTransaction(order) {
  try{
    let status=Constants.ORDER_STATUS.CREATED;
    let txId = order.pay_tx_id;
    const now = moment();
    let end = moment(order.createdAt);
    let diff = now.diff(end, "minutes");
    let errorMessage;
    const trx = await web3.eth.getTransaction(txId);
    const currentBlock = await web3.eth.getBlockNumber();
  
    if (trx == null) {
      console.log("Transaction with hash " + txId + " has not been confirmed");
      if (diff > 30) {
        errorMessage = `Transaction [${txId}] not found`;
        status = Constants.ORDER_STATUS.PAYMENT_FAILED;
      }
    } else {
      let trxConfirmations =
        trx.blockNumber === null ? 0 : currentBlock - trx.blockNumber;
      if (trxConfirmations >= Constants.PAYMENT_CONFIRM_BLOCK_COUNT) {
        if (trx.to.toLowerCase() != global.adminAccountAddress.toLowerCase()) {
          errorMessage = `Transaction [${txId}] not recived on system address`;
          status = Constants.ORDER_STATUS.PAYMENT_FAILED;
        } else {
          let currentTicketPrice = await sequelize.query(
            `select ticket_price from lotteries where id = ${order.lottery_id}`
          );
          currentTicketPrice = currentTicketPrice[0][0].ticket_price;
          let calculatedOrderPrice = (
            order.total_ticket * currentTicketPrice
          ).toFixed(Constants.CURRENCY_DECIMAL);
          var calculatedOrderPriceInWei = web3.utils.toWei(
            calculatedOrderPrice.toString(),
            "ether"
          );
          calculatedOrderPriceInWei = JSON.parse(calculatedOrderPriceInWei);
          let trxValueInWei = JSON.parse(trx.value);
          if (
            calculatedOrderPrice != order.total_price ||
            calculatedOrderPriceInWei != trxValueInWei
          ) {
            errorMessage = `Transaction [${txId}] order amount mismatch`;
            status = Constants.ORDER_STATUS.PAYMENT_FAILED;
          } else {
            const trxReceipt = await web3.eth.getTransactionReceipt(
              order.pay_tx_id
            );
  
            let trxGasFee = await web3.utils.fromWei(trx.gasPrice, "ether");
            let trxCharge = trxReceipt.gasUsed * JSON.parse(trxGasFee);
            status = Constants.ORDER_STATUS.PAYMENT_COMPLETED;
          }
        }
      }
    }
  
    await LotteryOrders.update(
      {
        status: status,
        error: errorMessage,
      },
      {
        where: {
          pay_tx_id: txId,
        },
      }
    );
  }
  catch (err) {
    let txId = order.pay_tx_id;
    console.log(
      "confirmOrderPaymentTransaction_error_on_confirm_payment",
      err
    );
    await LotteryOrders.update(
      {
        status: Constants.ORDER_STATUS.PAYMENT_FAILED,
        error: JSON.stringify(err.message),
      },
      {
        where: {
          pay_tx_id: txId,
        },
      }
    );
  }
  

}




async function fetchPresentPricePerTicket(req, res) {
  let pricePerTicket = await sequelize.query(
    `select val from properties where id = 'TICKET_PRICE'`
  );
  pricePerTicket = pricePerTicket[0][0].val;
  if (req) {
    res.send(pricePerTicket);
    return;
  } else {
    return pricePerTicket;
  }

}


exports.fetchLotteryWinningNumber = async function (req, res) {
  console.log("Page Number Received ", req.body);
  let lotteryWinningNumber = await sequelize.query(
    `select id,winning_no,end_time,fund_distribution,total_ticket,total_user, winning_users, winning_teams, total_fund from lotteries where id != (select MAX(id) from lotteries) order by id desc limit 100 offset ${req.body.pageNumber * 100
    }`
  );
  lotteryWinningNumber = lotteryWinningNumber[0];
  console.log(" lotteryWinningNumber ", lotteryWinningNumber[0]);

  res.send(lotteryWinningNumber);
};

async function createNewLotteryEvent(previousLotteryId) {
  console.log("Inside Create New Lottery Event");
  let res = await Properties.findOne({
    where: { id: "LOTTERY_SLOT" },
  });
  let lotteryTimeSlot = JSON.parse(res.dataValues.val);

  let ticketPrice =  await updatePricePerTicket();

  var startTime = new Date();
  startTime.setHours(parseInt(lotteryTimeSlot.start_time.split(":")[0]));
  startTime.setMinutes(parseInt(lotteryTimeSlot.start_time.split(":")[1]));
  startTime.setSeconds(0);

  var endTime = new Date();
  endTime.setHours(parseInt(lotteryTimeSlot.end_time.split(":")[0]));
  endTime.setMinutes(parseInt(lotteryTimeSlot.end_time.split(":")[1]));
  endTime.setSeconds(0);

  var currentDate = new Date();
  if (currentDate.getHours() >= 23 && currentDate.getMinutes() >= 30) {
    startTime.setDate(startTime.getDate() + 1);
    endTime.setDate(endTime.getDate() + 1);
  }

  console.log(
    "Start  : ",
    "  end date  : ",
    date.format(endTime, "YYYY-MM-DD HH:mm:ss")
  );

  let fundDistributionDetails = await Properties.findOne({
    where: { id: "FUND_DISTRIBUTION_DETAILS" },
  });
  fundDistributionDetails = JSON.parse(fundDistributionDetails.dataValues.val);

  try {
    const newLottery = await Lottery.create({
      name: "Lottery Event " + (previousLotteryId + 1),
      start_time: date.format(startTime, "YYYY-MM-DD HH:mm:ss"),
      end_time: date.format(endTime, "YYYY-MM-DD HH:mm:ss"),
      ticket_price: ticketPrice,
      ticket_price_currency: global.currency,
      total_fund_currency: global.currency,
      total_fund: 0,
      total_ticket: 0,
      total_user: 0,
      fund_distribution: { 
        prize_pool: fundDistributionDetails.prize_pool,
        team_event: fundDistributionDetails.team_event, 
        maintenance: fundDistributionDetails.maintenance,
        first_winner: fundDistributionDetails.first_winner ,
        second_winner: fundDistributionDetails.second_winner ,
        third_winner: fundDistributionDetails.third_winner  },
      winning_no: null,
      winning_users: null,
      winning_teams: null,
    });
  } catch (error) {
    console.log("ERROR INSIDE CREATE NEW LOTTERY ", error);
  }
}


async function updatePricePerTicket() {
  let ticketMultiplicationFactor = await sequelize.query(
    `select * from properties where id = 'TICKET_MULTIPLICATION_FACTOR'`
  );
  ticketMultiplicationFactor = JSON.parse(ticketMultiplicationFactor[0][0].val);
  let estimatedGas = await blockChainService.estimateGasPrices();

  let newTicketPrice = ((estimatedGas.nftEstimatedGas + estimatedGas.bnbSendGas) * estimatedGas.gasPrice * ticketMultiplicationFactor).toFixed(Constants.CURRENCY_DECIMAL)*1;
  await Properties.update(
    {
      val: newTicketPrice,
    },
    {
      where: {
        id: "TICKET_PRICE",
      },
    }
  );
  return newTicketPrice
}


/* 
 @author:
 @date:21th April 2022
 @description: NFT transfer functionality
 handle 3 senario for expire and active ticket
 1.existing user
 2.transfer to admin address
 3.Ghost user
*/
async function updateLotteryOwner(log) {
  let fromAddress = "0x" + log.topics[1].slice(26);
  let toAddress = "0x" + log.topics[2].slice(26);
  const fromUser = await UserAccount.findOne({
    where: { address: fromAddress },
  });
  console.log(
    "updateLotteryOwner:",
    `Recived ticket from ${fromAddress} to ${toAddress} for transaction ${log.transactionHash}`
  );
  if (!fromUser) {
    console.log(
      "updateLotteryOwner:",
      `Recived ticket from ${fromAddress} to ${toAddress} but fromuser not found for transaction ${log.transactionHash}`
    );
    return;
  }

  try {
    const foundLotteryTicketTrxId = await LotteryUserTicket.findOne({
      where: { nft_tx_id: log.transactionHash },
    });
    if (foundLotteryTicketTrxId) return;

    let tokenId = await web3.utils.hexToNumberString(log.topics[3]);

    let lotteryTicket = await LotteryUserTicket.findAll({
      where: {
        address: fromAddress,
        nft_id: tokenId,
      },
    });

    if (lotteryTicket.length) {
      // Lottery ticket exist
      lotteryTicket = lotteryTicket[0].dataValues;

      // assuming lotteries table can't be empty
      let currentLotteryId = await sequelize.query(
        `select MAX(id) as id from lotteries`
      );
      currentLotteryId = currentLotteryId[0][0].id;
      // update lottery
      let oldLotteryTicket = await LotteryUserTicket.update(
        { ticket_status: Constants.TICKET_STATUS.TRANSFER_COMPLETED },
        {
          where: {
            id: lotteryTicket.id,
          },
        }
      );
      const toUser = await UserAccount.findOne({
        where: { address: toAddress },
      });
      let total_point = 0;

      let obj = {
        lottery_id: lotteryTicket.lottery_id,
        ticket_no: lotteryTicket.ticket_no,
        nft_pic_url: lotteryTicket.nft_pic_url,
        nft_id: lotteryTicket.nft_id,
        nft_tx_id: log.transactionHash,
        nft_doc_hash: lotteryTicket.nft_doc_hash,
        nft_tx_fee: lotteryTicket.nft_tx_fee,
        nft_tx_currency: global.currency,
        total_point: total_point,
        lottery_order_id: -1,
        ticket_filename: lotteryTicket.ticket_filename,
      };
      // case 1: user exist in sytemtem
      if (toUser) {
        const item = await LotteryUserTicket.create({
          ...obj,
          user_account_id: toUser.dataValues.id,
          address: toUser.dataValues.address,
          team_id: toUser.dataValues.team_id,
          ticket_status: Constants.TICKET_STATUS.COMPLETED,
        });
      }
    }
    // case 2: check transfer for admin
    else if (toAddress === global.adminAccountAddress) {
      const item = await LotteryUserTicket.create({
        ...obj,
        user_account_id: 0,
        address: toAddress,
        team_id: 0,
        ticket_status: Constants.TICKET_STATUS.TRANSFERRED_TO_ADMIN,
      });
    }
    // case 3: ghost user
    else {
      const ghostUser = await userController.createGhostUser(
        toAddress,
        lotteryTicket
      );
      const item = await LotteryUserTicket.create({
        ...obj,
        user_account_id: ghostUser.id,
        address: ghostUser.address,
        team_id: ghostUser.team_id,

        ticket_status: Constants.TICKET_STATUS.COMPLETED,
      });
    }
  } catch (err) {
    console.error(err);
    console.log(err);
  }
}





async function getLatestLotteryId(req, res) {
  console.log("Inside getLatestLotteryId");
  let activeLotteryDetails = await sequelize.query(
    `select MAX(id) as id from lotteries`
  );
  activeLotteryDetails = activeLotteryDetails[0];
  // const lotteryId = activeLotteryDetails[0].id ;
  console.log("latest_lottery_id", activeLotteryDetails[0].id);
  if (res) res.send(activeLotteryDetails[0]);
  return activeLotteryDetails[0].id;
}


async function updateWinningNumberToLotteryTable(lotteryId, winning_no) {
  console.log("Inside Update Winning Number To Lottery Table");

  await Lottery.update(
    {
      winning_no: winning_no,
    },
    { where: { id: lotteryId } }
  );
}



async function getAllLotteryTicketOrders(req, res) {
  console.log("Inside getAllLotteryTicketOrders", req.body);
  const valid = req.validate([
    { field: 'accountAddress', type: 'STRING', isRequired: true },
  ], req.body);
  if (!valid) {
    return res.sendError();
  }
  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }

  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    const reg = /^\d+$/;
    if(reg.test(req.body.searchText)){
    let totalCount = await sequelize.query(`select count(id) from lottery_orders where address = '${req.body.accountAddress}' and (lottery_id = ${req.body.searchText} or id = ${req.body.searchText})`)
    totalCount = parseInt(totalCount[0][0].count);
  
    if (totalCount > 0) {
    
      let userOrders = await sequelize.query(`select * from lottery_orders where address = '${req.body.accountAddress}' and (lottery_id = ${req.body.searchText} or id = ${req.body.searchText}) order by id Desc limit ${limit} offset ${offset}`)
   
  
      console.log("All Orders From User ", userOrders);
      return res.send({
        total: totalCount,
        list: userOrders[0]
      });
    } else {
      return res.send({
        total: 0,
        list: []
      });
    }
  }

  let totalCount = await sequelize.query(`select count(id) from lottery_orders where address = '${req.body.accountAddress}' and status ilike '%${req.body.searchText}%' `)
  totalCount = parseInt(totalCount[0][0].count);

  if (totalCount > 0) {
  
    let userOrders = await sequelize.query(`select * from lottery_orders where address = '${req.body.accountAddress}' and status ilike '%${req.body.searchText}%' order by id Desc limit ${limit} offset ${offset}`)
 

    console.log("All Orders From User ", userOrders);
    return res.send({
      total: totalCount,
      list: userOrders[0]
    });
  } else {
    return res.send({
      total: 0,
      list: []
    });
  }

}

  let totalCount = await sequelize.query(`select count(id) from lottery_orders where address = '${req.body.accountAddress}'`)
  totalCount = parseInt(totalCount[0][0].count);

  if (totalCount > 0) {
  
    let userOrders = await LotteryOrders.findAll({
      where: { address: req.body.accountAddress },
      order: [
        ["id", "DESC"],
        ["createdAt", "DESC"],
      ],
      limit: limit,
      offset: offset,
    });
 

    console.log("All Orders From User ", userOrders);
    res.send({
      total: totalCount,
      list: userOrders
    });
  } else {
    res.send({
      total: 0,
      list: []
    });
  }
}



async function getAllLotteryTicketPoints(req, res) {
  console.log("Inside getAllLotteryTicketOrders", req.body);
  try{
  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }
  let activeLotteryId = await getLatestLotteryId();
  let accountAddress = req.body.accountAddress;

  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    const reg = /^\d+$/;
    if(reg.test(req.body.searchText)){
      let userTotalTicketCount = await sequelize.query(
        `select count(id) 
        from lottery_user_tickets where address = '${accountAddress}' and lottery_id = ${req.body.searchText}`
      );
      userTotalTicketCount = parseInt(
        userTotalTicketCount[0][0].count
      );
      if (userTotalTicketCount > 0) {
    
        let userPoints = await sequelize.query(`select 
        lottery_user_tickets.lottery_id,
        lottery_user_tickets.ticket_no,
        lottery_user_tickets.total_point, 
        lotteries.winning_no
        from lottery_user_tickets
        inner join lotteries 
        on lottery_user_tickets.lottery_id = lotteries.id 
        where lotteries.id <> ${activeLotteryId} 
        AND lottery_user_tickets.address = '${accountAddress}' and lottery_user_tickets.lottery_id = ${req.body.searchText}
        group by lottery_user_tickets.lottery_id, 
        lottery_user_tickets.ticket_no,
        lottery_user_tickets.total_point, 
        lotteries.winning_no
        order by lottery_user_tickets.lottery_id desc,lottery_user_tickets.total_point desc 
        limit ${limit}
        offset ${offset}
        `);
    
        console.log("All Ticket Points of User ", userPoints);
        userPoints = userPoints[0];
        return res.send({
          list: userPoints,
          total: userTotalTicketCount,
        });
      } else {
        return res.send({
          list: [],
          total: 0,
        });
      }
    }

    let userTotalTicketCount = await sequelize.query(
      `select count(id) 
      from lottery_user_tickets where address = '${accountAddress}' and ticket_no ilike '%${req.body.searchText}%'`
    );
    userTotalTicketCount = parseInt(
      userTotalTicketCount[0][0].count
    );
    if (userTotalTicketCount > 0) {
  
      let userPoints = await sequelize.query(`select 
      lottery_user_tickets.lottery_id,
      lottery_user_tickets.ticket_no,
      lottery_user_tickets.total_point, 
      lotteries.winning_no
      from lottery_user_tickets
      inner join lotteries 
      on lottery_user_tickets.lottery_id = lotteries.id 
      where lotteries.id <> ${activeLotteryId} 
      AND lottery_user_tickets.address = '${accountAddress}' and lottery_user_tickets.ticket_no ilike '%${req.body.searchText}%'
      group by lottery_user_tickets.lottery_id, 
      lottery_user_tickets.ticket_no,
      lottery_user_tickets.total_point, 
      lotteries.winning_no
      order by lottery_user_tickets.lottery_id desc,lottery_user_tickets.total_point desc 
      limit ${limit}
      offset ${offset}
      `);
  
      console.log("All Ticket Points of User ", userPoints);
      userPoints = userPoints[0];
      return res.send({
        list: userPoints,
        total: userTotalTicketCount,
      });
    } else {
      return res.send({
        list: [],
        total: 0,
      });
    }
  }

  let userTotalTicketCount = await sequelize.query(
    `select sum(total_ticket) as total_ticket_count 
    from lottery_orders where address = '${accountAddress}'`
  );
  userTotalTicketCount = parseInt(
    userTotalTicketCount[0][0].total_ticket_count
  );
  if (userTotalTicketCount > 0) {

    let userPoints = await sequelize.query(`select 
    lottery_user_tickets.lottery_id,
    lottery_user_tickets.ticket_no,
    lottery_user_tickets.total_point, 
    lotteries.winning_no
    from lottery_user_tickets
    inner join lotteries 
    on lottery_user_tickets.lottery_id = lotteries.id 
    where lotteries.id <> ${activeLotteryId} 
    AND lottery_user_tickets.address = '${accountAddress}'
    group by lottery_user_tickets.lottery_id, 
    lottery_user_tickets.ticket_no,
    lottery_user_tickets.total_point, 
    lotteries.winning_no
    order by lottery_user_tickets.lottery_id desc,lottery_user_tickets.total_point desc 
    limit ${limit}
    offset ${offset}
    `);

    console.log("All Ticket Points of User ", userPoints);
    userPoints = userPoints[0];
    res.send({
      list: userPoints,
      total: userTotalTicketCount,
    });
  } else {
    res.send({
      list: [],
      total: 0,
    });
  }
}
catch(err)
{
  console.error("getAllLotteryTicketPoints=",err.message);
}
}


async function getSelectedOrderDetails(req, res) {
  console.log("Inside getOrderDetails", req.body.selectedOrder);

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }

  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }
  const address = req.body.accountAddress;

  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    let totalCount = await sequelize.query(`select count(id) from lottery_user_tickets where lottery_user_tickets.lottery_order_id = ${req.body.selectedOrder.id} and ( ticket_no ilike '%${req.body.searchText}%' or  ticket_status ilike '%${req.body.searchText}%')`);
    totalCount = parseInt(totalCount[0][0].count);
    if (totalCount > 0) {
  
      let userOrderDetails;
      try{
     userOrderDetails = await sequelize.query(`select 
    lottery_user_tickets.lottery_id, 
    lottery_user_tickets.lottery_order_id,
    lottery_user_tickets.ticket_no,
    lottery_user_tickets.ticket_status,
    lottery_user_tickets.nft_tx_id,
    lottery_user_tickets.ticket_filename,
    lottery_orders.pay_tx_currency,
    lottery_orders.pay_tx_id,
    lottery_orders."createdAt",
    ROUND(lottery_orders.total_price/lottery_orders.total_ticket, 6) as per_ticket_price
    from lottery_user_tickets 
    inner join lottery_orders on lottery_user_tickets.lottery_order_id = lottery_orders.id
    where lottery_user_tickets.lottery_order_id = ${req.body.selectedOrder.id} AND
    lottery_user_tickets.lottery_id = ${req.body.selectedOrder.lottery_id} AND
    lottery_user_tickets.address = '${address}' AND ( ticket_no ilike '%${req.body.searchText}%' or  ticket_status ilike '%${req.body.searchText}%')
    limit ${limit}
    offset ${offset}
    `);
      }
      catch(e)
      {
        console.log(e);
      }
     
  
      userOrderDetails = userOrderDetails[0];
      console.log("Order Details  ", userOrderDetails);
      return res.send({
        list: userOrderDetails.map((item) => {
          return {
            ...item,
            ticket_image:
              process.env.TICKET_URL_PREFIX +
              encodeURIComponent(item.ticket_filename ? item.ticket_filename : item.ticket_no) +
              ".svg",
          }
        }),
        total: totalCount
      }
      );
    } else {
      return res.send({
        list: [],
        total: 0
      })
    }
  }

  let totalCount = await sequelize.query(`select total_ticket from lottery_orders where id = ${req.body.selectedOrder.id}`);
  totalCount = parseInt(totalCount[0][0].total_ticket);
  if (totalCount > 0) {

    let userOrderDetails;
    try{
   userOrderDetails = await sequelize.query(`select 
  lottery_user_tickets.lottery_id, 
  lottery_user_tickets.lottery_order_id,
  lottery_user_tickets.ticket_no,
  lottery_user_tickets.ticket_status,
  lottery_user_tickets.nft_tx_id,
  lottery_user_tickets.ticket_filename,
  lottery_orders.pay_tx_currency,
  lottery_orders.pay_tx_id,
  lottery_orders."createdAt",
  ROUND(lottery_orders.total_price/lottery_orders.total_ticket, 6) as per_ticket_price
  from lottery_user_tickets 
  inner join lottery_orders on lottery_user_tickets.lottery_order_id = lottery_orders.id
  where lottery_user_tickets.lottery_order_id = ${req.body.selectedOrder.id} AND
  lottery_user_tickets.lottery_id = ${req.body.selectedOrder.lottery_id} AND
  lottery_user_tickets.address = '${address}'
  limit ${limit}
  offset ${offset}
  `);
    }
    catch(e)
    {
      console.log(e);
    }
   

    userOrderDetails = userOrderDetails[0];
    console.log("Order Details  ", userOrderDetails);
    res.send({
      list: userOrderDetails.map((item) => {
        return {
          ...item,
          ticket_image:
            process.env.TICKET_URL_PREFIX +
            encodeURIComponent(item.ticket_filename ? item.ticket_filename : item.ticket_no) +
            ".svg",
        }
      }),
      total: totalCount
    }
    );
  } else {
    res.send({
      list: [],
      total: 0
    })
  }
}


async function getReceivedOrderDetails(req, res) {
  //console.log("Inside getOrderDetails", req.body.selectedOrder);

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }

  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }
  const address = req.body.accountAddress;

  if(req.body.searchText !== undefined && req.body.searchText.trim()){

    const reg = /^\d+$/;
    if(reg.test(req.body.searchText)){
      let totalCount = await sequelize.query(`select count(id) from lottery_user_tickets 
      where lottery_order_id = -1 and address = '${address}' and lottery_id = ${req.body.searchText} `);
      totalCount = parseInt(totalCount[0][0].count);
      if (totalCount > 0) {
    
        let userOrderDetails = await sequelize.query(`select 
      lottery_user_tickets.lottery_id, 
      lottery_user_tickets.lottery_order_id,
      lottery_user_tickets.ticket_no,
      lottery_user_tickets.ticket_status,
      lottery_user_tickets.nft_tx_id,
      lottery_user_tickets."createdAt",
      lottery_user_tickets.ticket_filename,
      lottery_user_tickets.nft_tx_currency
      from lottery_user_tickets 
      where lottery_user_tickets.lottery_order_id = -1 AND
      lottery_user_tickets.address = '${address}' AND lottery_id = ${req.body.searchText}
      limit ${limit}
      offset ${offset}
      `);
    
        userOrderDetails = userOrderDetails[0];
        console.log("Order Details  ", userOrderDetails);
        return res.send({
          list: userOrderDetails.map((item) => {
            return {
              ...item,
              ticket_image:
                process.env.TICKET_URL_PREFIX +
                encodeURIComponent(item.ticket_filename ? item.ticket_filename : item.ticket_no) +
                ".svg",
            }
          }),
          total: totalCount
        });
      } else {
        return res.send({
          list: [],
          total: 0
        })
      }
    }

    let totalCount = await sequelize.query(`select count(id) from lottery_user_tickets 
    where lottery_order_id = -1 and address = '${address}' and ticket_no ilike '%${req.body.searchText}%' `);
    totalCount = parseInt(totalCount[0][0].count);
    if (totalCount > 0) {
  
      let userOrderDetails = await sequelize.query(`select 
    lottery_user_tickets.lottery_id, 
    lottery_user_tickets.lottery_order_id,
    lottery_user_tickets.ticket_no,
    lottery_user_tickets.ticket_status,
    lottery_user_tickets.nft_tx_id,
    lottery_user_tickets."createdAt",
    lottery_user_tickets.ticket_filename,
    lottery_user_tickets.nft_tx_currency
    from lottery_user_tickets 
    where lottery_user_tickets.lottery_order_id = -1 AND
    lottery_user_tickets.address = '${address}' AND ticket_no ilike '%${req.body.searchText}%'
    limit ${limit}
    offset ${offset}
    `);
  
      userOrderDetails = userOrderDetails[0];
      console.log("Order Details  ", userOrderDetails);
      return res.send({
        list: userOrderDetails.map((item) => {
          return {
            ...item,
            ticket_image:
              process.env.TICKET_URL_PREFIX +
              encodeURIComponent(item.ticket_filename ? item.ticket_filename : item.ticket_no) +
              ".svg",
          }
        }),
        total: totalCount
      });
    } else {
      return res.send({
        list: [],
        total: 0
      })
    }
  }

  let totalCount = await sequelize.query(`select count(id) from lottery_user_tickets 
  where lottery_order_id = -1 and address = '${address}'`);
  totalCount = parseInt(totalCount[0][0].count);
  if (totalCount > 0) {

    let userOrderDetails = await sequelize.query(`select 
  lottery_user_tickets.lottery_id, 
  lottery_user_tickets.lottery_order_id,
  lottery_user_tickets.ticket_no,
  lottery_user_tickets.ticket_status,
  lottery_user_tickets.nft_tx_id,
  lottery_user_tickets."createdAt",
  lottery_user_tickets.ticket_filename,
  lottery_user_tickets.nft_tx_currency
  from lottery_user_tickets 
  where lottery_user_tickets.lottery_order_id = -1 AND
  lottery_user_tickets.address = '${address}'
  limit ${limit}
  offset ${offset}
  `);

    userOrderDetails = userOrderDetails[0];
    console.log("Order Details  ", userOrderDetails);
    res.send({
      list: userOrderDetails.map((item) => {
        return {
          ...item,
          ticket_image:
            process.env.TICKET_URL_PREFIX +
            encodeURIComponent(item.ticket_filename ? item.ticket_filename : item.ticket_no) +
            ".svg",
        }
      }),
      total: totalCount
    });
  } else {
    res.send({
      list: [],
      total: 0
    })
  }
}


async function getAllNft(req, res) {
  //console.log("Inside getOrderDetails", req.body.selectedOrder);

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }

  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }
  const address = req.body.accountAddress;
  const activeFlag = req.body.active;
  let userOrderDetails = [];

  let ticketExchangeRates = await sequelize.query(
    `select * from properties where id in ('TICKET_TO_TICKET_EX_RATE', 'TICKET_TO_BNB_EX_RATE') order by id asc`
  );
  let ticketToBNBExchangeRate = JSON.parse(ticketExchangeRates[0][0].val);
  let ticketToTicketExchangeRate = JSON.parse(ticketExchangeRates[0][1].val);

  let totalCount = 0;

  if (activeFlag === false) {
    totalCount = await sequelize.query(`select count(id) from lottery_user_tickets where lottery_user_tickets.lottery_id != ${req.body.lottery_id} AND lottery_user_tickets.ticket_status != 'transfer_completed' AND
    lottery_user_tickets.address = '${address}'`)

    totalCount = parseInt(totalCount[0][0].count)

    if (totalCount > 0) {

      userOrderDetails = await sequelize.query(`select 
    lottery_user_tickets.lottery_id, 
    lottery_user_tickets.lottery_order_id,
    lottery_user_tickets.ticket_no,
    lottery_user_tickets.ticket_status,
    lottery_user_tickets.ticket_filename,
    lottery_user_tickets.nft_tx_id,
    lottery_user_tickets.nft_id,
    lottery_user_tickets."createdAt" ,
    lottery_user_tickets.nft_tx_currency
    from lottery_user_tickets 
    where
    lottery_user_tickets.lottery_id != ${req.body.lottery_id} AND lottery_user_tickets.ticket_status != 'transfer_completed' AND
    lottery_user_tickets.address = '${address}' order by "createdAt" DESC
    limit ${limit}
    offset ${offset}

    `);
    }
  } else {
    totalCount = await sequelize.query(`select count(id) from lottery_user_tickets where lottery_user_tickets.lottery_id = ${req.body.lottery_id} AND lottery_user_tickets.ticket_status != 'transfer_completed' AND
    lottery_user_tickets.address = '${address}'`)
    totalCount = parseInt(totalCount[0][0].count)
    if (totalCount > 0) {

      userOrderDetails = await sequelize.query(`select 
    lottery_user_tickets.lottery_id, 
    lottery_user_tickets.lottery_order_id,
    lottery_user_tickets.ticket_no,
    lottery_user_tickets.ticket_status,
    lottery_user_tickets.nft_tx_id,
    lottery_user_tickets.nft_id,
    lottery_user_tickets.ticket_filename,
    lottery_user_tickets."createdAt" ,
    lottery_user_tickets.nft_tx_currency
    from lottery_user_tickets 
    where lottery_user_tickets.lottery_id = ${req.body.lottery_id} AND lottery_user_tickets.ticket_status != 'transfer_completed' AND
    lottery_user_tickets.address = '${address}' order by "createdAt" DESC
    limit ${limit}
    offset ${offset}
    `);
    }
  }

  if(userOrderDetails.length > 0){
    userOrderDetails = userOrderDetails[0];
    console.log("Order Details  ", userOrderDetails);
    res.send({
      list: userOrderDetails.map((item) => {
        return {
          ...item,
          ticket_image:
            process.env.TICKET_URL_PREFIX +
            encodeURIComponent(item.ticket_filename ? item.ticket_filename : item.ticket_no) +
            ".svg"
        };
      }),
      ticketToBNBExchangeRate: ticketToBNBExchangeRate,
      ticketToTicketExchangeRate: ticketToTicketExchangeRate,
      totalCount: totalCount
    })
  }else {
    res.send({
      list: userOrderDetails,
      ticketToBNBExchangeRate: ticketToBNBExchangeRate,
      ticketToTicketExchangeRate: ticketToTicketExchangeRate,
      totalCount: totalCount
  })
}

}


async function getAllTransactions(req, res) {
  try{
  console.log("Inside getAllTransactions", req.body);
  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }
  let accountAddress = req.body.accountAddress;

  if(req.body.searchText !== undefined && req.body.searchText.trim()){

    let trxCount = await LotteryResults.findAll({
      attributes: [
        [sequelize.fn("count", sequelize.col("id")), "id"],
      ],
      where: { address: accountAddress, prize_payment_id: { [Op.ne]: "" }, lottery_id: req.body.searchText },
    });
    if (trxCount.length > 0) {
  
      let userTransactions = await LotteryResults.findAll({
        attributes: [
          "prize_payment_id",
          "prize_payment_currency",
          "lottery_id",
          "ticket_prize_info",
          "prize_payment_time",
          "total_prize",
          "total_ticket",
          "team_prize",
        ],
        where: { address: accountAddress, prize_payment_id: { [Op.ne]: ""}, lottery_id: req.body.searchText },
        order: [["lottery_id", "DESC"]],
        limit: limit,
        offset: offset,
      });
  
      console.log("All Transactions From User ", userTransactions);
      return res.send({
        list: userTransactions,
        total: trxCount.length
      });
    } else {
      return res.send({
        list: [],
        total: 0
      });
    }
  }

  let trxCount = await LotteryResults.findAll({
    attributes: [
      [sequelize.fn("count", sequelize.col("id")), "id"],
    ],
    where: { address: accountAddress, prize_payment_id: { [Op.ne]: "" } },
  });
  if (trxCount.length > 0) {

    let userTransactions = await LotteryResults.findAll({
      attributes: [
        "prize_payment_id",
        "prize_payment_currency",
        "lottery_id",
        "ticket_prize_info",
        "prize_payment_time",
        "total_prize",
        "total_ticket",
        "team_prize",
      ],
      where: { address: accountAddress, prize_payment_id: { [Op.ne]: "" } },
      order: [["lottery_id", "DESC"]],
      limit: limit,
      offset: offset,
    });

    console.log("All Transactions From User ", userTransactions);
    res.send({
      list: userTransactions,
      total: trxCount.length
    });
  } else {
    res.send({
      list: [],
      total: 0
    });
  }
}
catch(err)
{
  console.error("getAllTransactions=>",err.message)
}
}


async function testMintNFT() {
  let gasPrice = global.gasPrice;

  let data;
  try {
    data = await blockChainService.nftContractInstance.methods
      .mint(
        "0x16Ea5a0fada525EF239e13CdA7667934027A3EB1",
        "https://ipfs.io/ipfs/QmbSSd2U1XAQLFUT3duJYwywiANiZCZrimioptr59jN6Be"
      )
      .send({
        from: "0x98C00d4a8F47CCdD641744E8Fee804E99d504CDe",
        gasLimit: 200000,
        gasPrice: gasPrice,
      });
    console.log("NFT Tx Hash is ", data);
  } catch (error) {
    console.log("Error is ", error); // 'Returned error: unknown account'
  }
}


async function checkNFTTransferStatus(req, res) {
  console.log("Checking NFT Transfer Statuses", req.body.data);
  let trxArray = req.body.data;
  let trxStatusArray = [];

  for (let i = 0; i < trxArray.length; i++) {
    let trx = trxArray[i];
    trx = JSON.parse(trx);
    let ticketStatus = await sequelize.query(
      `select * from lottery_user_tickets where nft_tx_id = '${trx.txHash}' and ticket_status = 'completed'`
    );
    ticketStatus = ticketStatus[0][0];
    if (ticketStatus) {
      if (ticketStatus.ticket_status === "completed") {
        let trxDetails = {
          txHash: trx.txHash,
          status: "completed",
          nftId: ticketStatus.nft_id,
        };
        trxStatusArray.push(trxDetails);
      }
    }
  }
  res.send(trxStatusArray);
}



exports.tradeTicket = async function (req, res) {
  console.log("Ticket Transfer Request is ", req.body.transferDetails);

  const foundReq = await LotteryOrders.findOne({
    where: {
      pay_tx_id: req.body.transferDetails.transferTrxHash,
    },
  });

  if (foundReq != null) {
    res.send("order_exists");
    return;
  }

  const foundUser = await UserAccount.findOne({
    where: { address: req.body.transferDetails.accountAddress },
  });
  if (foundUser == null) {
    res.send("unreg_user");
    return;
  }  

  let ticketExchangeRates = await sequelize.query(
    `select * from properties where id in ('TICKET_TO_TICKET_EX_RATE', 'TICKET_TO_BNB_EX_RATE') order by id asc`
  );
  let ticketToBNBExchangeRate = JSON.parse(ticketExchangeRates[0][0].val);
  let ticketToTicketExchangeRate = JSON.parse(ticketExchangeRates[0][1].val); 
  let ticketsToTransferCount = req.body.transferDetails.transferTicketsArray.length

  let isValidCount=ticketsToTransferCount % ticketToTicketExchangeRate;
  if(isValidCount != 0)
  {
    res.send("invalid_ticket_count");
    return;
  }

    let totalPrice = JSON.parse(req.body.transferDetails.totalPrice);
    // excahnage rate in reverse here excahnage rate=no of old tickets required to get one new ticket
    let currentLotteryId = await getLatestLotteryId();
    const tradeOrder = await LotteryOrders.create({
      lottery_id: currentLotteryId,
      user_account_id: foundUser.dataValues.id,
      address: foundUser.dataValues.address,
      team_id: foundUser.dataValues.team_id,
      total_ticket: req.body.transferDetails.ticketsToBeGained,
      total_price: totalPrice,
      total_price_currency: global.currency,
      pay_tx_id: req.body.transferDetails.transferTrxHash,
      pay_tx_fee: null,
      pay_tx_currency: global.currency,
      status:Constants.ORDER_STATUS.CREATED,
      order_type:Constants.ORDER_TYPE.TRADE
    });

  

    await confirmTransferTicketTrx(req,res,tradeOrder);

  
};

async function confirmTransferTicketTrx(req,res,tradeOrder) {
  
  let txId=req.body.transferDetails.transferTrxHash;
  let transferObjectsArray=req.body.transferDetails.transferTicketsArray;
  let address= req.body.transferDetails.accountAddress;

  console.log("Inside confirmTransferTicketTrx", txId);
  setTimeout(async () => {
    const trxConfirmations = await blockChainService.getConfirmations(txId);
    console.log("Transaction with hash " + txId + " has " + trxConfirmations + " confirmation(s)");
    if (trxConfirmations && trxConfirmations >= Constants.TRADE_BLOCK_COUNT) {
      console.log("Transaction with hash " + txId + " has been confirmed");

      const item = await LotteryOrders.update({
        status: Constants.ORDER_STATUS.TRADE_PAYMENT_COMPLETED,
      }, {
        where: {
          id:tradeOrder.id
        }
      });
      // updte usertickets transfered
      // add new row for admin

      await setTransferStatus(transferObjectsArray, address,tradeOrder);

      res.send("trans_req_recorded");
      return
    }
    return await confirmTransferTicketTrx(req,res,tradeOrder);
  }, 5 * 1000)
}


async function setTransferStatus(transferObjectsArray, address,tradeOrder) {
  console.log("Ticket Transfer Request is ", transferObjectsArray);
  // Preparing a ticket number array
  let ticketsArray = [];
  for (let i = 0; i < transferObjectsArray.length; i++) {
    const element = transferObjectsArray[i];
    ticketsArray.push(element.ticket_no);
  }

  // let transferOrderId = transferOrder.id;

  const item = await LotteryUserTicket.update({
    ticket_status: Constants.TICKET_STATUS.TRANSFER_REQ,
    trade_order_id:tradeOrder.id
  }, {
    where: { ticket_no: { [Op.in]: ticketsArray }, address: address }
  })
}



async function transferTicketsToAdmin() {
  let transferTickets = await sequelize.query(
    `select id,address,ticket_no, nft_id, ticket_status from lottery_user_tickets where ticket_status = '${Constants.TICKET_STATUS.TRANSFER_REQ}' limit 6`
  );
  transferTickets = transferTickets[0];
  for (let i = 0; i < transferTickets.length; i++) {
    const element = transferTickets[i];
    let owner = element.address,
      tokenId = element.nft_id,
      ticketNo = element.ticket_no;
    let gasPrice = await global.gasPrice;

    try {
      let data = await blockChainService.nftContractInstance.methods
        .transferFrom(owner, global.adminAccountAddress, tokenId)
        .send({
          // from: availableAccount.address,
          from: global.adminAccountAddress,
          gasLimit: 250000,
          gasPrice: gasPrice,
        });

      await LotteryUserTicket.update(
        {
          transfer_nft_tx_id: data.transactionHash,
          ticket_status: Constants.TICKET_STATUS.TRANSFER_CONF_PENDING,
        },
        {
          where: {
            id:element.id
          },
        }
      );
      console.log("Transfer Tx Hash is ", data);
    } catch (error) {
      console.log("Error Occured", error);
    }
  }
}


async function verifyApprovalTransaction(req, res) {
  console.log("Inside verifyApprovalTransaction", req.body);
  let txId = req.body.txHash;

  const foundReq = await LotteryUser.findOne({
    where: {
      pay_tx_id: txId,
    },
  });

  if (foundReq != null) {
    res.send("order_exists");
    return;
  }

  const foundUser = await UserAccount.findOne({
    where: { address: req.body.accountAddress },
  });
  if (foundUser == null) {
    res.send("unreg_user");
    return;
  } else {
    console.log("Found User", foundUser.dataValues);

    const item = await LotteryUser.create({
      lottery_id: -3,  // -3 status for Granting Approval to Admin Case
      user_account_id: foundUser.dataValues.id,
      address: foundUser.dataValues.address,
      team_id: foundUser.dataValues.team_id,
      total_ticket: 0,
      total_price: 0,
      total_price_currency: global.currency,
      pay_tx_id: txId,
      pay_tx_fee: null,
      pay_tx_currency: global.currency,
      note: "admin_aprvl_pending",
    });
    let response = await confirmApprovalTransaction(req, res);
  }
}


async function confirmApprovalTransaction(req, res, confirmations = 1) {
  let txId = req.body.txHash;
  console.log("Inside confirmApprovalTransaction", req.body);
  setTimeout(async () => {
    const trxConfirmations = await blockChainService.getConfirmations(txId);
    console.log("Transaction with hash " + txId + " has " + trxConfirmations + " confirmation(s)");
    if (trxConfirmations >= confirmations) {
      console.log("Transaction with hash " + txId + " has been confirmed");

      const item = await LotteryUser.update({
        note: "admin_aprvl_received",
      }, {
        where: {
          pay_tx_id: txId,
        }
      });
      res.send("confirmed");
      return
    }
    return await confirmApprovalTransaction(req, res, confirmations);
  }, 5 * 1000)
}






async function fetchPendingUserOrders(req, res){
  console.log("Inside fetchPendingUserOrders ", req.body);
  let address = req.body.address

  let pendingUserOrders = await sequelize.query(`
  select pay_tx_id from lottery_orders 
  where address = '${address}'
  and status in (
    'validation_pending', 'validation_failed', 'verification_pending','verified')` );

  pendingUserOrders = pendingUserOrders[0];

  if(pendingUserOrders.length >0){
     res.send(pendingUserOrders.map((item)=> item.pay_tx_id))
  }
  else res.send([]);

}


async function updateShowCredit(req, res) {
  const accountAddress = req.body.accountAddress;
  const lotteryId = req.body.lotteryId;
  let showCreditAlert = 0;

    // let listOfLotteryArray = [];
    // lotteryId.forEach(element => {
    //   listOfLotteryArray.push(element.address)
    // });
    try{

    showCreditAlert = await LotteryResults.update(
      { result_view_status: true },
      {
        where: {
          address: accountAddress,
          lottery_id: lotteryId,
        },
      }
    );
    } catch(error){
      res.send(error);
    }

	  console.log("data " , showCreditAlert);
    res.send(showCreditAlert);

  return;
}

async function createNft() {


  let availableAccount = await sequelize.query(
    `select account_balance,admin_account_address as address from admin_accounts 
    where availability_status = 'available' 
    and account_balance > ${global.minNFTFee} order by account_balance desc limit ${Constants.IPFS_LIMIT}`
  );
  let availableAccountCount=availableAccount[0].length;
  availableAccount = availableAccount[0];

  if(availableAccountCount==0)
  {
    console.log("All admin accounts are busy");
    return;
  }

	let txList = await LotteryUserTicket.findAll({
    where: {
      ticket_status: { [Op.in]: [Constants.TICKET_STATUS.CREATED,
        Constants.TICKET_STATUS.IPFS_FAILED, 
        Constants.TICKET_STATUS.NFT_FAILED] }
    },
    limit: availableAccountCount
  })
  console.log("Transaction List With Pending NFT Transactions with Errors  ", txList);

  for (let i = 0; i < txList.length; i++) {
    let userTicket = txList[i].dataValues;
    uploadIPFSAndCreateNFT(userTicket,availableAccount[i]);
  }
}

async function uploadIPFSAndCreateNFT(userTicket,availableAccount)
{
  await LotteryUserTicket.update({
    ticket_status: Constants.TICKET_STATUS.PROCESSING,
  }, {
    where: { id:userTicket.id }
  });

  if (userTicket.ticket_status == Constants.TICKET_STATUS.CREATED 
    || userTicket.ticket_status == Constants.TICKET_STATUS.IPFS_FAILED) {

    let ipfsDetails = await uploadIpfsTicket(userTicket);
    console.log("NEW IPFS DETAILS  :  ", ipfsDetails);

    if (ipfsDetails != null) {
      //willingly setting mint_error to enable minting again.
      await LotteryUserTicket.update({
        ticket_status: Constants.TICKET_STATUS.IPFS_COMPLETED,
        nft_doc_hash: ipfsDetails.ticketUriHash,
        nft_pic_url: ipfsDetails.ticketImageHash
      }, {
        where: { id:userTicket.id }
      });
      userTicket.ticket_status = Constants.TICKET_STATUS.IPFS_COMPLETED;
      if (ipfsDetails.ticketUriHash) {
        userTicket.nft_doc_hash = ipfsDetails.ticketUriHash;
      }
      if (ipfsDetails.ticketImageHash) {
        userTicket.nft_pic_url = ipfsDetails.ticketImageHash;
      }
    }

  }
  if (!availableAccount) {
    await LotteryUserTicket.update(
      {
        ticket_status: Constants.TICKET_STATUS.NFT_FAILED,
      },
      {
        where: { id: userTicket.id },
      }
    );
  } else if (
    userTicket.ticket_status == Constants.TICKET_STATUS.IPFS_COMPLETED ||
    userTicket.ticket_status == Constants.TICKET_STATUS.NFT_FAILED
  ) {
    let txDetails = await blockChainService.mintNFT(
      userTicket,
      availableAccount
    );
  }
}

async function orderConfirmation() {
  let orderDetails;
  try{
   orderDetails = await sequelize.query(`select total_ticket, id, address from lottery_orders where status = '${Constants.ORDER_STATUS.PENDING}'`)
  }
  catch(err)
  {
    console.log(err);
  }
  orderDetails = orderDetails[0];
  console.log(" ORDER DETAILS ", orderDetails);

  for (let i = 0; i < orderDetails.length; i++) {
    let orderTicketCount = orderDetails[i].total_ticket;
    let orderId = orderDetails[i].id
    let mintTotal = await sequelize.query(`select COUNT(*) as total_ticket_order from lottery_user_tickets where lottery_order_id = ${orderId} AND (ticket_status = '${Constants.TICKET_STATUS.COMPLETED}')`)
    mintTotal = (mintTotal[0]);
    console.log("MINT TOTAL IN ORDER SCHEDULAR  ", mintTotal[0].total_ticket_order, orderTicketCount, orderDetails[i].id)
    if (mintTotal[0].total_ticket_order == orderTicketCount) {
      await LotteryOrders.update({
        status: Constants.ORDER_STATUS.COMPLETED,
        finish_time: date.format(new Date(), 'YYYY-MM-DD HH:mm:ss')
      }, {
        where: {
          id: orderDetails[i].id
        }
      })
      // order completed notification
      const notification = await Notification.create({
        event: Constants.EVENT.ORDER_COMPLETED,
        address: orderDetails[i].address,
        details: "Order id: " + orderDetails[i].id + " is completed",
        type: Constants.TYPE.ORDER_COMPLETED,
      });
    }
  }
}
async function ticketTransferOrderConfirmation() {
	let orderDetails = await sequelize.query(`select * from lottery_orders where order_type = '${Constants.ORDER_TYPE.TRADE}' and status = '${Constants.ORDER_STATUS.TRADE_PAYMENT_COMPLETED}'`)
		orderDetails = orderDetails[0];
		console.log(" ORDER DETAILS ", orderDetails);

		for (let i = 0; i < orderDetails.length; i++) {

      let transferTotal = await sequelize.query(`select ticket_status, COUNT(*) as total_ticket_order from lottery_user_tickets where trade_order_id = '${orderDetails[i].id}' group by ticket_status`);
	
      if(transferTotal[0].length==1&&transferTotal[0][0].ticket_status==Constants.TICKET_STATUS.TRANSFER_COMPLETED)
      {
        // it will start to create tickets
        await LotteryOrders.update({
          status: Constants.ORDER_STATUS.PAYMENT_COMPLETED
        }, {
          where: {
            id: orderDetails[i].id
          }
        })
      }
		}
}

async function transferNftConfirmation() {
	let txList = await LotteryUserTicket.findAll({
    where: {
      ticket_status: Constants.TICKET_STATUS.TRANSFER_CONF_PENDING
    },
  })
  console.log("Transaction List With Pending NFT Transaction Confirmations ", txList);
  for (let i = 0; i < txList.length; i++) {
    let userTicket = txList[i].dataValues;
    console.log("USER TICKET For Confimation ", i, " is  ", userTicket)
   
    let result=await confirmNFTTransaction(userTicket);
    const item = await LotteryUserTicket.update(
      {
        ...result,
        ticket_status: Constants.TICKET_STATUS.TRANSFER_COMPLETED,
      },
      {
        where: {
          id:userTicket.id
        },
      }
    );
      // trade ticket notification
    const notification = await Notification.create({
      event: Constants.EVENT.TRADE_COMPLETED,
      address: userTicket.address,
      details: "Ticket: " + userTicket.ticket_no + " is traded successfully.",
      type: Constants.TYPE.TRADE_TICKET_TO_ADMIN,
    });
  }
}

async function updateNftTransferProcess(req,res){
  console.log("updateNftTransferProcess " + req.body);

  let updateTransferredLottery = await LotteryUserTicket.update(
    { ticket_status: Constants.TICKET_STATUS.TRANSFER_IN_PROGRESS ,
      nft_tx_id: req.body.txHash,
     },
    {
      where: {
        address: req.body.fromAddress,
        nft_id: req.body.token,
      },
    }
  );

  return res.send(updateTransferredLottery);
}

async function transferLotteryOwner() {

  const transferredTickets = await LotteryUserTicket.findAll({
    where: { ticket_status: Constants.TICKET_STATUS.TRANSFER_IN_PROGRESS },
  });

  if(transferredTickets.length > 0){

    for (let i = 0; i < transferredTickets.length; i++) {
      const trx = await web3.eth.getTransaction(transferredTickets[i].dataValues.nft_tx_id);
      console.log(trx);
      if(!trx){continue}

      var receipt = await web3.eth.getTransactionReceipt(transferredTickets[i].dataValues.nft_tx_id)
      console.log(receipt);
      if(!receipt){continue}
  
      toAddress = "0x" + receipt.logs[1].topics[2].slice(26);
    
      try {
          // Lottery ticket exist
          lotteryTicket = transferredTickets[i].dataValues;
    
          // assuming lotteries table can't be empty
          let currentLotteryId = await sequelize.query(
            `select MAX(id) as id from lotteries`
          );
          currentLotteryId = currentLotteryId[0][0].id;
          // update lottery
          let oldLotteryTicket = await LotteryUserTicket.update(
            { ticket_status: Constants.TICKET_STATUS.TRANSFER_COMPLETED },
            {
              where: {
                address: trx.from.toLowerCase(),
                id: lotteryTicket.id,
              },
            }
          );
          const toUser = await UserAccount.findOne({
            where: { address: toAddress },
          });
          let total_point = 0;
    
          let obj = {
            lottery_id: lotteryTicket.lottery_id,
            ticket_no: lotteryTicket.ticket_no,
            nft_pic_url: lotteryTicket.nft_pic_url,
            nft_id: lotteryTicket.nft_id,
            nft_tx_id: transferredTickets[i].dataValues.nft_tx_id,
            nft_doc_hash: lotteryTicket.nft_doc_hash,
            nft_tx_fee: lotteryTicket.nft_tx_fee,
            nft_tx_currency: global.currency,
            total_point: total_point,
            lottery_order_id: -1,
            ticket_filename: lotteryTicket.ticket_filename,
          };
          // case 1: user exist in sytemtem
          if (toUser) {
            const item = await LotteryUserTicket.create({
              ...obj,
              user_account_id: toUser.dataValues.id,
              address: toUser.dataValues.address,
              team_id: toUser.dataValues.team_id,
              ticket_status: Constants.TICKET_STATUS.COMPLETED,
            });
          }
        // case 2: check transfer for admin
        if (toAddress === global.adminAccountAddress) {
          const item = await LotteryUserTicket.create({
            ...obj,
            user_account_id: 0,
            address: toAddress,
            team_id: 0,
            ticket_status: Constants.TICKET_STATUS.TRANSFERRED_TO_ADMIN,
          });
        }
        // case 3: ghost user
        else if(!toUser) {
          const ghostUser = await userController.createGhostUser(
            toAddress,
            lotteryTicket
          );
          const item = await LotteryUserTicket.create({
            ...obj,
            user_account_id: ghostUser.id,
            address: ghostUser.address,
            team_id: ghostUser.team_id,
    
            ticket_status: Constants.TICKET_STATUS.COMPLETED,
          });
        }
        // add notification
        const notificationToSender = await Notification.create({
          event: Constants.EVENT.TICKET_TRANSFERRED,
          address: trx.from.toLowerCase(),
          details: "Ticket: " + lotteryTicket.ticket_no + " is transferred successfully.",
          type: Constants.TYPE.TICKET_TRANSFERRED,
        });

        const notificationToReceiever = await Notification.create({
          event: Constants.EVENT.RECEIVED_TICKET,
          address: toAddress,
          details: "Ticket: " + lotteryTicket.ticket_no + " is received.",
          type: Constants.TYPE.TICKET_TRANSFERRED,
        });

        return
      } catch (err) {
        console.error(err);
        console.log(err);
      }
    }

    
  }
  return
}

async function getExtentedOrderList(req, res) {
  try{
  console.log("Inside getAllTransactions", req.body);

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  const valid = req.validate(
    [
      { field: "lotteryId", type: "NUMBER", isRequired: true },
      { field: "orderId", type: "NUMBER", isRequired: true },
      { field: "accountAddress", type: "STRING", isRequired: true },
    ],
    req.body
  );

  if (!valid) {
    return res.sendError();
  }

  let accountAddress = req.body.accountAddress.toLowerCase();
  let loteryId = req.body.lotteryId;
  let orderId = req.body.orderId;

   let userTransactions = await LotteryResults.findAll({
      attributes: [
        "prize_payment_id",
        "prize_payment_currency",
        "lottery_id",
        "ticket_prize_info",
        "prize_payment_time",
        "total_prize",
        "total_ticket",
        "team_prize",
      ],
      where: { address: accountAddress, lottery_id: loteryId },
    });

    let teamPrize = 0;
    let amountCredited = 0;
    let count = 0;

    if(userTransactions.length > 0){
      userTransactions[0].ticket_prize_info.forEach(element => {
        if(element.order_id == orderId){
          amountCredited = amountCredited + Number(element.prize);
          count++;
        }
      });

      teamPrize = (Number(userTransactions[0].team_prize) / parseInt(userTransactions[0].ticket_prize_info.length) ) * count;
      amountCredited = amountCredited + teamPrize;
    }

    console.log("All Transactions From User ", userTransactions);
    return res.send({userTransactions, teamPrize, amountCredited});
}
catch(err)
{
  console.error("getExtentedOrderList=>",err.message)
}
}

exports.transferNftConfirmation=transferNftConfirmation;
exports.ticketTransferOrderConfirmation=ticketTransferOrderConfirmation;
exports.orderConfirmation = orderConfirmation;
exports.createNft = createNft;
exports.updateShowCredit = updateShowCredit;
exports.checkOrderStatus = checkOrderStatus;
exports.fetchPendingUserOrders = fetchPendingUserOrders
exports.nftConfirmation = nftConfirmation;

exports.confirmNFTTransaction = confirmNFTTransaction;
exports.getCreditDetails = getCreditDetails;
exports.uploadIpfsTicket = uploadIpfsTicket;
exports.fetchPresentPricePerTicket = fetchPresentPricePerTicket;
exports.updateWinningNumberToLotteryTable = updateWinningNumberToLotteryTable;
exports.getLatestLotteryId = getLatestLotteryId;
exports.updateLotteryOwner = updateLotteryOwner;
exports.createNewLotteryEvent = createNewLotteryEvent;
exports.updatePricePerTicket = updatePricePerTicket;
exports.getAllLotteryTicketOrders = getAllLotteryTicketOrders;
exports.getAllLotteryTicketPoints = getAllLotteryTicketPoints;
exports.getSelectedOrderDetails = getSelectedOrderDetails;
exports.getReceivedOrderDetails = getReceivedOrderDetails;
exports.getAllNft = getAllNft;
exports.getAllTransactions = getAllTransactions;
exports.testMintNFT = testMintNFT;
exports.checkNFTTransferStatus = checkNFTTransferStatus;
exports.confirmTransferTicketTrx = confirmTransferTicketTrx;
exports.setTransferStatus = setTransferStatus;
exports.transferTicketsToAdmin = transferTicketsToAdmin;
exports.verifyApprovalTransaction = verifyApprovalTransaction;
exports.confirmApprovalTransaction = confirmApprovalTransaction;
exports.getTeamNameById=getTeamNameById;
exports.confirmOrderPaymentTransaction=confirmOrderPaymentTransaction;
exports.transferLotteryOwner=transferLotteryOwner;
exports.updateNftTransferProcess=updateNftTransferProcess;
exports.getExtentedOrderList=getExtentedOrderList;
