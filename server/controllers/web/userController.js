const db = require("../../models");
const fs = require("fs");
const {
  UserAccount,
  sequelize,
  UserAccountPayments,
  LotteryUserTicket,
  LotteryResults,
  Team,
} = require("../../models");
const blockChainService = require("../../services/blockChainService");
const Constants = require("../../Constants");

/* 
 @author:
 @date:14th April 2022
 @description:
*/
exports.getUserProfileDetails = async function (req, res) {
  console.log("Inside getUserProfileDetails", req.body);
  const { accountAddress } = req.body;

  const foundItem = await UserAccount.findOne({
    where: { address: accountAddress },
  });
  let teamWithMinUser;
  let teamWithMaxUser = [];

  if (foundItem) {
    if (foundItem.dataValues.team_id != null) {
      // teamWithMaxUser = await sequelize.query(
      //   `select id from teams where id <> ${foundItem.dataValues.team_id} and user_cnt > (select user_cnt from teams where id = ${foundItem.dataValues.team_id})order by user_cnt desc limit 1`
      // );

      teamWithMinUser = await sequelize.query(
        `select team_id, user_cnt from teams where id != ${foundItem.dataValues.team_id} order by user_cnt asc`
      );

      if (teamWithMinUser[0].length == 0) teamWithMaxUser = [];
      else {
        if (
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt &&
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][2].user_cnt
        ) {
          teamWithMaxUser = [];
        } else if (
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt
        ) {
          teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
        } else {
          // calculating 30% user
          var intvalue = Math.round(
            teamWithMinUser[0][0].user_cnt * 0.3 +
              teamWithMinUser[0][0].user_cnt
          );

          if (intvalue > teamWithMinUser[0][1].user_cnt) {
            teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
          } else {
            teamWithMaxUser.push(teamWithMinUser[0][1].team_id);
            teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
          }
        }
      }
    } else {
      // teamWithMaxUser = await sequelize.query(
      //   `select id from teams where user_cnt = (select min(user_cnt) from teams ) limit 2`
      // );

      teamWithMinUser = await sequelize.query(
        `select team_id, user_cnt from teams order by user_cnt asc`
      );

      if (teamWithMinUser[0].length == 0) teamWithMaxUser = [];
      else {
        if (
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt &&
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][2].user_cnt &&
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][3].user_cnt
        ) {
          teamWithMaxUser = [];
        } else if (
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt &&
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][2].user_cnt
        ) {
          teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
        } else if (
          teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt
        ) {
          teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
          teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
        } else {
          // calculating 30% user
          var intvalue = Math.round(
            teamWithMinUser[0][0].user_cnt * 0.3 +
              teamWithMinUser[0][0].user_cnt
          );

          if (intvalue > teamWithMinUser[0][1].user_cnt) {
            teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
            teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
          } else {
            teamWithMaxUser.push(teamWithMinUser[0][1].team_id);
            teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
            teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
          }
        }
      }
    }

    let teamChangeCost = await sequelize.query(
      `select * from properties where id = 'TEAM_CHANGE_COST' `
    );
    teamChangeCost = teamChangeCost[0][0].val;
    let avatarChangeCost = await sequelize.query(
      `select * from properties where id = 'AVATAR_CHANGE_COST' `
    );
    avatarChangeCost = avatarChangeCost[0][0].val;
    console.log("Found item", foundItem.dataValues);
    res.send({
      userData: foundItem.dataValues,
      teamChangeCost: JSON.parse(teamChangeCost),
      avatarChangeCost: JSON.parse(avatarChangeCost),
      teamWithMaxUser: teamWithMaxUser,
      // avatarImage: process.env.AVATAR_URL_PREFIX + encodeURIComponent(accountAddress) + '.svg'
    });
  } else {
    teamWithMinUser = await sequelize.query(
      `select team_id, user_cnt from teams order by user_cnt asc`
    );

    if (teamWithMinUser[0].length == 0) teamWithMaxUser = null;
    else {
      if (
        teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt &&
        teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][2].user_cnt &&
        teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][3].user_cnt
      ) {
        teamWithMaxUser = [];
      } else if (
        teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt &&
        teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][2].user_cnt
      ) {
        teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
      } else if (
        teamWithMinUser[0][0].user_cnt == teamWithMinUser[0][1].user_cnt
      ) {
        teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
        teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
      } else {
        // calculating 30% user
        var intvalue = Math.round(
          teamWithMinUser[0][0].user_cnt * 0.3 + teamWithMinUser[0][0].user_cnt
        );

        if (intvalue > teamWithMinUser[0][1].user_cnt) {
          teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
          teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
        } else {
          teamWithMaxUser.push(teamWithMinUser[0][1].team_id);
          teamWithMaxUser.push(teamWithMinUser[0][2].team_id);
          teamWithMaxUser.push(teamWithMinUser[0][3].team_id);
        }
      }
    }

    res.send({
      message: "NoUserFound",
      teamWithMaxUser: teamWithMaxUser,
    });
  }
};

/* 
 @author:
 @date:14th April 2022
 @description:
*/
async function saveUserProfileDetails(req, res) {
  console.log("Inside saveUserProfileDetails", req.body);
  let accountAddress, name, email, picUrl, teamId;
  if (req.body.userNewProfileDetails) {
    if (
      req.body.userNewProfileDetails.teamId == null ||
      req.body.userNewProfileDetails.teamId > 4 ||
      req.body.userNewProfileDetails.teamId < 1 ||
      isNaN(req.body.userNewProfileDetails.teamId)
    ) {
      res.send("invalid_team");
      return;
    }
    // let accountAddress, name, email, picUrl, teamId;

    // let { id, accountAddress, name, email, picUrl, teamId, totalTradeVolume, totalPoints, totalWinCount, createTime, updateTime, totalTradeCurrency } = req.body.userNewProfileDetails
    accountAddress = req.body.userNewProfileDetails.accountAddress;
    name = req.body.userNewProfileDetails.name;
    email = req.body.userNewProfileDetails.email;
    picUrl = req.body.userNewProfileDetails.picUrl;
    teamId = req.body.userNewProfileDetails.teamId;
  } else {
    if (
      req.body.teamId == null ||
      req.body.teamId > 4 ||
      req.body.teamId < 1 ||
      isNaN(req.body.teamId)
    ) {
      res.send("invalid_team");
      return;
    }

    accountAddress = req.body.accountAddress;
    name = req.body.name;
    email = req.body.email;
    picUrl = req.body.picUrl;
    teamId = req.body.teamId;
  }
  // const { id, accountAddress, name, email, picUrl, teamId, totalTradeVolume, totalPoints, totalWinCount, createTime, updateTime, totalTradeCurrency } = req.body

  const foundUser = await UserAccount.findOne({
    where: { address: accountAddress },
  });

  const foundTeam = await Team.findOne({
    where: { team_id: parseInt(teamId) },
  });

  console.log("Found Team", foundTeam);

  let teamName = "";
  if (parseInt(teamId) == 1) teamName = "Fire";
  else if (parseInt(teamId) == 2) teamName = "Water";
  else if (parseInt(teamId) == 3) teamName = "Air";
  else if (parseInt(teamId) == 4) teamName = "Earth";

  if (!foundUser) {
    let newPicUrl = await saveProfileAvatar(req);

    const teamUpdate = await Team.update(
      {
        user_cnt: foundTeam.dataValues.user_cnt + 1,
      },
      { where: { team_id: parseInt(teamId) } }
    );

    const createUser = await UserAccount.create({
      address: accountAddress,
      username: name,
      email: email,
      pic_url: newPicUrl,
      team_id: parseInt(teamId),
      total_trade_volume: 0,
      total_point: 0,
      total_win_cnt: 0,
      total_trade_currency: global.currency,
    });

    if (!req.body.userNewProfileDetails) res.send("success");
  } else if (foundUser && picUrl == "") {
    let newPicUrl = await saveProfileAvatar(req);

    if (parseInt(teamId) != foundUser.dataValues.team_id) {
      await Team.update(
        {
          user_cnt: sequelize.literal(`user_cnt + 1`),
        },
        { where: { team_id: parseInt(teamId) } }
      );

      await Team.update(
        {
          user_cnt: sequelize.literal(`user_cnt - 1`),
        },
        { where: { team_id: foundUser.dataValues.team_id } }
      );
    }

    await LotteryResults.update(
      {
        prize_payment_status: Constants.PRIZE_PAYMENT_STATUS.PENDING,
      },
      {
        where: {
          address: accountAddress,
          prize_payment_status: Constants.PRIZE_PAYMENT_STATUS.PAYMENT_CLAIM_PENDING,
        },
      }
    );

    const item = await UserAccount.update(
      {
        address: accountAddress,
        username: name,
        email: email,
        pic_url: newPicUrl,
        team_id: parseInt(teamId),
      },
      { where: { address: accountAddress } }
    );
    if (!req.body.userNewProfileDetails) res.send("success");
  } else {
    // Found an item, update it
    if (parseInt(teamId) != foundUser.dataValues.team_id) {
      await Team.update(
        {
          user_cnt: sequelize.literal(`user_cnt + 1`),
        },
        { where: { team_id: parseInt(teamId) } }
      );

      await Team.update(
        {
          user_cnt: sequelize.literal(`user_cnt - 1`),
        },
        { where: { team_id: foundUser.dataValues.team_id } }
      );
    }

    const item = await UserAccount.update(
      {
        address: accountAddress,
        username: name,
        email: email,
        pic_url: picUrl,
        team_id: parseInt(teamId),
      },
      { where: { address: accountAddress } }
    );
    if (!req.body.userNewProfileDetails) res.send("success");
  }
}

/* 
 @author:
 @date:14th April 2022
 @description:
*/
async function saveUserProfileAvatar(req, res) {
  console.log(" Inside saveUserProfileAvatar ", req.body);
  const avatarTheme = fs.readFileSync(
    __dirname + "/../../templates/avatar.svg",
    "utf8"
  );
  if (avatarTheme) {
    console.log("Avatar Template Found ");
    let newAvatarName = req.body.userNewProfileDetails.accountAddress + "_new";
    let oldAvatarName = req.body.userNewProfileDetails.accountAddress + "_old";

    try {
      const userAvatar = fs.readFileSync(
        __dirname + "/../../avatars/" + newAvatarName + ".svg",
        "utf8"
      );

      if (userAvatar) {
        fs.rename(
          __dirname + "/../../avatars/" + newAvatarName + ".svg",
          __dirname + "/../../avatars/" + oldAvatarName + ".svg",
          function (err) {
            if (err) console.log("ERROR: " + err);
          }
        );
      }

      var change = avatarTheme.replace(
        "imageSrc",
        JSON.stringify(req.body.userNewProfileDetails.avatarImageSrc)
      );

      fs.writeFileSync(
        __dirname + "/../../avatars/" + newAvatarName + ".svg",
        change
      );
    } catch (error) {
      fs.writeFileSync(
        __dirname + "/../../avatars/" + newAvatarName + ".svg",
        avatarTheme.replace(
          "imageSrc",
          JSON.stringify(req.body.userNewProfileDetails.avatarImageSrc)
        )
      );
    }

    if (req.body.txId) {
      let profileChangeResponse = await UserAccountPayments.create({
        address: req.body.userNewProfileDetails.accountAddress,
        amount: req.body.userNewProfileDetails.netAmountToBeCharged,
        tx_id: req.body.txId,
        status: "completed",
        currency: global.currency,
        type: "avatar_changed",
        type_data: {
          old: req.body.userNewProfileDetails.accountAddress + "_old.svg",
          new: req.body.userNewProfileDetails.accountAddress + "_new.svg",
        },
      });
    }

    if (!req.body.userNewProfileDetails.picUrl) {
      const createUser = await UserAccount.create({
        address: req.body.userNewProfileDetails.accountAddress,
        username: "",
        email: "",
        pic_url:
          process.env.AVATAR_URL_PREFIX +
          encodeURIComponent(req.body.userNewProfileDetails.accountAddress) +
          "_new.svg",
        team_id: null,
        total_trade_volume: 0,
        total_point: 0,
        total_win_cnt: 0,
        total_trade_currency: global.currency,
      });

      let newPicUrl =
        process.env.AVATAR_URL_PREFIX +
        encodeURIComponent(req.body.userNewProfileDetails.accountAddress) +
        "_new.svg";
      res.send(newPicUrl);
      return;
    }

    await UserAccount.update(
      {
        pic_url:
          process.env.AVATAR_URL_PREFIX +
          encodeURIComponent(req.body.userNewProfileDetails.accountAddress) +
          "_new.svg",
      },
      {
        where: {
          address: req.body.userNewProfileDetails.accountAddress,
        },
      }
    );

    let newPicUrl =
      process.env.AVATAR_URL_PREFIX +
      encodeURIComponent(req.body.userNewProfileDetails.accountAddress) +
      "_new.svg";
    res.send(newPicUrl);
    return;
  } else {
    console.log("Some Error Occured. No Template data Found");
    res.send("error");
    return;
  }
}

async function confirmAndUpdateAvatar(req, res, confirmations = 1) {
  console.log("Request is ", req.body);
  setTimeout(async () => {
    let response = await checkTrxValidation(req, res);
    if (response == "trx_exists") {
      res.send("trx_exists");
      return;
    } else if (response == "invalid_team") {
      res.send("invalid_team");
      return;
    } else if (response == "invalid_trx") {
      res.send("invalid_trx");
      return;
    } else if (response == "invalid_price_paid") {
      res.send("invalid_price_paid");
      return;
    } else if (response == "invalid_user") {
      res.send("invalid_user");
      return;
    } else {
      // Get current number of confirmations and compare it with sought-for value
      const trxConfirmations = await blockChainService.getConfirmations(
        req.body.txId
      );

      console.log(
        "Transaction with hash " +
          req.body.txId +
          " has " +
          trxConfirmations +
          " confirmation(s)"
      );

      if (trxConfirmations >= confirmations) {
        console.log(
          "Transaction with hash " +
            req.body.txId +
            " has been successfully confirmed"
        );
        await saveUserProfileAvatar(req, res);

        // await updateUserAccountPaymentTable(req, res);
        // res.send("success")

        return;
      }
      // Recursive call
      return await confirmAndUpdateAvatar(req, res, confirmations);
    }
  }, 20 * 1000);
}

async function confirmTransactionForProfileUpdate(req, res, confirmations = 1) {
  console.log("Request is ", req.body);
  setTimeout(async () => {
    let response = await checkTrxValidation(req, res);
    if (response == "trx_exists") {
      res.send("trx_exists");
      return;
      getCreditDetails;
    } else if (response == "invalid_team") {
      res.send("invalid_team");
      return;
    } else if (response == "invalid_trx") {
      res.send("invalid_trx");
      return;
    } else if (response == "invalid_price_paid") {
      res.send("invalid_price_paid");
      return;
    } else if (response == "invalid_user") {
      res.send("invalid_user");
      return;
    } else {
      // Get current number of confirmations and compare it with sought-for value
      const trxConfirmations = await blockChainService.getConfirmations(
        req.body.txId
      );

      console.log(
        "Transaction with hash " +
          req.body.txId +
          " has " +
          trxConfirmations +
          " confirmation(s)"
      );

      if (trxConfirmations >= confirmations) {
        console.log(
          "Transaction with hash " +
            req.body.txId +
            " has been successfully confirmed"
        );
        await saveUserProfileDetails(req, res);

        await updateUserAccountPaymentTable(req, res);
        res.send("success");

        return;
      }
      // Recursive call
      return await confirmTransactionForProfileUpdate(req, res, confirmations);
    }
  }, 20 * 1000);
}

async function saveProfileAvatar(req) {
  console.log(" Inside saveUserProfileAvatar ", req.body);
  const avatarTheme = fs.readFileSync(
    __dirname + "/../../templates/avatar.svg",
    "utf8"
  );
  if (avatarTheme) {
    console.log("Avatar Template Found ");
    let newAvatarName = req.body.accountAddress + "_new";
    let oldAvatarName = req.body.accountAddress + "_old";

    try {
      const userAvatar = fs.readFileSync(
        __dirname + "/../../avatars/" + newAvatarName + ".svg",
        "utf8"
      );

      if (userAvatar) {
        fs.rename(
          __dirname + "/../../avatars/" + newAvatarName + ".svg",
          __dirname + "/../../avatars/" + oldAvatarName + ".svg",
          function (err) {
            if (err) console.log("ERROR: " + err);
          }
        );
      }

      var change = avatarTheme.replace(
        "imageSrc",
        JSON.stringify(req.body.avatarImageSrc)
      );

      fs.writeFileSync(
        __dirname + "/../../avatars/" + newAvatarName + ".svg",
        change
      );
    } catch (error) {
      fs.writeFileSync(
        __dirname + "/../../avatars/" + newAvatarName + ".svg",
        avatarTheme.replace("imageSrc", JSON.stringify(req.body.avatarImageSrc))
      );
    }

    let newPicUrl =
      process.env.AVATAR_URL_PREFIX +
      encodeURIComponent(req.body.accountAddress) +
      "_new.svg";
    // res.send(newPicUrl);
    return newPicUrl;
  } else {
    console.log("Some Error Occured. No Template data Found");
    // res.send("error")
    return "error";
  }
}
async function createGhostUser(toAddress, lotteryTicket) {
  const createUser = await UserAccount.create({
    address: toAddress,
    username: toAddress.slice(0, 6),
    email: "",
    pic_url: "",
    team_id: parseInt(lotteryTicket.team_id),
    total_trade_volume: 0,
    total_point: 0,
    total_win_cnt: 0,
    total_trade_currency: global.currency,
  });

  const foundTeam = await Team.findOne({
    where: { team_id: parseInt(lotteryTicket.team_id) },
  });
  // Update user count
  const teamUpdate = await Team.update(
    {
      user_cnt: foundTeam.dataValues.user_cnt + 1,
    },
    { where: { team_id: parseInt(lotteryTicket.team_id) } }
  );

  return createUser.dataValues;
}
async function checkTrxValidation(req, res) {
  let trxResponse = await UserAccountPayments.findOne({
    where: {
      tx_id: req.body.txId,
    },
  });
  if (trxResponse != null) {
    // res.send("trx_exists");
    return "trx_exists";
  }

  if (
    req.body.userNewProfileDetails.teamId > 4 &&
    req.body.userNewProfileDetails.teamId < 1
  ) {

    return "invalid_team";
  }

  let teamChangeCost = await sequelize.query(
    `select * from properties where id = 'TEAM_CHANGE_COST' `
  );
  teamChangeCost = JSON.parse(teamChangeCost[0][0].val);
  let avatarChangeCost = await sequelize.query(
    `select * from properties where id = 'AVATAR_CHANGE_COST' `
  );
  avatarChangeCost = JSON.parse(avatarChangeCost[0][0].val);

  
  let trx;
  try {
    trx = await web3.eth.getTransaction(req.body.txId);
    if (trx == null) {
      // res.send("invalid_trx");
      return "invalid_trx";
    }
    let amountPaid =
      req.body.userNewProfileDetails.netAmountToBeCharged.toString();
    var tokens = web3.utils.toWei(amountPaid, "ether");
    if (trx.value != tokens) {
      // res.send("invalid_price_paid");
      return "invalid_price_paid";
    }
    let actualAmountToBeCharged = 0;
    if (req.body.userNewProfileDetails.avatarChanged)
      actualAmountToBeCharged = actualAmountToBeCharged + avatarChangeCost;
    if (req.body.userNewProfileDetails.teamChanged)
      actualAmountToBeCharged = actualAmountToBeCharged + teamChangeCost;
    actualAmountToBeCharged = web3.utils.toWei(
      actualAmountToBeCharged.toString(),
      "ether"
    );
    if (trx.value != actualAmountToBeCharged) {
      // res.send("invalid_price_paid");
      return "invalid_price_paid";
    }
   
  } catch (error) {
    // res.send("invalid_trx");
    return "invalid_trx";
  }

  let teamWithMaxUser = await sequelize.query(
    `select id from teams where id <> ${req.body.userNewProfileDetails.oldUserAccount.team_id} and user_cnt > (select user_cnt from teams where id = ${req.body.userNewProfileDetails.oldUserAccount.team_id})order by user_cnt desc limit 1`
  );

  if (teamWithMaxUser[0].length != 0) {
    teamWithMaxUser = teamWithMaxUser[0][0].id;
    if (teamWithMaxUser == req.body.userNewProfileDetails.teamId) {
  
      return "invalid_team";
    }
  }
  if (
    trx.from.toLowerCase() !=
    req.body.userNewProfileDetails.accountAddress.toLowerCase()
  ) {
    // res.send("invalid_user");
    return "invalid_user";
  }
  // }
}
async function updateUserAccountPaymentTable(req, res) {
  let profileChangeResponse;
  if (req.body.userNewProfileDetails.avatarChanged)
    profileChangeResponse = await UserAccountPayments.create({
      address: req.body.userNewProfileDetails.accountAddress,
      amount: req.body.userNewProfileDetails.netAmountToBeCharged,
      tx_id: req.body.txId,
      status: "completed",
      currency: global.currency,
      type: "avatar_changed",
      type_data: {
        old: req.body.userNewProfileDetails.oldUserAccount.pic_url,
        new: req.body.userNewProfileDetails.picUrl,
      },
    });
  if (req.body.userNewProfileDetails.teamChanged)
    profileChangeResponse = await UserAccountPayments.create({
      address: req.body.userNewProfileDetails.accountAddress,
      amount: req.body.userNewProfileDetails.netAmountToBeCharged,
      tx_id: req.body.txId,
      status: "completed",
      currency: global.currency,
      type: "team_changed",
      type_data: {
        old: req.body.userNewProfileDetails.oldUserAccount.team_id,
        new: req.body.userNewProfileDetails.teamId,
      },
    });
}
exports.updateUserAccountPaymentTable = updateUserAccountPaymentTable;
exports.createGhostUser = createGhostUser;
exports.confirmTransactionForProfileUpdate = confirmTransactionForProfileUpdate;
exports.saveProfileAvatar = saveProfileAvatar;
exports.confirmAndUpdateAvatar = confirmAndUpdateAvatar;
exports.saveUserProfileAvatar = saveUserProfileAvatar;
exports.saveUserProfileDetails = saveUserProfileDetails;
