const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
} = require("../../models");

const { Team } = require("../../models");
const { Lottery } = require("../../models");
const user = require("../../models/userAccount");
const Op = db.Sequelize.Op;
const date = require("date-and-time");
require("dotenv").config();
const crypto = require("crypto");
const properties = require("../../models/properties");

exports.getAdminAllProperties = async function (req, res) {
  console.log("getAll");

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }

  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    let totalCount = await sequelize.query(`select count(id) from properties where upper(id) ilike upper('%${req.body.searchText}%') `)
    totalCount = parseInt(totalCount[0][0].count);

    let id = req.body.searchText.toUpperCase();

    if (totalCount > 0) {
      const properties = await await sequelize.query(`select * from properties where upper(id) ilike upper('%${req.body.searchText}%') order by id Asc limit ${limit} offset ${offset}`)
    console.log("All Past Lotteries History Details ", properties[0]);
    return res.send({
      totalCount : totalCount,
      list : properties[0]
    });
    } else {
      return res.send({
        totalCount: 0,
        list: []
      });
    }
  }

  let totalCount = await sequelize.query(`select count(id) from properties`)
  totalCount = parseInt(totalCount[0][0].count);

  if (totalCount > 0) {
    const properties = await Properties.findAll({
      limit: 10,
      offset: offset,
    });

  console.log("All Past Lotteries History Details ", properties);
  return res.send({
    totalCount : totalCount,
    list : properties
  });
  } else {
    return res.send({
      totalCount: 0,
      list: []
    });
  }

};

  async function addAdminProperties(req, res) {
    console.log("Inside addAdminProperties", req.body);

    if(Object.keys(req.body).length === 0 || !req.body.value.trim() || !req.body.id.trim()){
      return res.status(400).send("Invalid Input");
    }
    
    try{
    const property = await Properties.create({
            id: req.body.id,
            val: req.body.value
        });
    } catch(error){
      return res.status(500).send("Error Adding Property " + error);
    }
  
    console.log("Added property ", property);
    res.send(property);
  }
  exports.addAdminProperties = addAdminProperties;

  async function updateAdminProperties(req, res) {
    console.log("Inside updateAdminProperties", req.body);

    if(Object.keys(req.body).length === 0 || !req.body.value.trim() || !req.body.id.trim()){
      return res.status(400).send("Invalid Input");
    }

    let property;
    
    try{
    property = await Properties.update(
        { val: req.body.value },
        {
          where: {
            id: req.body.id
          },
        }
      );
      } catch(error){
        return res.status(500).send("Error Updating Property " + error);
      }
  
    console.log("Updated property ", property);
    res.send(property);
  }
  exports.updateAdminProperties = updateAdminProperties;