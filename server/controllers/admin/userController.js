const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
} = require("../../models");

const { Team } = require("../../models");
const { Lottery } = require("../../models");
const user = require("../../models/userAccount");
const Op = db.Sequelize.Op;
const date = require("date-and-time");
require("dotenv").config();
const crypto = require("crypto");
const properties = require("../../models/properties");
const nodemailer = require('nodemailer');
const otpGenerator = require('otp-generator');
const passwordGenerator = require('generate-password');
const adminController = require('../admin/adminLoginController');

exports.getAllUsers = async function (req, res) {
  console.log("getAllUsers");

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  if (!req.body.role.trim()) {
    return res.send("Please Add Role");
  }

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }
  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    let userRole = req.body.role === "user" ? "role = 'user'" : "role = 'admin' or role = 'support'"
    let totalCount = await sequelize.query(`select count(id) from user_accounts where (upper(username) ilike upper('%${req.body.searchText}%') or
    upper(address) ilike upper('%${req.body.searchText}%') or upper(email) ilike upper('%${req.body.searchText}%')) and (${userRole})`)
    totalCount = parseInt(totalCount[0][0].count);

    if (totalCount > 0) {
      const userList = await sequelize.query(`select * from user_accounts where (upper(username) ilike upper('%${req.body.searchText}%') or
      upper(address) ilike upper('%${req.body.searchText}%') or upper(email) ilike upper('%${req.body.searchText}%')) and (${userRole})
      order by id Asc limit ${limit} offset ${offset} `);
      console.log("All Past Lotteries History Details ", userList[0][0]);
     return res.send({
        totalCount: totalCount,
        list: userList[0]
      });
    } else {
      return res.send({
        totalCount: 0,
        list: []
      });
    }
  }
  if (req.body.role === "user") {
    let totalCount = await sequelize.query(`select count(id) from user_accounts where role = '${req.body.role}' `)
    totalCount = parseInt(totalCount[0][0].count);

    if (totalCount > 0) {
      const userList = await UserAccount.findAll({
        where: { role: req.body.role },
        order: [["createdAt", "DESC"]],
        limit: 10,
        offset: offset,
      });
      console.log("All Past Lotteries History Details ", userList);
      return res.send({
        totalCount: totalCount,
        list: userList
      });
    } else {
      return res.send({
        totalCount: 0,
        list: []
      });
    }
  } else {
    let totalCount = await sequelize.query(`select count(id) from user_accounts where role = 'admin' or role = 'support' `)
    totalCount = parseInt(totalCount[0][0].count);

    if (totalCount > 0) {
      const userList = await UserAccount.findAll({
        where: {[Op.or]: [{role:  "admin"}, {role:  "support"}]},
        order: [["createdAt", "DESC"]],
        limit: 10,
        offset: offset,
      });
      console.log("All Past Lotteries History Details ", userList);
      return res.send({
        totalCount: totalCount,
        list: userList
      });
    } else {
      return res.send({
        totalCount: 0,
        list: []
      });
    }
  }

};

  async function adminCreateUser(req, res) {
    console.log("Inside adminCreateUser", req.body);

    if (Object.keys(req.body).length === 0) {
      return res.send("Empty Body");
    }

    if(!req.body.role.trim() || !req.body.name.trim() || !req.body.email.trim()){
      res.status(400).send("Invalid Input");
      return
    }

    let role = req.body.role;
    
    if(role === "super_admin"){
      res.status(400).send("Invalid Role");
      return
    }

    let password = passwordGenerator.generate({
        length: 10,
        numbers: true
    });

    const hashPassword = await adminController.decryptPassword(password);

    try{
      const createUser = await UserAccount.create({
        address: "",
        username: req.body.name,
        email: req.body.email,
        pic_url: "",
        team_id: -1,
        total_trade_volume: 0,
        total_point: 0,
        total_win_cnt: 0,
        total_trade_currency: global.currency,
        password: hashPassword,
        role: role,
        status: 'active'
      });
    } catch(error){
      return res.status(500).send("Error During Creating User " + error);
    }

    let subject = 'Confidential ';
    let message = 'Your password is ' + password

    await emailProviderService.sendEmail(req.body.email, subject, message);
  
    res.send(createUser.dataValues);
  }
  exports.adminCreateUser = adminCreateUser;

  async function disableUser(req, res) {
    console.log("Inside disableUser", req.body);

    if (Object.keys(req.body).length === 0) {
      return res.send("Empty Body");
    }

    if(!req.body.email.trim() || !req.body.status.trim()){
      res.status(400).send("Invalid Input");
      return
    }
    
    try{

    const user = await UserAccount.update(
      {
        status: req.body.status,
      },
      {
        where: {
          email: req.body.email,
        },
      }
    );
  
    console.log("User disabled ", user);
    res.send(user);
    }catch(error){
      return res.status(500).send("Error Disabling User " + error);
    }
  }
  exports.disableUser = disableUser;