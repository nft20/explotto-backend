const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  Lottery,
  Team,
  LotteryUser,
  LotteryUserTicket,
  LotteryResults
} = require("../../models");

const Op = db.Sequelize.Op;
const date = require("date-and-time");
require("dotenv").config();
const crypto = require("crypto");
const properties = require("../../models/properties");

exports.getAllLotteryResult = async function (req, res) {
  console.log("getAllLotteryResult");
  
  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit;
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset");
    return;
  }

  try{
    if(req.body.searchText !== undefined && req.body.searchText.trim()){
      const reg = /^\d+$/;
      if(reg.test(req.body.searchText)){
        let totalCount = await sequelize.query(`select count(id) from lottery_results where lottery_results.lottery_id =${req.body.searchText}`);
      totalCount = parseInt(totalCount[0][0].count);
    
      if (totalCount > 0) {
        const lotteries = await sequelize.query(`select * from lottery_results where lottery_results.lottery_id =${req.body.searchText} order by id Desc limit ${limit} offset ${offset}`);
        console.log("All Lotteries Result Details ", lotteries);
        return res.send({
          totalCount: totalCount,
          list: lotteries[0],
        });
      } else {
        return res.send({
          totalCount: 0,
          list: [],
        });
      }
      }
      let totalCount = await sequelize.query(`select count(id) from lottery_results where lottery_results.address ilike '%${req.body.searchText}%' or lottery_results.prize_payment_id ilike '%${req.body.searchText}%'
      or lottery_results.prize_payment_status ilike '%${req.body.searchText}%'`);
      totalCount = parseInt(totalCount[0][0].count);
    
      if (totalCount > 0) {
        const lotteries = await sequelize.query(`select * from  lottery_results where lottery_results.address ilike '%${req.body.searchText}%' or lottery_results.prize_payment_id ilike '%${req.body.searchText}%'
        or lottery_results.prize_payment_status ilike '%${req.body.searchText}%' order by id Desc limit ${limit} offset ${offset}`);
        console.log("All Lotteries Result Details ", lotteries);
        return res.send({
          totalCount: totalCount,
          list: lotteries[0],
        });
      } else {
        return res.send({
          totalCount: 0,
          list: [],
        });
      }
    }

    let totalCount = await sequelize.query(`select count(id) from lottery_results `);
    totalCount = parseInt(totalCount[0][0].count);
  
    if (totalCount > 0) {
      const lotteries = await LotteryResults.findAll({
        order: [["createdAt", "DESC"]],
        limit: 10,
        offset: offset,
      });
      console.log("All Lotteries Result Details ", lotteries);
      return res.send({
        totalCount: totalCount,
        list: lotteries,
      });
    } else {
      return res.send({
        totalCount: 0,
        list: [],
      });
    }
  } catch(error){
    console.log(error);
    return res.status(400).send({
        message: 'Error During Process!'
     });
  }

};

async function changePaymentStatus(req, res) {
  console.log("Inside changePaymentStatus", req.body);

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  const valid = req.validate(
    [
      { field: "lotteryId", type: "NUMBER", isRequired: true },
      { field: "status", type: "STRING", isRequired: true },
      { field: "address", type: "STRING", isRequired: true },
    ],
    req.body
  );

  if (!valid) {
    return res.sendError();
  }

  
  try{

  const result = await LotteryResults.update(
    {
      prize_payment_status: req.body.status,
    },
    {
      where: {
        lottery_id: req.body.lotteryId,
        address: req.body.address
      },
    }
  );

  console.log("Payment Status ", result);
  return res.send(result);
  }catch(error){
    return res.status(500).send("Error Changing payment Status " + error);
  }
}
exports.changePaymentStatus = changePaymentStatus;