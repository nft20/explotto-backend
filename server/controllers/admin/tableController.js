const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
} = require("../../models");

const { Team } = require("../../models");
const { Lottery } = require("../../models");
const user = require("../../models/userAccount");
const Op = db.Sequelize.Op;
const date = require("date-and-time");
require("dotenv").config();
const Constants = require('../../Constants');

exports.getAllLotteryEventDetails = async function (req, res) {
  console.log("getAllLotteryEventDetails");
  
  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit;
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset");
    return;
  }

  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    const reg = /^\d+$/;
    if(reg.test(req.body.searchText)){
      let totalCount = await sequelize.query(`select count(id) from lotteries where id = ${req.body.searchText}`);
      totalCount = parseInt(totalCount[0][0].count);
    
      if (totalCount > 0) {
        const pastLotteries = await Lottery.findAll({
          where: { 
            id: req.body.searchText
          },
          order: [["createdAt", "DESC"]],
          limit: limit,
          offset: offset,
        });
        console.log("All Past Lotteries History Details ", pastLotteries);
        return res.send({
          totalCount: totalCount,
          list: pastLotteries,
        });
      } else {
        return res.send({
          totalCount: 0,
          list: [],
        });
      }
    }
    
    let totalCount = await sequelize.query(`select count(id) from lotteries where winning_no ilike '%${req.body.searchText}%'`);
    totalCount = parseInt(totalCount[0][0].count);
  
    if (totalCount > 0) {
      const pastLotteries = await sequelize.query(`select * from lotteries where winning_no ilike '%${req.body.searchText}%' order by id Desc limit ${limit} offset ${offset}`);
      console.log("All Past Lotteries History Details ", pastLotteries[0]);
      return res.send({
        totalCount: totalCount,
        list: pastLotteries[0],
      });
    } else {
      return res.send({
        totalCount: 0,
        list: [],
      });
    }
  }

  let totalCount = await sequelize.query(`select count(id) from lotteries `);
  totalCount = parseInt(totalCount[0][0].count);

  if (totalCount > 0) {
    const pastLotteries = await Lottery.findAll({
      order: [["createdAt", "DESC"]],
      limit: limit,
      offset: offset,
    });
    console.log("All Past Lotteries History Details ", pastLotteries);
    return res.send({
      totalCount: totalCount,
      list: pastLotteries,
    });
  } else {
    return res.send({
      totalCount: 0,
      list: [],
    });
  }
};

async function getAdminAllLotteryOrders(req, res) {
  console.log("Inside getAdminAllLotteryOrders", req.body);
  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit;
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset");
    return;
  }

  if(Object.keys(req.body).length === 0 || req.body.selectedLottery.length === 0){
    res.send("invalid_selectedOrder");
    return;
  }

  if(req.body.searchText !== undefined && req.body.searchText.trim()){
    const reg = /^\d+$/;
    if(reg.test(req.body.searchText)){
      let totalCount = await sequelize.query(
        `select count(id) from lottery_orders where lottery_id = ${req.body.selectedLottery.id} and id = ${req.body.searchText}`
      );
      totalCount = parseInt(totalCount[0][0].count);
    
      if (totalCount > 0) {
        let userOrders = await LotteryOrders.findAll({
          where: { 
            lottery_id: req.body.selectedLottery.id ,
            id: req.body.searchText
          },
          order: [
            ["id", "DESC"],
            ["createdAt", "DESC"],
          ],
          limit: limit,
          offset: offset,
        });
    
        console.log("All Orders From User ", userOrders);
       return res.send({
          totalCount: totalCount,
          list: userOrders,
        });
      } else {
        return res.send({
          totalCount: 0,
          list: [],
        });
      }
    }

    let totalCount = await sequelize.query(
      `select count(id) from lottery_orders where lottery_id = ${req.body.selectedLottery.id} and address ilike '%${req.body.searchText}%'`
    );
    totalCount = parseInt(totalCount[0][0].count);
  
    if (totalCount > 0) {
      let userOrders = await sequelize.query(
        `select * from lottery_orders where lottery_id = ${req.body.selectedLottery.id} and address ilike '%${req.body.searchText}%' order by id Desc limit ${limit} offset ${offset}`
      );
  
      console.log("All Orders From User ", userOrders[0]);
     return res.send({
        totalCount: totalCount,
        list: userOrders[0],
      });
    } else {
      return res.send({
        totalCount: 0,
        list: [],
      });
    }
  }

  let totalCount = await sequelize.query(
    `select count(id) from lottery_orders where lottery_id = ${req.body.selectedLottery.id} `
  );
  totalCount = parseInt(totalCount[0][0].count);

  if (totalCount > 0) {
    let userOrders = await LotteryOrders.findAll({
      where: { lottery_id: req.body.selectedLottery.id },
      order: [
        ["id", "DESC"],
        ["createdAt", "DESC"],
      ],
      limit: limit,
      offset: offset,
    });

    console.log("All Orders From User ", userOrders);
    return res.send({
      totalCount: totalCount,
      list: userOrders,
    });
  } else {
   return res.send({
      totalCount: 0,
      list: [],
    });
  }
}
exports.getAdminAllLotteryOrders = getAdminAllLotteryOrders;

async function getAdminSelectedOrderDetails(req, res) {
  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit;
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset");
    return;
  }

  if(Object.keys(req.body).length === 0 || req.body.selectedOrder.length === 0){
    res.send("invalid_selectedOrder");
    return;
  }

  let totalCount = await sequelize.query(
    `select count(id)
    from lottery_user_tickets 
    where lottery_user_tickets.lottery_order_id = ${req.body.selectedOrder.id} AND
    lottery_user_tickets.lottery_id = ${req.body.selectedOrder.lottery_id} AND
    lottery_user_tickets.address = '${req.body.selectedOrder.address}' `
  );
  totalCount = parseInt(totalCount[0][0].count);

  let resultCount = await sequelize.query(`select count(id) from lottery_results where lottery_results.lottery_id = ${req.body.selectedOrder.lottery_id} AND lottery_results.address = '${req.body.selectedOrder.address}'`);
  resultCount = parseInt(resultCount[0][0].count);

  if (totalCount > 0 && resultCount > 0) {
  let userOrderDetails = await sequelize.query(`select lottery_user_tickets.lottery_id,lottery_user_tickets.address,lottery_user_tickets.team_id,lottery_user_tickets.ticket_no,
  lottery_user_tickets.total_point, lottery_results.total_prize
  from lottery_user_tickets inner join lottery_results on lottery_user_tickets.lottery_id = lottery_results.lottery_id
  where lottery_user_tickets.lottery_order_id = ${req.body.selectedOrder.id} AND
  lottery_user_tickets.lottery_id = ${req.body.selectedOrder.lottery_id} AND
  lottery_user_tickets.address = '${req.body.selectedOrder.address}' AND lottery_results.address = '${req.body.selectedOrder.address}'
  limit ${limit}
  offset ${offset}
  `);

  userOrderDetails = userOrderDetails[0];
  console.log("Order Details  ", userOrderDetails);
  return res.send({
    totalCount: totalCount,
    list: userOrderDetails,
  });
  }
  else if (totalCount > 0) {
    let userOrderDetails = await sequelize.query(`select lottery_user_tickets.lottery_id,lottery_user_tickets.address,lottery_user_tickets.team_id,lottery_user_tickets.ticket_no,
    lottery_user_tickets.total_point
    from lottery_user_tickets
    where lottery_user_tickets.lottery_order_id = ${req.body.selectedOrder.id} AND
    lottery_user_tickets.lottery_id = ${req.body.selectedOrder.lottery_id} AND
    lottery_user_tickets.address = '${req.body.selectedOrder.address}'
    limit ${limit}
    offset ${offset}
    `);
  
    userOrderDetails = userOrderDetails[0];
    console.log("Order Details  ", userOrderDetails);
    return res.send({
      totalCount: totalCount,
      list: userOrderDetails,
    });
    } else {
    return res.send({
      totalCount: 0,
      list: [],
    });
  }
}
exports.getAdminSelectedOrderDetails = getAdminSelectedOrderDetails;

async function getAdminTotalFeeAndNFTFee(req, res) {
  console.log("Inside getOrderDetails", req.body.selectedLottery);

  if(Object.keys(req.body).length === 0 || req.body.selectedLottery.length === 0){
    res.send("invalid_selectedLottery");
    return;
  }

  let feeOnNFT = await sequelize.query(`select sum(nft_tx_fee) as nft_fee
  from lottery_user_tickets 
  where 
  lottery_user_tickets.lottery_id = ${req.body.selectedLottery.id} `);

  feeOnNFT = feeOnNFT[0][0].nft_fee;

  let feeOnSend = await sequelize.query(`select prize_payment_fee
  from lottery_results 
  where 
  lottery_results.lottery_id = ${req.body.selectedLottery.id} `);

  feeOnSend = feeOnSend[0].length === 0 || feeOnSend[0][0].prize_payment_fee === null ? 0 : feeOnSend[0][0].prize_payment_fee ;
  feeOnNFT = feeOnNFT === null ? 0: feeOnNFT
  res.send({
    feeOnNFT,
    feeOnSend
});
}
exports.getAdminTotalFeeAndNFTFee = getAdminTotalFeeAndNFTFee;

async function changeOrderStatus(req, res) {
  console.log("Inside changeOrderStatus", req.body);

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  const valid = req.validate(
    [
      { field: "id", type: "NUMBER", isRequired: true },
      { field: "status", type: "STRING", isRequired: true },
    ],
    req.body
  );

  if (!valid) {
    return res.sendError();
  }

  
  try{

  const order = await LotteryOrders.update(
    {
      status: req.body.status,
    },
    {
      where: {
        id: req.body.id,
      },
    }
  );

  console.log("order Availibity Status ", order);
  res.send(order);
  }catch(error){
    return res.status(500).send("Error Changing Order Status " + error);
  }
}
exports.changeOrderStatus = changeOrderStatus;

async function getUserAndPic(req, res) {
  console.log("Inside getUserAndPic", req.body);

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit;
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset");
    return;
  }

  const valid = req.validate(
    [
      { field: "address", type: "Array", isRequired: true },
    ],
    req.body
  );

  if (!valid) {
    return res.sendError();
  }
  
  try{
    const totalCount = await UserAccount.count({
      where: { address: req.body.address },
      limit: limit,
      offset: offset,
    });
  
    if(totalCount>0){
      const users = await UserAccount.findAll({
        where: { address: req.body.address },
        limit: limit,
        offset: offset,
      });
  
    console.log("Users ", users);
    return res.send({
      totalCount: totalCount,
      list: users,
    });
    }else{
      return res.send({
        totalCount: 0,
        list: [],
      });
    }

  }catch(error){
    return res.status(500).send("Error getting users" + error);
  }
}
exports.getUserAndPic = getUserAndPic;

