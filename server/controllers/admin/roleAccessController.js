const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
  ApiRoleAccess,
  AdminApi,
} = require("../../models");
const { LotteryUserTicket } = require("../../models");
const { LotteryUser } = require("../../models");
const { Team } = require("../../models");
const { Lottery } = require("../../models");
const user = require("../../models/userAccount");
const Op = db.Sequelize.Op;

const nftContractABI = global.nftContractABI;
const date = require("date-and-time");
require("dotenv").config();
const crypto = require("crypto");

exports.getAllApis = async function (req, res) {
    console.log("getAllUsers");

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }
    let limit = 10;
    if (req.body.limit) {
      limit = req.body.limit
    }
    let offset = req.body.pageNumber * limit;
    if (offset < 0) {
      res.send("invalid_offset")
      return
    }

    if(req.body.searchText !== undefined && req.body.searchText.trim()){
      let totalCount = await sequelize.query(`select count(id) from admin_apis where api_name ilike lower('%${req.body.searchText}%') and id NOT IN (1,2,3,4)`)
      totalCount = parseInt(totalCount[0][0].count);
    
      if (totalCount > 0) {
        const apiList = await sequelize.query(
          `select admin_apis.id, admin_apis.api_name, api_role_accesses.roles_id from admin_apis left join api_role_accesses on admin_apis.id = api_role_accesses.admin_apis_id 
          where admin_apis.api_name ilike lower('%${req.body.searchText}%') and admin_apis.id NOT IN (1,2,3,4)
          order by admin_apis.id Asc limit ${limit} offset ${offset}`
        );
        console.log("All API list ", apiList[0]);
        return res.send({
          totalCount : totalCount,
          list : apiList[0]
        });
      } else {
       return res.send({
          totalCount: 0,
          list: []
        });
      }
    }
  
    let totalCount = await sequelize.query(`select count(id) from admin_apis where id NOT IN (1,2,3,4) `)
    totalCount = parseInt(totalCount[0][0].count);
  
    if (totalCount > 0) {
      const apiList = await sequelize.query(
        `select admin_apis.id, admin_apis.api_name, api_role_accesses.roles_id from admin_apis left join api_role_accesses on admin_apis.id = api_role_accesses.admin_apis_id where admin_apis.id NOT IN (1,2,3,4)
        order by admin_apis.id Asc limit ${limit} offset ${offset}`
      );
      console.log("All API list ", apiList[0]);
      return res.send({
        totalCount : totalCount,
        list : apiList[0]
      });
    } else {
      return res.send({
        totalCount: 0,
        list: []
      });
    }
  };

async function addRoleAccess(req, res) {
    console.log("Inside addRoleAccess", req.body.roleArray);

    if (Object.keys(req.body).length === 0 || req.body.roleArray.length === 0) {
        return res.send("Invalid Data");
    }

    let roleArray = req.body.roleArray;

    try{
    for (i = 0; i < roleArray.length; i++) {
        let foundItem = await ApiRoleAccess.findOne({ where: { admin_apis_id: roleArray[i].id } });
        if (foundItem) {
            let item = await ApiRoleAccess.update(
                {
                    roles_id: roleArray[i].roles_id
                },
                {
                    where: {
                        admin_apis_id: roleArray[i].id
                    },
                }
            );

        } else {
            let item = await ApiRoleAccess.create({
                admin_apis_id: roleArray[i].id,
                roles_id: roleArray[i].roles_id
            });
        }
    }
    res.send("Success");
  }catch(error){
    return res.status(500).send("Error Adding Role " + error);
  }
}
  exports.addRoleAccess = addRoleAccess;