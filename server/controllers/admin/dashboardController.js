const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
} = require("../../models");

const { Team } = require("../../models");
const { Lottery } = require("../../models");
const user = require("../../models/userAccount");
const Op = db.Sequelize.Op;
const date = require("date-and-time");
require("dotenv").config();
const moment = require("moment");
const { duration } = require("moment");


exports.loteryStat = async function (req, res) {
  console.log("loteryStat");

  const valid = req.validate(
    [
      { field: "duration", type: "STRING", isRequired: true },
    ],
    req.body
  );
  if (!valid) {
    return res.sendError();
  }

  let lotteryId = await sequelize.query(
    `select id from lotteries order by id desc limit 7`
  );
  lotteryId = lotteryId[0];

  let startDate = '';
  let endDate = '' ;

  if(req.body.duration === 'today'){
    startDate = moment().startOf('day').format();     
    endDate = moment().endOf('day').format();
  }else if(req.body.duration === 'yesterday'){
    startDate = moment().subtract(1, 'days').startOf('day').format();     
    endDate = moment().subtract(1, 'days').endOf('day').format();
  }else if(req.body.duration === 'week'){
    startDate = moment().startOf('week').format();     
    endDate = moment().endOf('week').format();
  }
  

  if (req.body.duration === 'allTime') {
    let pastLotteries = await sequelize.query(`select count(id) as lottery, sum(total_ticket) as total_ticket, sum(total_user) as total_user, 
          sum(total_fund) as total_fund, SUM (CAST (fund_distribution  ->> 'prize_pool' AS Float)) as prize_pool,
            SUM (CAST (fund_distribution  ->> 'maintenance' AS Float)) as maintenance from lotteries  `);
    console.log("All Past Lotteries History Details ", pastLotteries[0][0]);

    res.send(pastLotteries[0][0]);
  } else {
    let pastLotteries = await sequelize.query(`select count(id) as lottery, sum(total_ticket) as total_ticket, sum(total_user) as total_user, 
    sum(total_fund) as total_fund, SUM (CAST (fund_distribution  ->> 'prize_pool' AS Float)) as prize_pool,
      SUM (CAST (fund_distribution  ->> 'maintenance' AS Float)) as maintenance from lotteries where start_time between '${startDate}' and '${endDate}' `);
  console.log("All Past Lotteries History Details ", pastLotteries[0][0]);

  res.send(pastLotteries[0][0]);
  }
};

async function getAdminTotalExpense(req, res) {
  console.log("Inside getOrderDetails", req.body.selectedLottery);
  let feeOnNFT = 0;
  let feeOnSend = 0;
  let lotteryId = 0;

  let startDate = '';
  let endDate = '' ;

  if(req.body.duration === 'today'){
    startDate = moment().startOf('day').format();     
    endDate = moment().endOf('day').format();
  }else if(req.body.duration === 'yesterday'){
    startDate = moment().subtract(1, 'days').startOf('day').format();     
    endDate = moment().subtract(1, 'days').endOf('day').format();
  }else if(req.body.duration === 'week'){
    startDate = moment().startOf('week').format();     
    endDate = moment().endOf('week').format();
  }

  if (req.body.duration != undefined && req.body.duration !='allTime') {
    lotteryId = await sequelize.query(
      `select id from lotteries where start_time between '${startDate}' and '${endDate}'`
    );
    lotteryId = lotteryId[0];
  }

  if (req.body.duration === 'today') {
    feeOnNFT = await sequelize.query(`select sum(nft_tx_fee) as nft_fee
        from lottery_user_tickets 
        where 
        lottery_user_tickets.lottery_id = ${lotteryId[0].id} `);

    feeOnNFT = feeOnNFT[0][0].nft_fee;

    feeOnSend = await sequelize.query(`select prize_payment_fee
        from lottery_results 
        where 
        lottery_results.lottery_id = ${lotteryId[0].id} `);
    feeOnSend = feeOnSend[0].length === 0 || feeOnSend[0][0].prize_payment_fee === null ? 0 : feeOnSend[0][0].prize_payment_fee ;
  }
  else if (req.body.duration === 'yesterday') {
    feeOnNFT = await sequelize.query(`select sum(nft_tx_fee) as nft_fee
        from lottery_user_tickets 
        where 
        lottery_user_tickets.lottery_id = ${lotteryId[0].id} `);

    feeOnNFT = feeOnNFT[0][0].nft_fee;

    feeOnSend = await sequelize.query(`select prize_payment_fee
        from lottery_results 
        where 
        lottery_results.lottery_id = ${lotteryId[0].id} `);
    feeOnSend = feeOnSend[0].length === 0 || feeOnSend[0][0].prize_payment_fee === null ? 0 : feeOnSend[0][0].prize_payment_fee ;
  }
  else if (req.body.duration === 'week') {
    const lotteryIdLenght = lotteryId.length - 1 ;
    feeOnNFT = await sequelize.query(`select sum(nft_tx_fee) as nft_fee
            from lottery_user_tickets 
            where 
            lottery_user_tickets.lottery_id between ${lotteryId[lotteryIdLenght].id} and ${lotteryId[0].id}`);

    feeOnNFT = feeOnNFT[0][0].nft_fee;

    feeOnSend = await sequelize.query(`select sum(prize_payment_fee) as feeOnSend
            from lottery_results 
            where 
            lottery_results.lottery_id between ${lotteryId[lotteryIdLenght].id} and ${lotteryId[0].id} `);
    feeOnSend =  feeOnSend[0].length === 0 || feeOnSend[0][0].feeonsend === null ? 0: feeOnSend[0][0].feeonsend
  }
  else {
    feeOnNFT = await sequelize.query(`select sum(nft_tx_fee) as nft_fee
            from lottery_user_tickets `);

    feeOnNFT = feeOnNFT[0][0].nft_fee;

    feeOnSend = await sequelize.query(`select sum(prize_payment_fee) as feeOnSend
            from lottery_results `);
    feeOnSend =  feeOnSend[0].length === 0 || feeOnSend[0][0].feeonsend === null ? 0: feeOnSend[0][0].feeonsend
  }

  feeOnNFT = feeOnNFT === null ? 0: feeOnNFT;
  res.send({
    feeOnNFT,
    feeOnSend
  });
}
exports.getAdminTotalExpense = getAdminTotalExpense;