const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
} = require("../../models");

const { Team } = require("../../models");
const { Lottery } = require("../../models");
const user = require("../../models/userAccount");
const Op = db.Sequelize.Op;
const web3=global.web3;
const nftContractABI = global.nftContractABI;
const date = require("date-and-time");
require("dotenv").config();
const crypto = require("crypto");
const properties = require("../../models/properties");
const blockChainService = require('../../services/blockChainService');

exports.getAllAdminAccounts = async function (req, res) {
  console.log("getAllAdminAccounts");

  if (Object.keys(req.body).length === 0) {
    return res.send("Empty Body");
  }

  let limit = 10;
  if (req.body.limit) {
    limit = req.body.limit
  }
  let offset = req.body.pageNumber * limit;
  if (offset < 0) {
    res.send("invalid_offset")
    return
  }
try{
  if(req.body.accountAddress !== undefined && req.body.accountAddress.trim()){
    address = req.body.accountAddress;
    let totalCount = await sequelize.query(`select count(id) from admin_accounts where admin_account_address = '${address}'`)
    totalCount = parseInt(totalCount[0][0].count);
  
    if (totalCount > 0) {
    const accounts = await AdminAccounts.findAll({
      where: { admin_account_address: address },
      order: [["createdAt", "DESC"]],
      limit: 10,
      offset: offset,
    });
    console.log("All Accounts Details ", accounts);
    return res.send({
      totalCount : totalCount,
      list : accounts
    });
  } else {
    return res.send({
      totalCount: 0,
      list: []
    });
  }
  }

  let totalCount = await sequelize.query(`select count(id) from admin_accounts `)
  totalCount = parseInt(totalCount[0][0].count);

  if (totalCount > 0) {
  const accounts = await AdminAccounts.findAll({
    order: [["createdAt", "DESC"]],
    limit: 10,
    offset: offset,
  });
  console.log("All Accounts Details ", accounts);
  return res.send({
    totalCount : totalCount,
    list : accounts
  });
} else {
  return res.send({
    totalCount: 0,
    list: []
  });
}
} catch(error){
  return res.sendError(error);
}
  
};

exports.verifySecretKey = async function (req, res) {
    console.log("verifySecretKey");

    if (Object.keys(req.body).length === 0) {
      return res.send("Empty Body");
    }

    if(!req.body.key.trim() || !req.body.toAddress.trim() || !req.body.amount.trim()){
      return res.status(400).send("Invalid Input");
    }

    if(process.env.MASTER_KEY === req.body.key){
        transferAmount(req,res);
    }else{
      res.status(400).send({message: "Key does not match"});
    }
  };

async function transferAmount(req, res) {
  let amount = req.body.amount;
  var tokens = web3.utils.toWei(amount, 'ether') ;   //totalPrice is a  string, while numberOfTickets is a number.
  var bntokens = web3.utils.toBN(tokens);

  let gasPrice = global.gasPrice;

  console.log("BN TOkens ", tokens, bntokens);

try{

  const nonce = await web3.eth.getTransactionCount(global.adminAccountAddress, 'latest');

  await web3.eth.sendTransaction({
    from: global.adminAccountAddress,
    to: req.body.toAddress,
    value: bntokens,
    gas: 50000,
    gasPrice: gasPrice,
    nonce: nonce,
    // chain: '0x61',
  }, async (err, data) => {
        console.log("DATA ", err, data)
        if(data){
            console.log("Transaction Hash Of BNB Transfer ", data);
            await confirmAmountTransfer(data,amount,req.body.toAddress,res)
        }
    });
  } catch (error) {
        console.log(error);
        return res.status(400).send({
            message: 'Error During Transaction!'
         });
    }
}

async function confirmAmountTransfer(
    txHash,
    amount,
    address,
    res,
    confirmations = 1
  ) {
    // Get current number of confirmations and compare it with sought-for value
    const trxConfirmations = await blockChainService.getConfirmations(txHash);
  
    console.log(
      "Transaction with hash " +
      txHash +
      " has " +
      trxConfirmations +
      " confirmation(s)"
    );
  
    if (trxConfirmations >= confirmations) {
      // Handle confirmation event according to your business logic
      console.log("Transaction with hash " + txHash + " has been successfully confirmed");
      const trx = await web3.eth.getTransaction(txHash);
      const trxReceipt = await web3.eth.getTransactionReceipt(txHash);
      let trxGasFee = await web3.utils.fromWei(trx.gasPrice, "ether");
      trxCharge = trxReceipt.gasUsed * JSON.parse(trxGasFee);
     
      await AdminAccountPayments.create({
        pay_tx_id: txHash, 
        amount: amount,
        from_account: global.adminAccountAddress,
        to_account: address,
        pay_status: "cnf_pending",
        pay_currency: global.currency,
        pay_fee: trxCharge
      });

      return res.send("Success");
      
    }
    // Recursive call
    return await confirmAmountTransfer( txHash,amount,address,res,confirmations );
  }

  async function changeAvailibilityStatus(req, res) {
    console.log("Inside changeAvailibilityStatus", req.body);

    if (Object.keys(req.body).length === 0) {
      return res.send("Empty Body");
    }

    const valid = req.validate(
      [
        { field: "address", type: "STRING", isRequired: true },
        { field: "status", type: "STRING", isRequired: true },
      ],
      req.body
    );

    
    try{

    const admin = await AdminAccounts.update(
      {
        availability_status: req.body.status,
      },
      {
        where: {
          admin_account_address: req.body.address,
        },
      }
    );
  
    console.log("Admin Availibity Status ", admin);
    res.send(admin);
    }catch(error){
      return res.status(500).send("Error Changing Availibity Status " + error);
    }
  }
  exports.changeAvailibilityStatus = changeAvailibilityStatus;