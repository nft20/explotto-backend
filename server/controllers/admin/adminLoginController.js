const db = require("../../models");
const {
  UserAccount,
  sequelize,
  TeamStat,
  UserAccountTeam,
  LotteryOrders,
  Properties,
  UserAccountPayments,
  AdminAccounts,
  AdminAccountPayments,
} = require("../../models");
const { Team } = require("../../models");
const { Lottery } = require("../../models");
const user = require("../../models/userAccount");
const Op = db.Sequelize.Op;
const date = require("date-and-time");
require("dotenv").config();
const crypto = require("crypto");
const properties = require("../../models/properties");
const otpGenerator = require('otp-generator');
const nodemailer = require('nodemailer');
const emailProviderService = require('../../services/emailProviderService');
const jwt = require("jsonwebtoken");
const moment = require("moment");


async function login(req, res) {
  console.log("Login data", req.body.data);

  if(Object.keys(req.body).length === 0 || !req.body.email.trim()){
    return res.status(400).send({
      message: "Invalid Data",
    });
  }

  try{
    let foundUser = await UserAccount.findOne({ where: { email: req.body.email } });
    if (foundUser === null) {
      return res.status(400).send({
        message: "Invalid Username or Password",
      });
    } else {
      foundUser = foundUser.dataValues;
      const hashPassword = await decryptPassword(req.body.password);
      if (foundUser.role !== "admin" && foundUser.role !== "super_admin" && foundUser.role !== "support") {
        return res.status(403).send({
          message: "Access forbidden",
        });
      } else if (foundUser.status === "disabled") {
        return res.status(403).send({
          message: "Account is disabled",
        });
      }else if (foundUser.password !== hashPassword) {
        return res.status(401).send({
          message: "Invalid Username or Password",
        });
      } else if (foundUser.password === hashPassword) {
        const token = jwt.sign(
          { user_id: foundUser.id, email: foundUser.email, role: foundUser.role },
          process.env.TOKEN_KEY,
          {
            expiresIn: "24h",
          }
        );
        const item = await UserAccount.update(
          {
            token: token
          },
          { where: { email: req.body.email  } }
        );
  
         return res.send({foundUser, token});
      } else {
        return res.status(400).send({
          message: "Invalid Username or Password",
        });
      }
    }
  } catch(error){
    return res.status(400).send({
      error
    });
  }

}
exports.login = login;

async function decryptPassword(password) {
  // Creating a unique salt for a particular user
  this.salt = process.env.MASTER_KEY;

  // Hashing user's salt and password with 1000 iterations,
  //64 length and sha256 digest
  return (this.hash = crypto
    .pbkdf2Sync(password, this.salt, 1000, 64, `sha256`)
    .toString(`hex`));
}
exports.decryptPassword = decryptPassword;

async function forgotPassword(req, res) {
  console.log("inside forgotPassword", req.body.data);

  if(Object.keys(req.body).length === 0 || !req.body.email.trim()){
    return res.status(400).send("Invalid Input");
  }
  
  let email = req.body.email;
  let subject = 'Account Recovery - Explotto';
  const otp = otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false, digits:true, lowerCaseAlphabets: false });
  let message = 'Your otp is ' + otp
  let currentDate = moment().add(10, 'minutes');

  let foundUser = await UserAccount.findOne({ where: { email: email } });
  if (foundUser === null) {
    return res.status(400).send({
      message: "User not found",
    });
  } else {
    let user = await UserAccount.update(
      { auth_data: otp,
        auth_data_time:  currentDate.utc().format()
       },
      {
        where: {
          email: email
        },
      }
    );


    await emailProviderService.sendEmail(email, subject, message);
    return res.send("Otp sent ");
  }
}
exports.forgotPassword = forgotPassword;

 async function verifyOtp(req, res) {
  console.log("verifyOtp data", req.body);

  if(Object.keys(req.body).length === 0 || !req.body.email.trim() || !req.body.otp.trim()){
    return res.status(400).send("Invalid Input");
  }

  let email = req.body.email;
  let currentDate = moment().utc().format();

  let foundUser = await UserAccount.findOne({ where: { email: email } });
  if (foundUser === null) {
    return res.status(400).send({
      message: "User not found",
    });
  } else {
     foundUser = foundUser.dataValues;
     
    if(moment(foundUser.auth_data_time).isSameOrAfter(moment(currentDate))) {
    if(foundUser.auth_data === req.body.otp){
      return res.send("Success");
    }else{
      return res.status(400).send({
        message: "Wrong OTP",
      });
    }
  }else{
    return res.status(400).send({
      message: "OTP Expired",
    });
  }
  }
}
exports.verifyOtp = verifyOtp;

async function resetPassword(req, res) {
  console.log("inside resetPassword", req.body.data);

  if(Object.keys(req.body).length === 0 || !req.body.email.trim() || !req.body.newPassword.trim()){
    return res.status(400).send({
      message: "Invalid input",
    });
  }

  let email = req.body.email;

  let foundUser = await UserAccount.findOne({ where: { email: email } });
  if (foundUser === null) {
    return res.status(400).send({
      message: "User not found",
    });
  } else {
    // foundUser = foundUser.dataValues;
    const hashPassword = await decryptPassword(req.body.newPassword);
    let user = await UserAccount.update(
      { password: hashPassword
       },
      {
        where: {
          email: email
        },
      }
    );

    return res.send("Password Changed Successfully");
  }
}
exports.resetPassword = resetPassword;