const db = require("../../models")
const user_account1 = db.user
const Op = db.Sequelize.Op;

user_account1.findAll({ where: {id : 1 }})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving user accounts."
      });
    });