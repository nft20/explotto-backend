

module.exports={
    ORDER_STATUS:{
        CREATED: 'created',//when create order
        PROCESSING: 'processing',//when its in progress
        PAYMENT_FAILED: 'payment_failed',//when payment failed 
        PAYMENT_COMPLETED: 'payment_completed',// will start to create tickets
        TICKET_PROCESSING: 'ticket_processing',//when local ticket processing starts
        TRADE_PAYMENT_COMPLETED: 'trade_payment_completed',// will start to create tickets
        PENDING:'pending',// tickets created and ipfs/nft process started
        COMPLETED:'completed',
    },
    TICKET_STATUS:{
        CREATED: 'created',//when create order
        PROCESSING: 'processing',//when its in progress
        FAILED:'failed',
        FILE_NOT_FOUND:'file_not_found',//if template file not found
        IPFS_FAILED:'ipfs_failed',
        IPFS_COMPLETED:'ipfs_completed',
        NFT_FAILED: 'nft_failed',
        NFT_CONFIRMATION_PENDING: 'nft_confirmation_pending',
        NFT_CONFIRMATION_PROCESSING: 'nft_confirmation_processing',//when its in progress
        COMPLETED:'completed',
        TRANSFER_REQ:'transfer_requested',
        TRANSFER_CONF_PENDING:'transfer_confirmation_pending',
        TRANSFER_COMPLETED:'transfer_completed',
        TRANSFERRED_TO_ADMIN:'transferred_to_admin',
        TRANSFER_IN_PROGRESS: 'transfer_in_progress',

    },
    PAYMENT_CONFIRM_BLOCK_COUNT:3,
    SEND_CONFIRM_BLOCK_COUNT:1,
    NFT_CONFIRM_BLOCK_COUNT:1,
    TRADE_BLOCK_COUNT:1,
    PAYMENT_TYPE:{
        TRADE_TICKET:'trade_ticket'
    },
    TRADE_TICKET_STATUS:{
        PAYMENT_CONFIRM_PENDING:'payment_confirmation_pending',
        COMPLETED:'completed',
    },
    ORDER_TYPE:{
        BUY:'buy',
        TRADE:'trade',
    },
    PRIZE_PAYMENT_STATUS:{
        PENDING:'pending',
        PAYMENT_IN_PROGRESS:'in_progress',
        PAYMENT_CONF_PENDING:'confirmation_pending',
        PAYMENT_CLAIM_PENDING:'claim_pending',
        COMPLETED:'completed',
        FAILED:'failed',
    },
    CURRENCY_DECIMAL:8,
    TOPUP_ADMIN_ACCOUNT_PAYMENT_STATUS:{
        PENDING:'pending',
        PROCESSING:'processing',
        FAILED:'failed',
        PAYMENT_CONF_PENDING:'confirmation_pending',
        COMPLETED:'completed',
    },
    MINT_APPROX_GAS:150000,
    IPFS_LIMIT:12,// 12 per minute
    ACCOUNT_STATUS:{
        AVAILABLE:'available',
        BUSY:'busy',
        INACTIVE:'inactive',
    },
    EVENT:{
        TICKET_TRANSFERRED:'ticket_transferred',
        TRADE_COMPLETED:'trade_completed',
        ORDER_COMPLETED:'order_completed',
        RECEIVED_TICKET:'ticket_received',
    },
    TYPE:{
        TICKET_TRANSFERRED:'ticket_transferred',
        TRADE_TICKET_TO_ADMIN:'trade_ticket_toAdmin',
        ORDER_COMPLETED:'order_completed',
    },
}
