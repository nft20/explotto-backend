module.exports = (sequelize, Sequelize) => {

    const Role = sequelize.define("roles", {

        role : {
            type: Sequelize.STRING
        },
    });

    return Role;

};