module.exports = (sequelize, Sequelize) => {

    const UserAccountTeam = sequelize.define("user_account_team", {
      user_account_id: {
        type: Sequelize.INTEGER
      },
      team_id: {
        type: Sequelize.INTEGER
      },
      trade_volume: {
        type: Sequelize.INTEGER
      },
      total_point: {
        type: Sequelize.INTEGER
      },
      win_cnt: {
        type: Sequelize.INTEGER
      },
      trade_currency: {
        type: Sequelize.STRING
      },
      is_active: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return UserAccountTeam;
  
};