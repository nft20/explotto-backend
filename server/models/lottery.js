module.exports = (sequelize, Sequelize) => {

    const Lottery = sequelize.define("lotteries", {
        name: {
            type: Sequelize.STRING
          },
        start_time: {
            type: Sequelize.STRING
        },
        end_time: {
            type: Sequelize.STRING
        },
        ticket_price: {
            type: Sequelize.INTEGER
        },
        ticket_price_currency: {
            type: Sequelize.STRING
        },
        total_fund :{
            type: Sequelize.INTEGER
        },
        total_fund_currency : {
            type: Sequelize.STRING
        },
        total_ticket : {
            type: Sequelize.INTEGER
        },
        total_user : {
            type: Sequelize.INTEGER
        },
        fund_distribution : {
            type: Sequelize.JSON
        },
        winning_no : {
            type: Sequelize.STRING
        },
        winning_teams : {
            type: [Sequelize.TEXT],
        },
        winning_users : {
            type: [Sequelize.TEXT]
        }
        
    });

    return Lottery;

};