module.exports = (sequelize, Sequelize) => {

    const AdminApi = sequelize.define("admin_apis", {

        api_name : {
            type: Sequelize.STRING
        },
    });

    return AdminApi;

};