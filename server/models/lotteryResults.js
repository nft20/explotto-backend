module.exports = (sequelize, Sequelize) => {

    const LotteryResults = sequelize.define("lottery_results", {
        lottery_id: {
            type: Sequelize.INTEGER
        },
        address: {
            type: Sequelize.STRING
        },

        total_ticket: {
            type: Sequelize.INTEGER
          },
        total_ticket_price: {
            type: Sequelize.INTEGER
        },
        total_prize: {
            type: Sequelize.INTEGER
        },
        tickets_prize: {
            type: Sequelize.INTEGER
        },
        ticket_prize_info: {
            type: Sequelize.JSON
        },
        team_prize :{
            type: Sequelize.INTEGER
        },
        prize_payment_id : {
            type: Sequelize.STRING
        },
        prize_payment_time : {
            type: Sequelize.STRING
        },
        prize_payment_status : {
            type: Sequelize.STRING
        },
        result_view_status : {
            type: Sequelize.BOOLEAN
        },
        team_id : {
            type: Sequelize.INTEGER
        },
        prize_payment_currency:{
            type: Sequelize.STRING
        },
        prize_payment_fee:{
            type: Sequelize.INTEGER
        },
        error : {
            type: Sequelize.STRING
        },
        
    });

    return LotteryResults;

};