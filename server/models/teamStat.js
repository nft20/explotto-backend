module.exports = (sequelize, Sequelize) => {

    const TeamStat = sequelize.define("team_stat", {

        team_id : {
            type : Sequelize.INTEGER
        },
        lottery_id : {
            type : Sequelize.INTEGER
        },
        user_cnt : {
            type: Sequelize.INTEGER
        },
        ticket_cnt : {
            type: Sequelize.INTEGER
        },
        total_point: {
            type: Sequelize.INTEGER
        },
        rank: {
            type: Sequelize.INTEGER
        },
        total_prize_amount: {
            type: Sequelize.INTEGER
        },
        total_trade_currency: {
            type: Sequelize.STRING
        }
    });

    return TeamStat;

};