module.exports = (sequelize, Sequelize) => {

    const AdminAccounts = sequelize.define("admin_accounts", {
        admin_account_name: {
            type: Sequelize.STRING
          },
        admin_account_address: {
            type: Sequelize.STRING
        },
        en_private_key: {
            type: Sequelize.STRING
        },
        availability_status : {
            type: Sequelize.STRING
        },
        account_balance : {
            type: Sequelize.INTEGER
        }
    });

    return AdminAccounts;

};