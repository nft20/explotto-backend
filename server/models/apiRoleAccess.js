module.exports = (sequelize, Sequelize) => {

    const ApiRoleAccess = sequelize.define("api_role_accesses", {

        admin_apis_id: {
            type: Sequelize.INTEGER
          },

        roles_id : {
            type: [Sequelize.INTEGER]
        },
    });

    return ApiRoleAccess;

};