module.exports = (sequelize, Sequelize) => {

    const AdminAccountPayments = sequelize.define("admin_account_payments", {
      from_account: {
        type: Sequelize.STRING
      },
      to_account : {
        type: Sequelize.STRING
      },
      amount : {
        type: Sequelize.INTEGER
      },
      pay_currency : {
        type: Sequelize.STRING
      },
      pay_status : {
        type: Sequelize.STRING
      },
      pay_tx_id : {
        type: Sequelize.STRING
      },
      pay_fee : {
        type: Sequelize.INTEGER
      },
    });
  
    return AdminAccountPayments;
  
};