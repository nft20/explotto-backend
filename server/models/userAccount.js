module.exports = (sequelize, Sequelize) => {

    const UserAccount = sequelize.define("user_account", {
      address: {
        type: Sequelize.STRING
      },
      username: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      }
      ,
      pic_url: {
        type: Sequelize.STRING
      },
      team_id: {
        type: Sequelize.INTEGER
      },
      total_trade_volume: {
        type: Sequelize.INTEGER
      },
      total_point: {
        type: Sequelize.INTEGER
      },
      total_win_cnt: {
        type: Sequelize.INTEGER
      },
      total_trade_currency: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },     
      role: {
        type: Sequelize.STRING
      },     
      auth_data: {
        type: Sequelize.STRING
      },     
      auth_data_time: {
        type: Sequelize.DATE
      },     
      status: {
        type: Sequelize.STRING
      },
      token: {
        type: Sequelize.STRING
      }
    });
  
    return UserAccount;
  
};