module.exports = (sequelize, Sequelize) => {

    const LotteryOrders = sequelize.define("lottery_orders", {
        lottery_id: {
            type: Sequelize.INTEGER
        },
        user_account_id: {
            type: Sequelize.INTEGER
          },
        address: {
            type: Sequelize.STRING
        },
        team_id: {
            type: Sequelize.INTEGER
        },
        total_ticket: {
            type: Sequelize.INTEGER
        },
        total_price: {
            type: Sequelize.INTEGER
        },
        total_price_currency :{
            type: Sequelize.STRING
        },
        pay_tx_id : {
            type: Sequelize.STRING
        },
        pay_tx_fee : {
            type: Sequelize.STRING
        },
        pay_tx_currency : {
            type: Sequelize.STRING
        },
        status : {
            type: Sequelize.STRING
        },
        error : {
            type: Sequelize.STRING
        },
        order_type : {
            type: Sequelize.STRING
        },

    });

    return LotteryOrders;

};