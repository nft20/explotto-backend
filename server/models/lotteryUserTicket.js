module.exports = (sequelize, Sequelize) => {

    const LotteryUserTicket = sequelize.define("lottery_user_ticket", {
        lottery_order_id: {
            type: Sequelize.INTEGER
        },
        lottery_id: {
            type: Sequelize.INTEGER
        },
        user_account_id: {
            type: Sequelize.INTEGER
          },
        address: {
            type: Sequelize.STRING
        },
        team_id: {
            type: Sequelize.INTEGER
        },
        ticket_no: {
            type: Sequelize.STRING
        },
        nft_pic_url: {
            type: Sequelize.STRING
        },
        nft_id :{
            type: Sequelize.STRING
        },
        nft_tx_id : {
            type: Sequelize.STRING
        },
        nft_doc_hash : {
            type: Sequelize.INTEGER
        },
        nft_tx_fee : {
            type: Sequelize.STRING
        },
        nft_tx_currency : {
            type: Sequelize.STRING
        },
        ticket_status : {
            type: Sequelize.STRING
        },
        total_point: {
            type: Sequelize.STRING
        },
      
       
       
        ticket_filename : {
            type : Sequelize.STRING
        },
        transfer_nft_tx_id : {
            type : Sequelize.STRING
        },
        trade_order_id: {
            type: Sequelize.INTEGER
        },
        ticket_price: {
            type: Sequelize.DECIMAL
        },
        nft_created_by : {
            type : Sequelize.STRING
        },
    });

    return LotteryUserTicket;

};