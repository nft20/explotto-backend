module.exports = (sequelize, Sequelize) => {

    const UserAccountPayments = sequelize.define("user_account_payments", {
      address: {
        type: Sequelize.STRING
      },
      type : {
        type: Sequelize.STRING
      },
      type_data: {
        type: Sequelize.JSON
      },
      amount : {
        type: Sequelize.INTEGER
      },
      currency : {
        type: Sequelize.STRING
      },
      status : {
        type: Sequelize.STRING
      },
      tx_id : {
        type: Sequelize.STRING
      }
    });
  
    return UserAccountPayments;
  
};