module.exports = (sequelize, Sequelize) => {

    const Notification = sequelize.define("notifications", {

        event: {
            type: Sequelize.STRING
          },

        details : {
            type: [Sequelize.STRING]
        },

        type : {
            type: [Sequelize.STRING]
        },

        address: {
            type: Sequelize.STRING
          },
    });

    return Notification;

};