require('dotenv').config();
const dbConfig = require("../config/config.js");

const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  logging: true,
//   dialectOptions: {
//     useUTC: false, // -->Add this line. for reading from database
// },
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.UserAccount = require("./userAccount.js")(sequelize, Sequelize);
db.LotteryUserTicket = require("./lotteryUserTicket")(sequelize, Sequelize);
db.Team = require("./team.js")(sequelize, Sequelize);
db.Lottery = require("./lottery.js")(sequelize, Sequelize);
db.TeamStat = require("./teamStat.js")(sequelize, Sequelize);
db.UserAccountTeam = require("./userAccountTeam.js")(sequelize, Sequelize);
db.LotteryOrders = require("./lotteryOrders.js")(sequelize, Sequelize);
db.Properties = require("./properties.js")(sequelize, Sequelize);
db.UserAccountPayments = require("./userAccountPayments.js")(sequelize, Sequelize); 
db.AdminAccounts = require("./adminAccounts.js")(sequelize, Sequelize); 
db.AdminAccountPayments = require("./adminAccountPayments")(sequelize, Sequelize); 
db.AdminApi = require("./adminApi")(sequelize, Sequelize);
db.ApiRoleAccess = require("./apiRoleAccess")(sequelize, Sequelize);
db.Role = require("./role")(sequelize, Sequelize);
db.LotteryResults = require("./lotteryResults")(sequelize, Sequelize); 
db.Notification = require("./notification")(sequelize, Sequelize);

module.exports = db;