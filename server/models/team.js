module.exports = (sequelize, Sequelize) => {

    const Team = sequelize.define("team", {

        name : {
            type: Sequelize.STRING
        },
        team_id : {
            type : Sequelize.INTEGER
        },
        info : {
            type: Sequelize.STRING
        },
        pic_url: {
            type: Sequelize.STRING
        },
        user_cnt : {
            type: Sequelize.INTEGER
        },
        win_cnt : {
            type: Sequelize.INTEGER
        },
        total_point: {
            type: Sequelize.INTEGER
        },
        total_trade_volume: {
            type: Sequelize.INTEGER
        },
        total_trade_currency: {
            type: Sequelize.STRING
        }
    });

    return Team;

};