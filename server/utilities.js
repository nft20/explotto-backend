const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/users", { useNewUrlParser: true })

const userSchema = new mongoose.Schema({
    name: String,
    email: String,
    userDesc: String,
    accountAddress: String,

});

const User = mongoose.model("User", userSchema);

exports.fetchProfileDetails = function (req, res) {
    console.log('Inside fetchProfileDetails', req.body.accountAddress);

    User.find({ accountAddress: req.body.accountAddress }, function (err, result) {
        console.log("Error ", err, "result", result)
        res.send(result);
    })

}

exports.saveUserProfileDetails = function (req, res) {
    console.log('Inside saveProfileDetails', req.body);

    var newDetails = {
        name: req.body.name,
        email: req.body.email,
        userDesc: req.body.userDesc,
        accountAddress: req.body.accountAddress
    }

    User.findOneAndUpdate({ accountAddress: req.body.accountAddress }, newDetails, { new: true, upsert: true }, function (err, result) {
        if (err) res.send("Error Occoured while saving the details");
        else {
            res.send("saving UserProfileDetails in utilities ")
        }

    })
}