const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/marketplaceList", { useNewUrlParser: true })

const marketplaceSchema = new mongoose.Schema({
    tokenId: Number,
    rootId: Number,
    tokenHash: String,
    tokenUrl: String,
    creater: String,
    currentOwner: String,
    previousOwner: String,
    tokenName: String,
    tokenDesc: String,
    tokenPrice: String,
    createTime: Number,
    updateTime: Number,
    status: String,

});


const MarketplaceItem = mongoose.model("MarketplaceItem", marketplaceSchema);

exports.fetchMarketplaceDetails = function (req, res) {
    console.log('Inside fetchMarketplaceDetails');

    MarketplaceItem.find({ status: 'AVAILABLE' }, function (err, result) {
        console.log("Error ", err, "result", result)
        res.send(result);
    })

}

exports.fetchUserNFTs = function (req, res) {
    console.log('Inside fetchUserNFTs', req.body);

    MarketplaceItem.find({ currentOwner: req.body.accountAddress }, function (err, result) {
        console.log("Error ", err, "result", result)
        res.send(result);
    })

}

exports.saveToMarketplace = function (req, res) {
    console.log('Inside saveToMarketplace', (req.body.marketplaceTokenDetails.tokenId));

    const marketplaceObject = new MarketplaceItem({
        tokenId: req.body.marketplaceTokenDetails.tokenId,
        rootId: req.body.marketplaceTokenDetails.rootId,
        tokenHash: req.body.marketplaceTokenDetails.tokenHash,
        tokenUrl: req.body.marketplaceTokenDetails.tokenUrl,
        creater: req.body.marketplaceTokenDetails.creater,
        currentOwner: req.body.marketplaceTokenDetails.currentOwner,
        previousOwner: req.body.marketplaceTokenDetails.previousOwner,
        tokenName: req.body.marketplaceTokenDetails.tokenName,
        tokenDesc: req.body.marketplaceTokenDetails.tokenDesc,
        tokenPrice: req.body.marketplaceTokenDetails.tokenPrice,
        createTime: req.body.marketplaceTokenDetails.createTime,
        updateTime: req.body.marketplaceTokenDetails.updateTime,
        status: req.body.marketplaceTokenDetails.status
    });

    console.log('MarketplaceObject', marketplaceObject);

    MarketplaceItem.findOneAndUpdate({ tokenId: req.body.marketplaceTokenDetails.tokenId }, marketplaceObject, { new: true, upsert: true, useFindAndModify: false }, function (err, result) {
        if (err) {
            res.send("Error Occoured while saving the details to Marketplace");
        }
        else {
            res.send(result)
        }

    })
}


exports.sellOnMarketplace = function (req, res) {
    console.log('Inside sellOnMarketplace', (req.body));
   
    MarketplaceItem.findOneAndUpdate({ tokenId: req.body.tokenId }, {

        $set :  {status : "AVAILABLE"}

    }, { new: true, upsert: true, useFindAndModify: false }, function (err, result) {
        if (err) {
            res.send("Error Occoured while saving the details to Marketplace");
        }
        else {
            res.send(result)
        }

    })

}

exports.withdrawFromMarketplace = function (req, res) {
    console.log('Inside withdrawFromMarketplace', (req.body.marketplaceTokenDetails));
   
    MarketplaceItem.findOneAndUpdate({ tokenId: req.body.marketplaceTokenDetails.tokenId }, {

        $set :  {
            status : "NOT_AVAILABLE" , 
            currentOwner : req.body.marketplaceTokenDetails.currentOwner ,
            previousOwner : req.body.marketplaceTokenDetails.previousOwner,
            updateTime : req.body.marketplaceTokenDetails.updateTime,
            rootId : req.body.marketplaceTokenDetails.rootId
        }

    }, { new: true, upsert: true, useFindAndModify: false }, function (err, result) {
        if (err) {
            res.send("Error Occoured while saving the details to Marketplace");
        }
        else {
            res.send(result)
        }

    })

}

exports.delistFromMarketplace = function (req, res) {
    console.log('Inside delistFromMarketplace', (req.body.tokenId));
   
    MarketplaceItem.findOneAndUpdate({ tokenId: req.body.tokenId }, {

        $set :  {
            status : "NOT_AVAILABLE" , 
        }
    }, { new: true, upsert: true, useFindAndModify: false }, function (err, result) {
        if (err) {
            res.send("Error Occoured while saving the details to Marketplace");
        }
        else {
            res.send(result)
        }

    })

}

