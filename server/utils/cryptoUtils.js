const crypto = require("crypto");
function encrypt(data)
{
    var key = process.env.MASTER_KEY;
    var algorithm = "aes256";
    const iv = key.substring(0, 16);
    // crypto.randomBytes(16)
    var cipher = crypto.createCipheriv(algorithm, key, iv);
    var enData =
    cipher.update(data, "utf8", "hex") + cipher.final("hex");
    return enData;
}
function decrypt(enData)
{
    var key = process.env.MASTER_KEY;
    var algorithm = "aes256";
    const iv = key.substring(0, 16);
    var decipher = crypto.createDecipheriv(algorithm, key, iv);
    var data =
      decipher.update(enData, "hex", "utf8") + decipher.final("utf8");
    return data;
}
exports.encrypt=encrypt;
exports.decrypt=decrypt;