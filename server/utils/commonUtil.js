
async function generateTicketNumber() {
  let ticketNumber = "";
  for (let i = 0; i < 5; i++) {
    let number = Math.floor(Math.random() * 10);
    if (ticketNumber.length == 0) ticketNumber = ticketNumber + number;
    else ticketNumber = ticketNumber + " " + number;
  }
  console.log("ticketNumber ", ticketNumber);
  return ticketNumber;
}
exports.generateTicketNumber = generateTicketNumber;
